package com.lalidea.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.listeners.OnClickAdapter
import com.lalidea.models.GalleryData
import kotlinx.android.synthetic.main.row_gallery.view.*

class GalleryAdapter(private val mContext: Context, private val mArrayList: ArrayList<GalleryData>, private val productClick: OnClickAdapter) : RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    fun setList(mArrayList: ArrayList<GalleryData>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(mArrayList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GalleryViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_gallery, parent, false)
        return GalleryViewHolder(view)
    }

    inner class GalleryViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(galleryData: GalleryData) {

            Glide.with(view.context)
                    .load(galleryData.galleryUrl)
                    .fitCenter()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.ivGallery)

            itemView.setOnClickListener({
                productClick.onClickSelect(galleryData.galleryUrl.toString(), adapterPosition)
            })
        }
    }
}