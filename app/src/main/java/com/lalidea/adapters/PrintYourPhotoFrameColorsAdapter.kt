package com.lalidea.adapters

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.listeners.OnColorSelectedListener
import com.lalidea.models.SelfieModel
import kotlinx.android.synthetic.main.row_selfie_frame_color.view.*

class PrintYourPhotoFrameColorsAdapter(private val mContext: Context,
                                       private val mArrayList: ArrayList<SelfieModel.DataBean.ColorBean>,
                                       private val onColorSelectedListener: OnColorSelectedListener) : RecyclerView.Adapter<PrintYourPhotoFrameColorsAdapter.PrintYourPhotoFrameColorViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: PrintYourPhotoFrameColorViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    fun setList(mArrayList: ArrayList<SelfieModel.DataBean.ColorBean>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(mArrayList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PrintYourPhotoFrameColorViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_selfie_frame_color, parent, false)
        return PrintYourPhotoFrameColorViewHolder(view)
    }

    inner class PrintYourPhotoFrameColorViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(colorBean: SelfieModel.DataBean.ColorBean) {

            if (colorBean.sku == "#00000")
                view.ivColor.setBackgroundColor(Color.parseColor("#000000"))
            else {
                view.ivColor.setBackgroundColor(Color.parseColor(colorBean.sku))
            }

            val url ="https://www.lalidea.com/media/${colorBean.image}"

            Glide.with(view.context)
                    .load(url)
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(view.ivColorImage)

            view.setOnClickListener {
                onColorSelectedListener.onColorSelected(colorBean)
                colorBean.selected = true
            }
        }
    }
}