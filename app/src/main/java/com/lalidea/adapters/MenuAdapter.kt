package com.lalidea.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.activities.MainActivity
import com.lalidea.listeners.OnGetMenuId
import com.lalidea.models.MenuModel
import kotlinx.android.synthetic.main.row_menu_item.view.*
import kotlinx.android.synthetic.main.row_product_list_item.view.*

class MenuAdapter(
        var mainActivity: MainActivity,
        var menuList: MenuModel.DataBean?,
        var onGetMenuId: OnGetMenuId) : RecyclerView.Adapter<MenuAdapter.MenuHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder? {
        val view = LayoutInflater.from(mainActivity).inflate(R.layout.row_menu_item, parent, false)
        return MenuHolder(view)
    }

    override fun onBindViewHolder(holder: MenuHolder, position: Int) {
        menuList!!.category!![position].let { holder.bind(menuList!!.category!![position]) }
    }

    override fun getItemCount(): Int {
        return menuList!!.category!!.size
    }

    inner class MenuHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(menuList: MenuModel.DataBean.CategoryBean) {
            itemView.tvMenuName.text = menuList.category_name
            Glide.with(mainActivity)
                    .load(menuList.icon)
                    .fitCenter()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.imgMenu)

            itemView.setOnClickListener {
                onGetMenuId.getMenuId(menuList.category_id, menuList.category_name, menuList.subcategory)
            }
        }
    }
}
