package com.lalidea.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.activities.BrowserActivity
import kotlinx.android.synthetic.main.row_layout_account_menu.view.*

class LegalAdapter(private val mContext: Context, private val mArrayList: ArrayList<String>) : RecyclerView.Adapter<LegalAdapter.LegalViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: LegalViewHolder?, position: Int) {
        holder?.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): LegalViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_account_menu, parent, false)

        return LegalViewHolder(view)
    }

    inner class LegalViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(str: String) {

            itemView.txt_menu.text = str

            itemView.setOnClickListener {
                val intent = Intent(mContext, BrowserActivity::class.java)
                intent.putExtra("position", adapterPosition)
                mContext.startActivity(intent)
            }
        }
    }
}