package com.lalidea.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.activities.*
import com.lalidea.utils.fromactivity
import com.lalidea.utils.menuactivity
import kotlinx.android.synthetic.main.row_layout_account_menu.view.*

class AccountMenuAdapter(private val mContext: Context, private val mArrayList: ArrayList<String>) : RecyclerView.Adapter<AccountMenuAdapter.AccountViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: AccountViewHolder?, position: Int) {
        holder?.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AccountViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_account_menu, parent, false)

        return AccountViewHolder(view)
    }

    inner class AccountViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(str: String) {

            itemView.txt_menu.text = str

            itemView.setOnClickListener({
                when (adapterPosition) {
                    0 -> {
                        val intent = Intent(mContext, EditProfileActivity::class.java)
                        mContext.startActivity(intent)
                    }
                    1 -> {
                        val intent = Intent(mContext, AddressesActivity::class.java)
                        intent.putExtra(fromactivity, menuactivity)
                        mContext.startActivity(intent)
                    }
                    2 -> {
                        val intent = Intent(mContext, MyOrderActivity::class.java)
                        mContext.startActivity(intent)
                    }
                    3 -> {
                        val intent = Intent(mContext, MyReviewsActivity::class.java)
                        mContext.startActivity(intent)
                    }
                    4 -> {
                        val intent = Intent(mContext, WishListActivity::class.java)
                        mContext.startActivity(intent)
                    }
                    5 -> {
                        val intent = Intent(mContext, RecentlyViewedProductsActivity::class.java)
                        mContext.startActivity(intent)
                    }
                    6 -> {
                        val intent = Intent(mContext, NewsLetterSubscriptionActivity::class.java)
                        mContext.startActivity(intent)
                    }
                }
            })
        }
    }
}