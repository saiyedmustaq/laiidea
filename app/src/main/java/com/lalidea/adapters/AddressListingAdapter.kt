package com.lalidea.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.activities.AddNewAddressActivity
import com.lalidea.listeners.OnClickAdapter
import com.lalidea.models.DefaultAddress
import kotlinx.android.synthetic.main.row_layout_address.view.*

class AddressListingAdapter(private val mContext: Context,
                            private val mArrayList: ArrayList<DefaultAddress>,
                            private val addClick: OnClickAdapter) : RecyclerView.Adapter<AddressListingAdapter.AccountViewHolder>() {

    override fun getItemCount() = mArrayList.size


    override fun onBindViewHolder(holder: AccountViewHolder?, position: Int) {
        holder?.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AccountViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_address, parent, false)

        return AccountViewHolder(view)
    }

    inner class AccountViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(address: DefaultAddress) {

            var fullName = address.firstname

            if (address.middlename != null) {
                fullName = fullName + " " + address.middlename
            }
            if (address.lastname != null) {
                fullName = fullName + " " + address.lastname
            }

            var fullAddress = address.street
            if (address.city != null) {
                fullAddress = fullAddress + ", " + address.city
            }
            if (address.region != null) {
                fullAddress = fullAddress + ", " + address.region
            }
            if (address.postcode != null) {
                fullAddress = fullAddress + ", " + address.postcode
            }

            fullAddress += "-India."

            itemView.txtAdd.text = fullAddress
            itemView.txtAddName.text = fullName

            if (address.company != null) {
                itemView.txtAddcompany.text = address.company
            }

            itemView.txtAddtelephone.text = "Mobile: " + address.telephone

            if (address.fax != null) {
                itemView.txtAddtelefax.text = "Fax: " + address.fax
            }

            itemView.setOnClickListener({
                addClick.onClickSelect(itemView.txtAdd.text.toString(), adapterPosition)
            })

            itemView.ivEditAddress.setOnClickListener {
                val intent = Intent(mContext, AddNewAddressActivity::class.java)
                intent.putExtra("FROM", "EDIT")
                intent.putExtra("ADDRESSMODEL", address)
                mContext.startActivity(intent)
            }

            itemView.ivDeleteAddress.setOnClickListener {
                addClick.onClick(adapterPosition)
            }
        }
    }
}