package com.lalidea.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.models.ReviewsMainResponse
import com.lalidea.utils.convertDateTime
import kotlinx.android.synthetic.main.row_layout_review.view.*

class MyReviewsAdapter(private val mContext: Context, private val mArrayList: ArrayList<ReviewsMainResponse.DataBean>, var str: String) : RecyclerView.Adapter<MyReviewsAdapter.AccountViewHolder>() {

    override fun getItemCount(): Int {
        return if (str == "all") mArrayList.size else 2
    }

    override fun onBindViewHolder(holder: AccountViewHolder?, position: Int) {
        holder?.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AccountViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_review, parent, false)

        return AccountViewHolder(view)
    }

    inner class AccountViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(model: ReviewsMainResponse.DataBean) {
            itemView.txtProduct.text = model.product_name
            itemView.txtReview.text = model.detail

            itemView.txtProduct.isSelected = true

            if (model.date != null) {

                val convertedDateTime = convertDateTime("yyyy-MM-dd HH:mm:ss", "MMMM dd, yyyy", model.date!!)
                itemView.txtRevDate.text = "Submitted on: " + convertedDateTime
            }

            if (model.product_small_image != null) {
                Glide.with(view.context)
                        .load(model.product_detail_image)
                        .dontAnimate()
                        .placeholder(R.drawable.ic_place_holder)
                        .error(R.drawable.ic_place_holder)
                        .into(itemView.ivProductReview)

            }

        }
    }
}