package com.lalidea.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.activities.ProductDetailActivity
import com.lalidea.database.models.RecentlyViewedProductsModel
import kotlinx.android.synthetic.main.row_layout_recent_viewed.view.*
import java.util.*

class RecentViewedProductsAdapter(private val mContext: Context,
                                  private val mArrayList: ArrayList<RecentlyViewedProductsModel>) :
        RecyclerView.Adapter<RecentViewedProductsAdapter.AccountViewHolder>() {

    var mRecentProductArrayList: List<RecentlyViewedProductsModel> = mArrayList
        set(value) {
            field = value
            Collections.reverse(mArrayList)
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return mRecentProductArrayList.size
    }

    override fun onBindViewHolder(holder: AccountViewHolder?, position: Int) {
        holder?.bind(mRecentProductArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AccountViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_recent_viewed_all, parent, false)
        return AccountViewHolder(view)
    }

    inner class AccountViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(recentlyViewedProductsModel: RecentlyViewedProductsModel) {

            itemView.txtProductNmae.text = recentlyViewedProductsModel.name

            itemView.setOnClickListener({
                val intent = Intent(mContext, ProductDetailActivity::class.java)

                val bundle = Bundle()
                bundle.putString("PRODUCT_ID", "" + recentlyViewedProductsModel.id)
                bundle.putString("PRODUCT_NAME", recentlyViewedProductsModel.name)
                intent.putExtras(bundle)
                mContext.startActivity(intent)

            })
        }
    }
}