package com.lalidea.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.listeners.OnItemDeleteInShare
import com.lalidea.models.ShareRequestModel
import kotlinx.android.synthetic.main.row_layout_share.view.*

class ShareProductAdapter(private val mContext: Context,
                          private val mArrayList: ArrayList<ShareRequestModel.RecipientDataBean>,
                          private val onItemDeleteInShare: OnItemDeleteInShare)
    : RecyclerView.Adapter<ShareProductAdapter.ShareViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: ShareViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    fun setList(mArrayList: ArrayList<ShareRequestModel.RecipientDataBean>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(mArrayList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ShareViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_share, parent, false)
        return ShareViewHolder(view)
    }

    inner class ShareViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(share: ShareRequestModel.RecipientDataBean) {

            itemView.txtRecipientName.text = share.name
            itemView.txtRecipientEmail.text = share.email

            itemView.ivRemoveShareProduct.setOnClickListener {
                onItemDeleteInShare.onItemDelete(adapterPosition)
            }
        }
    }
}