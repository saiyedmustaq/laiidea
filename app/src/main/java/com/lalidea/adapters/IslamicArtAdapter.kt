package com.lalidea.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.activities.ProductDetailActivity
import com.lalidea.models.HomeModel
import com.lalidea.utils.typeNewArrival
import com.lalidea.utils.typePosters
import kotlinx.android.synthetic.main.row_home_products.view.*

class IslamicArtAdapter(private val mContext: Context,
                        private val mArrayList: ArrayList<HomeModel.DataBean.PostersBean>,
                        private val type: Int) : RecyclerView.Adapter<IslamicArtAdapter.IslamicHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IslamicHolder? {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_home_products, parent, false)

        if (type == typePosters || type == typeNewArrival) {
            val params = view.cvHomeProduct.layoutParams
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            params.width = ViewGroup.LayoutParams.MATCH_PARENT
            view.cvHomeProduct.layoutParams = params
        }
        return IslamicHolder(view)
    }

    override fun onBindViewHolder(holder: IslamicHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    override fun getItemCount() = mArrayList.size

    inner class IslamicHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(IslamicResponse: HomeModel.DataBean.PostersBean) {
            itemView.tvHomeProductName.text = IslamicResponse.product_name
            itemView.tvHomeProductPrice.text = IslamicResponse.product_price
            itemView.tvHomeProductName.isSelected = true
            Glide.with(itemView.context)
                    .load(IslamicResponse.product_image)
                    .fitCenter()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.ivBrands)

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, ProductDetailActivity::class.java)
                intent.putExtra("PRODUCT_ID", IslamicResponse.product_id)
                intent.putExtra("PRODUCT_NAME", IslamicResponse.product_name)
                itemView.context.startActivity(intent)
            }
        }
    }
}
