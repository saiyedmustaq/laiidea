package com.lalidea.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.activities.ProductDetailActivity
import com.lalidea.models.HomeModel
import com.lalidea.utils.typeNewArrival
import com.lalidea.utils.typePosters
import kotlinx.android.synthetic.main.row_home_products.view.*

class HomeProductsAdapter(private val mContext: Context,
                          private val mArrayList: ArrayList<HomeModel.DataBean.PostersBean>,
                          private val type: Int)
    : RecyclerView.Adapter<HomeProductsAdapter.BrandsViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: BrandsViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    fun setList(mArrayList: ArrayList<HomeModel.DataBean.PostersBean>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(mArrayList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BrandsViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_home_products, parent, false)

        if (type == typePosters) {
            val params = view.cvHomeProduct.layoutParams
            params.height = WRAP_CONTENT
            params.width = MATCH_PARENT
            view.cvHomeProduct.layoutParams = params
        }
        return BrandsViewHolder(view)
    }

    inner class BrandsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(moreFeaturesModel: HomeModel.DataBean.PostersBean) {

            view.tvHomeProductName.text = moreFeaturesModel.product_name
            view.tvHomeProductPrice.text = moreFeaturesModel.product_price
            itemView.tvHomeProductName.isSelected = true
            Glide.with(view.context)
                    .load(moreFeaturesModel.product_image)
                    .fitCenter()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.ivBrands)

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, ProductDetailActivity::class.java)
                intent.putExtra("PRODUCT_ID", moreFeaturesModel.product_id)
                intent.putExtra("PRODUCT_NAME", moreFeaturesModel.product_name)
                itemView.context.startActivity(intent)
            }
        }
    }
}