package com.lalidea.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.models.MoreFeaturesModel
import kotlinx.android.synthetic.main.row_more_features.view.*

class MoreFeaturesAdapter(private val mContext: Context, private val mArrayList: ArrayList<MoreFeaturesModel>) : RecyclerView.Adapter<MoreFeaturesAdapter.MoreFeaturesViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: MoreFeaturesViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    fun setList(mArrayList: ArrayList<MoreFeaturesModel>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(mArrayList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MoreFeaturesViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_more_features, parent, false)
        return MoreFeaturesViewHolder(view)
    }

    inner class MoreFeaturesViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(moreFeaturesModel: MoreFeaturesModel) {
            itemView.tvTitleMoreFeatures.text = moreFeaturesModel.title
            itemView.tvDescriptionMoreFeatures.text = moreFeaturesModel.description
            itemView.ivThumbMoreFeatures.setImageDrawable(ContextCompat.getDrawable(view.context, moreFeaturesModel.thumb!!))
        }
    }
}