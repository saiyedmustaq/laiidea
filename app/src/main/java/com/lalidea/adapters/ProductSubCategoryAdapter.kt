package com.lalidea.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.activities.ProductListActivity
import com.lalidea.models.ProductSubCategoryModel
import kotlinx.android.synthetic.main.row_product_sub_types.view.*


class ProductSubCategoryAdapter(private val mContext: Context, private val mArrayList: ArrayList<ProductSubCategoryModel.DataBean>) : RecyclerView.Adapter<ProductSubCategoryAdapter.MoreFeaturesViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: MoreFeaturesViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    fun setList(mArrayList: ArrayList<ProductSubCategoryModel.DataBean>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(mArrayList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MoreFeaturesViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_product_sub_types, parent, false)
        return MoreFeaturesViewHolder(view)
    }

    inner class MoreFeaturesViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(productSubTypeModel: ProductSubCategoryModel.DataBean) {

            itemView.tvProductTypeTitle.text = productSubTypeModel.name
            itemView.tvProductTypeId.text = productSubTypeModel.id

            itemView.rlProductSubTypeItem.setOnClickListener {
                val intent = Intent(itemView.context, ProductListActivity::class.java)
                intent.putExtra("Name", productSubTypeModel.name)
                intent.putExtra("CategoryId", productSubTypeModel.id)
                itemView.context.startActivity(intent)
            }
        }
    }
}