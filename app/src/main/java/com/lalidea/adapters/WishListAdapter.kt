package com.lalidea.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.listeners.OnItemUpdateDeleteAddWishList
import com.lalidea.models.WishlistMainResponse
import com.lalidea.utils.formatString
import kotlinx.android.synthetic.main.row_layout_wishlist.view.*

class WishListAdapter(private val mContext: Context, private val mArrayList: ArrayList<WishlistMainResponse.DataBean>,
                      private val onItemUpdateDeleteInCart: OnItemUpdateDeleteAddWishList) : RecyclerView.Adapter<WishListAdapter.AccountViewHolder>() {

    override fun getItemCount(): Int {
        return mArrayList.size
    }


    override fun onBindViewHolder(holder: AccountViewHolder?, position: Int) {
        holder?.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AccountViewHolder {

        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_wishlist, parent, false)
        return AccountViewHolder(view)

    }

    inner class AccountViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(model: WishlistMainResponse.DataBean) {

            itemView.txtProductName.text = model.product_name
            itemView.tvWishProductUnitPrice.text = model.price
            itemView.txtProductQuantity.text = formatString(model.qty!!.toDouble())
            itemView.txtProductDecription.text = model.product_description

            if (model.inStock == 0) {
                itemView.btnAddtoCart.visibility = View.GONE
                itemView.txtOutOfStock.visibility = View.VISIBLE


            } else {
                itemView.txtOutOfStock.visibility = View.GONE
                itemView.btnAddtoCart.visibility = View.VISIBLE
            }

//            itemView.txtProductQuantity.setOnClickListener {
//                onItemUpdateDeleteInCart.onItemUpdate(adapterPosition)
//            }

            Glide.with(view.context)
                    .load(model.product_image)
                    .centerCrop()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.ivProductWish)

            itemView.ivdelete.setOnClickListener {
                onItemUpdateDeleteInCart.onItemDelete(adapterPosition)
            }

            itemView.btnAddtoCart.setOnClickListener {
                onItemUpdateDeleteInCart.onItemAddToCart(adapterPosition)
            }
        }
    }

}