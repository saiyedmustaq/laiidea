package com.lalidea.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.models.ProductsCompareModel
import com.lalidea.listeners.OnProductRemoveFromCompare
import com.lalidea.utils.ResizableCustomView
import kotlinx.android.synthetic.main.row_layout_compare.view.*

/**
 * Created by kashifa on 12/12/17.
 */
class ProductCompareAdapter(private val mContext: Context,
                            private val mproductcompareList: ArrayList<ProductsCompareModel>,
                            private var onProductRemove: OnProductRemoveFromCompare)
    : RecyclerView.Adapter<ProductCompareAdapter.CompareViewHolder>(){

    override fun onBindViewHolder(holder: CompareViewHolder, position: Int) {
        holder.bind(mproductcompareList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CompareViewHolder {

        val view= LayoutInflater.from(mContext).inflate(R.layout.row_layout_compare,parent,false)
        return CompareViewHolder(view)
    }

    override fun getItemCount()= mproductcompareList.size

    inner class CompareViewHolder (val view: View):RecyclerView.ViewHolder(view){
        fun bind(productsCompare: ProductsCompareModel) {

            if (mproductcompareList.indexOf(productsCompare).equals(0)){

                itemView.llCompareIcons.visibility=View.INVISIBLE
                itemView.ivProductImageCompare.visibility=View.INVISIBLE
                itemView.ivRemoveCompareProduct.visibility=View.INVISIBLE
                itemView.tvDescriptionCompare.text = productsCompare.description
                itemView.tvSDCompare.text = productsCompare.shortdescription
                itemView.tvSkuCompare.text = productsCompare.sku
            }
            else {

                Glide.with(view.context)
                        .load(productsCompare.thumb)
                        .fitCenter()
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(itemView.ivProductImageCompare)

                itemView.tvCompareTitle.text = productsCompare.producttitle

                itemView.tvComparePrice.text = productsCompare.price

                itemView.tvDescriptionCompare.text = productsCompare.description

                ResizableCustomView.doResizeTextView(mContext,itemView.tvDescriptionCompare, 8, "View More", true)

                itemView.tvSDCompare.text = productsCompare.shortdescription

                ResizableCustomView.doResizeTextView(mContext,itemView.tvSDCompare, 3, "View More", true)

                itemView.tvSkuCompare.text = productsCompare.sku

                itemView.ivProductImageCompare.setOnClickListener{
                    onProductRemove.onItemClicked(adapterPosition)
                }

                itemView.ivRemoveCompareProduct.setOnClickListener {
                    onProductRemove.onCompareRemove(adapterPosition)
                }

                itemView.ivAddToCartCompare.setOnClickListener {
                    onProductRemove.onCompare_addtocart(adapterPosition)
                }

                itemView.ivAddToWishListCompare.setOnClickListener {
                    onProductRemove.onCompare_addtoWishlist(adapterPosition)
                }
            }
        }
    }

}