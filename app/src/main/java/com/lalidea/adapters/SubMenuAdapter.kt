package com.lalidea.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.listeners.OnGetMenuId
import com.lalidea.models.MenuModel
import kotlinx.android.synthetic.main.row_sub_category.view.*
import java.util.ArrayList

class SubMenuAdapter(
        val context: Context,
        var subMenuList: ArrayList<MenuModel.DataBean.CategoryBean.SubCategory>,
        var onGetMenuId: OnGetMenuId)
    : RecyclerView.Adapter<SubMenuAdapter.SubMenuHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubMenuHolder? {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_sub_category, parent, false)
        return SubMenuHolder(view)
    }

    override fun onBindViewHolder(holder: SubMenuHolder, position: Int) {
        subMenuList[position].let { holder.bind(subMenuList[position]) }
    }

    override fun getItemCount(): Int {
        return subMenuList.size

    }

    inner class SubMenuHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(menuList: MenuModel.DataBean.CategoryBean.SubCategory) {
            itemView.tvScbCategoryTitle.text = menuList.category_name
            itemView.setOnClickListener {
                onGetMenuId.onClicMenu(menuList.category_id,menuList.category_name)
            }
        }
    }
}
