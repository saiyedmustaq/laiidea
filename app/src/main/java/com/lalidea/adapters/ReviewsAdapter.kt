package com.lalidea.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.models.Reviews
import com.lalidea.utils.convertDateTime
import kotlinx.android.synthetic.main.row_layout_reviews.view.*

class ReviewsAdapter(private val mContext: Context,
                     private val mArrayList: ArrayList<Reviews>)
    : RecyclerView.Adapter<ReviewsAdapter.ReviewsViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: ReviewsViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ReviewsAdapter.ReviewsViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_reviews, parent, false)
        return ReviewsViewHolder(view)
    }


    inner class ReviewsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind(reviewsData: Reviews) {

            itemView.tvReviewId.text = reviewsData.reviewsId
            itemView.tvReviewName.text = reviewsData.title
            itemView.tvReviewDesc.text = reviewsData.detail

            if (reviewsData.createdAt != null) {
                val convertedDateTime = convertDateTime("yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy", reviewsData.createdAt!!)
                itemView.tvReviewBy.text = "Review By ${reviewsData.nickName} (Posted On $convertedDateTime)"
            }
        }
    }
}