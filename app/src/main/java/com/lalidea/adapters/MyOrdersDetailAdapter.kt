package com.lalidea.adapters

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.models.MyOrderDetailResponse
import com.lalidea.utils.formatString
import com.lalidea.utils.isColorValid
import kotlinx.android.synthetic.main.row_layout_my_order_detail.view.*
import java.util.*

class MyOrdersDetailAdapter(private val mContext: Context, private val mArrayList: ArrayList<MyOrderDetailResponse.DataBean.ProductBean>) : RecyclerView.Adapter<MyOrdersDetailAdapter.AccountViewHolder>() {

    override fun getItemCount(): Int {
        return mArrayList.size
    }

    override fun onBindViewHolder(holder: AccountViewHolder?, position: Int) {
        holder?.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AccountViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_my_order_detail, parent, false)
        return AccountViewHolder(view)
    }

    inner class AccountViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(myOrderDetail: MyOrderDetailResponse.DataBean.ProductBean) {
            Glide.with(view.context)
                    .load(myOrderDetail.product_image)
                    .fitCenter()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.ivImagepdt)


            if (myOrderDetail.custom_options!!.has("frame_color")) {
                if (isColorValid(myOrderDetail.custom_options!!.get("frame_color").asString)) {
                    itemView.llCartProductThumbCheckout.setBackgroundColor(Color.parseColor((myOrderDetail.custom_options!!.get("frame_color").asString)))
                } else {
                    itemView.llCartProductThumbCheckout.setBackgroundColor(Color.WHITE)
                }

            } else {
                itemView.llCartProductThumbCheckout.setBackgroundColor(Color.WHITE)
            }
            itemView.txtNameProduct.text = myOrderDetail.product_name

            itemView.txtNameProductDesc.text = myOrderDetail.product_sku

            itemView.txtorderprice.text = myOrderDetail.product_price

            itemView.txtorderQty.text = formatString(myOrderDetail.qty!!.toDouble())
            itemView.txtordersubtotal.text = myOrderDetail.subtotal
        }
    }
}