package com.lalidea.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import com.lalidea.fragments.HomeBannerFragment
import com.lalidea.models.HomeModel

class SectionsPagerAdapter(fm: FragmentManager, private var slideshowImageBean:
ArrayList<HomeModel.DataBean.SlideshowImageBean>?,private var viewPageHeight:Int) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        val fragmentPos = position % slideshowImageBean!!.size
        return HomeBannerFragment.newInstance(fragmentPos,
                slideshowImageBean!![fragmentPos].slider!!,viewPageHeight)
    }

    override fun getCount(): Int {
        return Integer.MAX_VALUE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }
}
