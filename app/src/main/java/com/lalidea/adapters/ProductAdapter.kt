package com.lalidea.adapters

import android.content.Context
import android.provider.Settings.Global.getString
import android.support.v7.widget.RecyclerView
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.listeners.OnProductItemClickListener
import com.lalidea.models.ProductDataModel
import kotlinx.android.synthetic.main.row_product_list_item.view.*

class ProductAdapter(private val mContext: Context,
                     private val mArrayList: ArrayList<ProductDataModel>,
                     private val onProductItemClickListener: OnProductItemClickListener)
    : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private val LIST_ITEM = 0
    private val GRID_ITEM = 1
    var isSwitchView = true

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ProductViewHolder {
        val view = if (viewType == LIST_ITEM) {
            LayoutInflater.from(parent?.context).inflate(R.layout.row_product_list_item, null)
        } else {
            LayoutInflater.from(parent?.context).inflate(R.layout.row_product_grid_item, null)
        }
        return ProductViewHolder(view)
    }

    fun addAll(arrayList: List<ProductDataModel>) {
        mArrayList.addAll(arrayList)
        notifyDataSetChanged()
    }

    inner class ProductViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(productData: ProductDataModel) {
            itemView.tvTitleProductListItem.text = productData.productName
            itemView.tvDescriptionProductListItem.text = productData.productDescription

            if (productData.special_price == null || productData.special_price == productData.productPrice || productData.special_price == "" || productData.special_price == 0.toString()) {
                itemView.tvPriceProductListItemNormalPrice.text = "Rs." + productData.productPrice
                itemView.tvPriceProductListItemSpecialPrice.visibility = View.GONE
            } else {
                itemView.tvPriceProductListItemSpecialPrice.text = "Rs." + productData.special_price
                val ssBuilder = SpannableStringBuilder("Rs" + productData.productPrice)
                val strikeboundSpan = StrikethroughSpan()
                ssBuilder.setSpan(
                        strikeboundSpan,
                        0,
                        ssBuilder.length,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                itemView.tvPriceProductListItemNormalPrice.text = ssBuilder
            }
            Glide.with(view.context)
                    .load(productData.productSmallImage)
                    .fitCenter()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.ivProductThumb)

            itemView.ivAddToCartProductListItem.setOnClickListener {
                onProductItemClickListener.onAddToCartClicked(productData)
            }

            itemView.ivAddToWishListProductListItem.setOnClickListener {
                onProductItemClickListener.onAddToWishListClicked(productData)
            }

            itemView.ivShareProductListItem.setOnClickListener {
                onProductItemClickListener.onAddToShareClicked(productData)
            }

            itemView.setOnClickListener {
                val productDataModel: ProductDataModel = mArrayList[adapterPosition]
                onProductItemClickListener.onItemClicked(productDataModel)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isSwitchView) {
            LIST_ITEM
        } else {
            GRID_ITEM
        }
    }

    fun toggleItemViewType(): Boolean {
        isSwitchView = !isSwitchView
        return isSwitchView
    }
}