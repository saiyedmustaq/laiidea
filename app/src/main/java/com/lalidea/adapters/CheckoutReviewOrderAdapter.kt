package com.lalidea.adapters

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.models.ShoppingCartResponseModel
import com.lalidea.utils.formatString
import com.lalidea.utils.isColorValid
import kotlinx.android.synthetic.main.row_layout_my_order_detail.view.*
import java.util.*

class CheckoutReviewOrderAdapter(private val mContext: Context,
                                 private val mArrayList: ArrayList<ShoppingCartResponseModel.DataBean.ProductBean>)
    : RecyclerView.Adapter<CheckoutReviewOrderAdapter.AccountViewHolder>() {

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: AccountViewHolder?, position: Int) {
        holder?.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AccountViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_my_order_detail, parent, false)

        return AccountViewHolder(view)
    }

    inner class AccountViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(str: ShoppingCartResponseModel.DataBean.ProductBean) {
            Glide.with(view.context)
                    .load(str.product_image)
                    .fitCenter()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.ivImagepdt)

            if (str.custom_options!!.has("frame_color")) {
                if (isColorValid(str.custom_options!!.get("frame_color").asString)) {
                    itemView.llCartProductThumbCheckout.setBackgroundColor(Color.parseColor((str.custom_options!!.get("frame_color").asString)))
                } else {
                    itemView.llCartProductThumbCheckout.setBackgroundColor(Color.WHITE)
                }

            } else {
                itemView.llCartProductThumbCheckout.setBackgroundColor(Color.WHITE)
            }


            itemView.txtNameProduct.text = str.product_name
            itemView.txtorderprice.text = str.unit_price
            itemView.txtorderQty.text = formatString(str.qty.toDouble())
            itemView.txtordersubtotal.text = str.product_sub_total
        }
    }
}