package com.lalidea.adapters

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.listeners.OnItemUpdateDeleteInCart
import com.lalidea.models.ShoppingCartResponseModel
import com.lalidea.utils.formatString
import com.lalidea.utils.isColorValid
import com.lalidea.utils.toast
import kotlinx.android.synthetic.main.row_shopping_cart.view.*

class ShoppingCartAdapter(private val mContext: Context,
                          private val mArrayList: ArrayList<ShoppingCartResponseModel.DataBean.ProductBean>,
                          private val onItemUpdateDeleteInCart: OnItemUpdateDeleteInCart)
    : RecyclerView.Adapter<ShoppingCartAdapter.ShoppingCartViewHolder>() {

    val TAG = ShoppingCartAdapter::class.java.simpleName

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: ShoppingCartViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    fun setList(mArrayList: List<ShoppingCartResponseModel.DataBean.ProductBean>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(mArrayList)
        notifyDataSetChanged()
    }

    fun clearAdapter() {
        this.mArrayList.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ShoppingCartViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_shopping_cart, parent, false)
        return ShoppingCartViewHolder(view)
    }

    inner class ShoppingCartViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(shoppingCartModel: ShoppingCartResponseModel.DataBean.ProductBean) {

            if (shoppingCartModel.custom_options!!.has("frame_color")) {
                Log.d(TAG, "${shoppingCartModel.custom_options!!.get("frame_color")}")
                if (isColorValid(shoppingCartModel.custom_options!!.get("frame_color").asString)) {
                    itemView.llCartProductThumb.setBackgroundColor(Color.parseColor((shoppingCartModel.custom_options!!.get("frame_color").asString)))
                } else {
                    itemView.llCartProductThumb.setBackgroundColor(Color.WHITE)
                }
//                itemView.llCartProductThumb.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_light))
            } else {
                itemView.llCartProductThumb.setBackgroundColor(Color.WHITE)
            }

            Glide.with(view.context)
                    .load(shoppingCartModel.product_image)
                    .fitCenter()
                    .dontAnimate()
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.ivCartProductThumb)

            itemView.tvCartProductName.text = shoppingCartModel.product_name
            itemView.tvCartProductUnitPrice.text = shoppingCartModel.unit_price
            itemView.tvCartProductQty.text = formatString(shoppingCartModel.qty.toDouble())
            itemView.tvCartProductSubTotal.text = shoppingCartModel.product_sub_total

            if (shoppingCartModel.inStock == 0) {
                itemView.tvProductAvailabilty.visibility = View.VISIBLE
            } else {
                itemView.tvProductAvailabilty.visibility = View.GONE
            }


            itemView.ivRemoveCartProduct.setOnClickListener {
                onItemUpdateDeleteInCart.onItemDelete(adapterPosition)
            }

            itemView.tvCartProductQty.setOnClickListener {
                if (shoppingCartModel.inStock == 0) {
                    toast(mContext, mContext.getString(R.string.product_is_out_of_stock))
                } else {
                    onItemUpdateDeleteInCart.onItemUpdate(adapterPosition)
                }
            }
        }
    }
}