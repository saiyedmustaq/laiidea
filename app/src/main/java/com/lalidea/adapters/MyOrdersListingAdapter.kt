package com.lalidea.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.activities.MyOrderDetailActivity
import com.lalidea.listeners.onCancelOrderClickListener
import com.lalidea.models.MyOrderListResponse
import com.lalidea.utils.convertDateTime
import kotlinx.android.synthetic.main.row_layout_my_order.view.*

class MyOrdersListingAdapter(private val mContext: Context,
                             private val mArrayList: ArrayList<MyOrderListResponse.DataBean>,
                             var str: String, var onReOrderClickListener: onCancelOrderClickListener) : RecyclerView.Adapter<MyOrdersListingAdapter.OrderViewHolder>() {

    override fun getItemCount(): Int = if (str == "all") mArrayList.size else 1

    override fun onBindViewHolder(holder: OrderViewHolder?, position: Int) {
        holder?.bind(mArrayList[position])
    }

    fun addAll(arrayList: List<MyOrderListResponse.DataBean>) {
        mArrayList.addAll(arrayList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): OrderViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_my_order, parent, false)

        return OrderViewHolder(view)
    }

    inner class OrderViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(str: MyOrderListResponse.DataBean) {

            itemView.txtorderstatus.text = "" + str.status
            itemView.txtorderNumber.text = "" + str.print_order_id

            itemView.txtordershipto.text = str.ship_to
            itemView.txtordertotal.text = "" + str.grand_total
            if (str.status != null) {
                when {
                    str.status!! == "Canceled" -> {
                        itemView.btnCancelMyOrderList.visibility = View.GONE
                        itemView.txtorderstatus.setTextColor(Color.parseColor("#FF0000"))

                    }
                    str.status!! == "Complete" -> {
                        itemView.btnCancelMyOrderList.visibility = View.GONE
                        itemView.txtorderstatus.setTextColor(Color.parseColor("#00FF00"))
                    }
                    else -> {
                        itemView.btnCancelMyOrderList.visibility = View.VISIBLE
                        itemView.txtorderstatus.setTextColor(Color.parseColor("#E1823A"))
                    }
                }
            }



            if (str.date != null) {
                val convertedDateTime = convertDateTime("yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy", str.date!!)
                itemView.txtorderDate.text = convertedDateTime
            }

            itemView.setOnClickListener({

                val intent = Intent(mContext, MyOrderDetailActivity::class.java)
                intent.putExtra("print_order_id", str.print_order_id)
                intent.putExtra("order_id", str.order_id)
                intent.putExtra("status", str.status)
                mContext.startActivity(intent)
            })

            itemView.btnCancelMyOrderList.setOnClickListener {
                onReOrderClickListener.onCancelOrderClick(str.order_id!!,adapterPosition)
            }
        }
    }
}