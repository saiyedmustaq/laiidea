package com.lalidea.fragments

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.FileProvider
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.R.string.arrayList
import com.lalidea.activities.ShoppingCartActivity
import com.lalidea.adapters.PrintYourPhotoFrameColorsAdapter
import com.lalidea.listeners.OnColorSelectedListener
import com.lalidea.models.AddToCartModel
import com.lalidea.models.SelfieModel
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.dialog_choose_image.*
import kotlinx.android.synthetic.main.dialog_update_qty.*
import kotlinx.android.synthetic.main.layout_no_data.view.*
import kotlinx.android.synthetic.main.print_your_photo_fragment.*
import kotlinx.android.synthetic.main.print_your_photo_fragment.view.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File


class PrintYourPhotoFragment : Fragment(), OnColorSelectedListener {

    var TAG: String = PrintYourPhotoFragment::class.java.simpleName

    lateinit var mActivity: Activity
    var profileImageUri = ""

    internal var height: Int = 0
    internal var width: Int = 0

    private val arrayColors = ArrayList<SelfieModel.DataBean.ColorBean>()
    private lateinit var mSelfieFrameColorsAdapter: PrintYourPhotoFrameColorsAdapter
    private var selfieImageFile: File? = null

    lateinit var cartBadge: LayerDrawable

    var colorId = "0"

    var productId = 40

    companion object {
        private val TYPE = "type"

        fun newInstance(): PrintYourPhotoFragment {
            val args = Bundle()
//            args.putSerializable(TYPE, type)
//            args.putInt(TYPE, type)
            val fragment = PrintYourPhotoFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.print_your_photo_fragment, container, false)

        val metrics = resources.displayMetrics
        height = 297
        width = 210
//
//        val viewTreeObserver: ViewTreeObserver = view.ivSelfiePrint.viewTreeObserver
//        viewTreeObserver.addOnPreDrawListener {
//            view.ivSelfiePrint.viewTreeObserver.removeOnPreDrawListener { true }
//            height = view.ivSelfiePrint.measuredHeight
//            width = view.ivSelfiePrint.measuredWidth
//            true
//        }
        setHasOptionsMenu(true)

        view.rvColors.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        mSelfieFrameColorsAdapter = PrintYourPhotoFrameColorsAdapter(activity, arrayColors, this)
        view.rvColors.adapter = mSelfieFrameColorsAdapter
        view.rvColors.isNestedScrollingEnabled = false


        if (isNetworkAvailable(activity)) {
            getSelfieData(view)
        } else {
            view.btnAddToCartSelfiePrint.visibility = View.GONE
            showNoInternetLayout(view.llParentNoData)
            toast(activity, R.string.network_error)
        }

        view.btnAddToCartSelfiePrint.setOnClickListener {
            if (selfieImageFile != null)
                dialogEditQty(view)
            else {
                toast(activity, getString(R.string.click_picture_first))
            }
        }


        return view
    }

    /**
     * dialog to edit quantity
     * */
    private fun dialogEditQty(view: View) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_update_qty)
        dialog.etDialogQty.setText("1")
        dialog.etDialogQty.setSelection(dialog.etDialogQty.text.length)
        dialog.show()

        dialog.tvQtyDialogCancel.setOnClickListener { dialog.dismiss() }

        dialog.tvQtyDialogOk.setOnClickListener {

            if (dialog.etDialogQty.text.toString().trim().isEmpty() || dialog.etDialogQty.text.toString().trim().toInt() == 0) {
                toast(activity, getString(R.string.please_enter_proper_qty))
            } else {
                if (!isNetworkAvailable(activity)) {
                    toast(activity, getString(R.string.network_error))
                } else {
                    addSelfieToCart(view, dialog.etDialogQty.text.toString(), dialog)
                }
            }
        }
    }


    /**
     * to check runtime permissions
     */
    private fun checkCameraAndStoragePermissions() {
        val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (!hasPermissions(activity, permissions)) {
            requestPermissions(permissions, CAMERA_PERMISSION_CODE)
        } else {
            chooseCameraOrGallery()
        }
    }

    private fun openCamera() {
        val mIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
        val file = File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg")
        val uri = FileProvider.getUriForFile(activity, activity.packageName + ".provider", file)
        mIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(mIntent, REQUEST_CAMERA_IMAGE)
//        activity.startActivityFromFragment(this@PrintYourPhotoFragment, mIntent, 3)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.e(TAG, "$requestCode , $permissions, $grantResults")
        //Checking the request code of our request
        if (requestCode == CAMERA_PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseCameraOrGallery()
            } else {
                //Displaying another toast if permission is not granted
                toast(activity, getString(R.string.camera_permission))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(TAG, "onActivityResult : requestCode:$requestCode , resultCode:$resultCode")

        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {

        //TODO... onCamera Picker Result
            REQUEST_CAMERA_IMAGE -> if (resultCode == Activity.RESULT_OK) {

                val file = File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg")
                val uri = FileProvider.getUriForFile(activity, activity.applicationContext.packageName + ".provider", file)

                //Crop camera image
                CropImage.activity(uri)
                        .setAspectRatio(width, height)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(activity, this@PrintYourPhotoFragment)
            }

        //TODO... onGallery Image Selection Result
            REQUEST_GALLERY_IMAGE -> if (resultCode == Activity.RESULT_OK) {
                val selectedImage = data!!.data


                //Crop gallery image
                CropImage.activity(selectedImage!!)
                        .setAspectRatio(width, height)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(activity, this@PrintYourPhotoFragment)
            }

        //TODO... onCropped Image Result
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> if (resultCode == Activity.RESULT_OK) {
                val result = CropImage.getActivityResult(data)
                val resultUri = result.uri

                val uri = Uri.parse(resultUri.toString())
                val myFile = File(uri.path)
                profileImageUri = myFile.absolutePath

                selfieImageFile = myFile

                //Crop camera image
                Log.d(TAG, "profileImageUri >> $profileImageUri")
                if (::mActivity.isInitialized)
                    Glide.with(mActivity)
                            .load(profileImageUri)
                            .dontAnimate()
                            .into(ivSelfiePrint)


            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_selfie, menu)

        val itemCart = menu.findItem(R.id.menuCartSelfie)
        cartBadge = itemCart.icon as LayerDrawable
        setBadgeCount(activity, cartBadge, SharedPref.getIntValue(activity, cartCount).toString())

        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menuCamera -> {
                checkCameraAndStoragePermissions()
            }
            R.id.menuCartSelfie -> {
                if (SharedPref.getIntValue(activity, cartCount) > 0)
                    startActivity(Intent(activity, ShoppingCartActivity::class.java))
                else
                    toast(activity, R.string.no_item_in_cart)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is Activity) {
            mActivity = context
        }
    }

    private fun getSelfieData(view: View) {
        showProgressDialog(activity, view)

        RetrofitClientSingleton.getInstance().getSelfieData().enqueue(object : retrofit2.Callback<SelfieModel> {
            override fun onFailure(call: Call<SelfieModel>, t: Throwable) {
                // toast(activity, getString(R.string.something_went_wrong))
                stopProgress()
                toast(activity, getString(R.string.something_went_wrong))
                showSomethingWentWrongDataLayout(view.llParentNoData)
            }

            override fun onResponse(call: Call<SelfieModel>, response: Response<SelfieModel>) {
                stopProgress()

                if (response.isSuccessful) {
                    val selfieDataResponse = response.body()
                    when (selfieDataResponse?.status) {
                        SUCCESS -> {
                            if (selfieDataResponse.data != null) {
                                val list = selfieDataResponse.data!!.Color!!
                                if (list.isNotEmpty()) {
                                    selfieImageFile = null

                                    view.btnAddToCartSelfiePrint.visibility = View.VISIBLE

                                    checkCameraAndStoragePermissions()
                                    productId = selfieDataResponse.data!!.product_id
                                    colorId = selfieDataResponse.data!!.Color!![0].option_type_id!!
                                    arrayColors.clear()
                                    arrayColors.addAll(selfieDataResponse.data!!.Color!!)
                                    mSelfieFrameColorsAdapter.notifyDataSetChanged()
                                    val price = String.format("%.2f", selfieDataResponse.data!!.getPrice)
                                    view.btnAddToCartSelfiePrint.text = price
                                } else {
                                    view.btnAddToCartSelfiePrint.visibility = View.GONE
                                    showSomethingWentWrongDataLayout(view.llParentNoData)
                                }
                            } else {
                                view.btnAddToCartSelfiePrint.visibility = View.GONE
                                toast(activity, selfieDataResponse.message)
                            }

                        }
                        FAILURE -> {
                            view.btnAddToCartSelfiePrint.visibility = View.GONE
                            toast(activity, selfieDataResponse.message)
                            showSomethingWentWrongDataLayout(view.llParentNoData)
                        }

                        ORDER_ALREADY_PLACED -> {
                            view.btnAddToCartSelfiePrint.visibility = View.GONE
                            toast(activity, selfieDataResponse.message)
                            showSomethingWentWrongDataLayout(view.llParentNoData)
                        }

                    }

                }
            }

        })

    }

    override fun onColorSelected(colorBean: SelfieModel.DataBean.ColorBean) {
        if (colorBean.sku == "#00000")
            rlSelfieImage.setBackgroundColor(Color.parseColor("#000000"))
        else
            rlSelfieImage.setBackgroundColor(Color.parseColor(colorBean.sku))

        colorId = colorBean.option_type_id!!
    }

    private fun addSelfieToCart(view: View, qty: String, dialog: Dialog) {
        showProgressDialog(activity, view)

        val map = HashMap<String, RequestBody>()

        val userCartId = SharedPref.getIntValue(activity, cartId)
        if (userCartId != 0)
            map["cart_id"] = getIntRequestBody(userCartId)
        else {
            map["cart_id"] = getStringRequestBody("")
        }
        map["device_id"] = getStringRequestBody(getAndroidId(activity))

//        map.put("product_id", productId)
        map["qty"] = getStringRequestBody(qty)
        map["product_type"] = getStringRequestBody("selfie")
        map["product_id"] = getIntRequestBody(productId)
        map["colorcode_id"] = getStringRequestBody(colorId)
        map["selfy_image\"; filename=\"" + selfieImageFile!!.name + "\" "] = getImageRequestBody(selfieImageFile!!)

        val userIdForCart = SharedPref.getStringValue(activity, userId)
        if (userIdForCart != "")
            map["user_id"] = getStringRequestBody(userIdForCart)



        RetrofitClientSingleton.getInstance().addSelfieToCart(map).enqueue(object : retrofit2.Callback<AddToCartModel> {
            override fun onFailure(call: Call<AddToCartModel>, t: Throwable) {
                // toast(activity, getString(R.string.something_went_wrong))
                stopProgress()
                toast(activity, getString(R.string.something_went_wrong))
//                showSomethingWentWrongDataLayout(view.llParentNoData)
            }

            override fun onResponse(call: Call<AddToCartModel>, response: Response<AddToCartModel>) {
                stopProgress()

                if (response.isSuccessful) {
                    val selfieDataResponse = response.body()
                    when (selfieDataResponse?.status) {
                        SUCCESS -> {
                            if (dialog.isShowing)
                                dialog.dismiss()

                            SharedPref.setIntValue(activity, cartCount, selfieDataResponse.data!!.count)
                            SharedPref.setIntValue(activity, cartId, selfieDataResponse.data!!.cart_id)
                            toast(activity, selfieDataResponse.message)
                            onResume()
                        }
                        FAILURE -> {
                            toast(activity, selfieDataResponse.message)
//                            showSomethingWentWrongDataLayout(view.llParentNoData)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(activity, selfieDataResponse.message)
//                            showSomethingWentWrongDataLayout(view.llParentNoData)
                        }

                    }

                }
            }

        })

    }

    override fun onResume() {
        super.onResume()
        println("$TAG, onResume: ${::cartBadge.isInitialized}: ")
        println("$TAG, onResume: ${SharedPref.getIntValue(activity, cartCount)}: ")
        if (::cartBadge.isInitialized)
            setBadgeCount(activity, cartBadge, SharedPref.getIntValue(activity, cartCount).toString())

    }

    private fun chooseCameraOrGallery() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_choose_image)
        dialog.setTitle("")

        val camera = dialog.camera
        val gallery = dialog.gallery

        camera.setOnClickListener {
            dialog.dismiss()

            val mIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
            val file = File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg")
            val uri = FileProvider.getUriForFile(activity, activity.packageName + ".provider", file)
            mIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri)
            startActivityForResult(mIntent, REQUEST_CAMERA_IMAGE)
//
//                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//                intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri())
//                startActivityForResult(intent, REQUEST_IMAGE)
        }

        gallery.setOnClickListener {
            dialog.dismiss()
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_GALLERY_IMAGE)
        }

        dialog.show()
    }
}