package com.lalidea.fragments

import android.content.Intent
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.RelativeLayout
import com.lalidea.R
import com.lalidea.activities.ProductListActivity
import com.lalidea.activities.ShoppingCartActivity
import com.lalidea.adapters.HomeProductsAdapter
import com.lalidea.adapters.MoreFeaturesAdapter
import com.lalidea.adapters.SectionsPagerAdapter
import com.lalidea.models.HomeModel
import com.lalidea.models.MenuModel
import com.lalidea.models.MoreFeaturesModel
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.home_fragment.view.*
import kotlinx.android.synthetic.main.layout_no_data.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment() {

    var TAG: String = HomeFragment::class.java.simpleName
    private val moreFeaturesArrayList = ArrayList<MoreFeaturesModel>()
    private val ourProductsArrayList = ArrayList<HomeModel.DataBean.PostersBean>()
    private val arrayListNewArrival = ArrayList<HomeModel.DataBean.PostersBean>()
    private val arrayListNewTrendingFrames = ArrayList<HomeModel.DataBean.PostersBean>()
    private val arrayListIslamicArt = ArrayList<HomeModel.DataBean.PostersBean>()
    private var mMoreFeaturesAdapter: MoreFeaturesAdapter? = null

    private var viewFragment: View? = null

    lateinit var cartBadge: LayerDrawable

    var isLoadingData = false

    val imageWidth = 1600.0f
    val imageHeight = 599.0f
    var viewPageHeight = 0

    companion object {
        private val TYPE = "type"

        fun newInstance(type: Int): HomeFragment {
            val args = Bundle()
            args.putInt(TYPE, type)
            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.home_fragment, container, false)
        this.viewFragment = view
        setHasOptionsMenu(true)

        val displayMetrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        val widthRadio: Float = width / imageWidth


        viewPageHeight = (widthRadio * imageHeight).toInt()

        Log.d(TAG, "width = $width")
        Log.d(TAG, "widthRadio = $widthRadio")
        Log.d(TAG, "viewPageHeight = $viewPageHeight")

        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = viewPageHeight.toInt()//left, top, right, bottom

        view.vpBanner.layoutParams = params


        view.tvHomeViewMore.setOnClickListener {
            val intent = Intent(activity, ProductListActivity::class.java)
            intent.putExtra("Name", "Trending")
            intent.putExtra("CategoryId", "7")
            activity.startActivity(intent)
        }

        view.tvIslamicViewMore.setOnClickListener {
            val intent = Intent(activity, ProductListActivity::class.java)
            intent.putExtra("Name", "Islamic")
            intent.putExtra("CategoryId", "29")
            activity.startActivity(intent)
        }
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)

        val itemCart = menu.findItem(R.id.menuCart)
        cartBadge = itemCart.icon as LayerDrawable
        setBadgeCount(activity, cartBadge, SharedPref.getIntValue(activity, cartCount).toString())

        val searchItem: MenuItem = menu.findItem(R.id.menuSearch)

        val searchView = searchItem.actionView as SearchView

        searchView.setOnSearchClickListener {
            setItemsVisibility(menu, searchItem, false)
        }
        // Detect SearchView close
        searchView.setOnCloseListener {
            setItemsVisibility(menu, searchItem, true)
            false
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(txt: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.onActionViewCollapsed()
                setItemsVisibility(menu, searchItem, true)
                submitSearchQuery(activity, query)
                return false
            }

        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun setItemsVisibility(menu: Menu, exception: MenuItem,
                                   visible: Boolean) {
        for (i in 0 until menu.size()) {
            val item = menu.getItem(i)
            if (item !== exception)
                item.isVisible = visible
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menuSearch -> {

            }
            R.id.menuCart -> {
                if (SharedPref.getIntValue(activity, cartCount) > 0)
                    startActivity(Intent(activity, ShoppingCartActivity::class.java))
                else
                    toast(activity, R.string.no_item_in_cart)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun updateCartBadge() {
        println("$TAG, updateCartBadge: ${::cartBadge.isInitialized}: ")
        println("$TAG, updateCartBadge: ${SharedPref.getIntValue(activity, cartCount)}: ")
        if (::cartBadge.isInitialized)
            setBadgeCount(activity, cartBadge, SharedPref.getIntValue(activity, cartCount).toString())
    }

    private fun setMoreFeatureMockArray(view: View) {
        val title: ArrayList<String> = arrayListOf("Stand out",
                "Create your own",
                "Unlimited colours",
                "Cool Stationery",
                "Win Hearts",
                "Affordable")

        val titleInfo: ArrayList<String> = arrayListOf("Stand out",
                "Create your own",
                "Unlimited colours",
                "Cool Stationery",
                "Win Hearts",
                "Affordable")

        val description: ArrayList<String> = arrayListOf("Custom posters are a great way to stand out. Lalidea lets you take advantage of almost any open space.",
                "Turn your favourite digital photos into posters.",
                "Choose from a wide variety of colours and sizes based on your interests.",
                "Buy variety of cool stationery for yourself and your office space.",
                "Gift your loved ones awesome posters",
                "Keep a lookout for great deals on posters and other merchandise.")

        val thumb: ArrayList<Int> = arrayListOf(R.drawable.ic_paint_red,
                R.drawable.ic_smart_phone_red,
                R.drawable.ic_menu_red,
                R.drawable.ic_microscope_red,
                R.drawable.ic_bulb_red,
                R.drawable.ic_anchor_red)

        moreFeaturesArrayList.clear()
        for (item in 0..2) {
            val moreFeaturesModel = MoreFeaturesModel()
            moreFeaturesModel.thumb = thumb[item]
            moreFeaturesModel.title = title[item]
            moreFeaturesModel.description = description[item]
            moreFeaturesModel.titleInfo = titleInfo[item]
            moreFeaturesArrayList.add(moreFeaturesModel)
        }

        view.rvMoreFeatures.layoutManager = LinearLayoutManager(activity)
        mMoreFeaturesAdapter = MoreFeaturesAdapter(activity, moreFeaturesArrayList)
        //  view.rvMoreFeatures.adapter = mMoreFeaturesAdapter
        view.rvMoreFeatures.isNestedScrollingEnabled = false
    }

    private fun setPosters(view: View, ourproduct: ArrayList<HomeModel.DataBean.PostersBean>) {
        ourProductsArrayList.clear()
        ourProductsArrayList.addAll(ourproduct)
        view.rvHomePosters.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        view.rvHomePosters.adapter = HomeProductsAdapter(activity, ourProductsArrayList, typePosters)
        view.rvHomePosters.isNestedScrollingEnabled = false
    }

    private fun setNewArrival(view: View, ourproduct: ArrayList<HomeModel.DataBean.PostersBean>) {
        arrayListNewArrival.clear()
        arrayListNewArrival.addAll(ourproduct)
        view.rvHomeNewArrival.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        view.rvHomeNewArrival.adapter = HomeProductsAdapter(activity, arrayListNewArrival, typeNewArrival)
        view.rvHomeNewArrival.isNestedScrollingEnabled = false
    }

    private fun setTrendingFrames(view: View, ourproduct: ArrayList<HomeModel.DataBean.PostersBean>) {
        arrayListNewTrendingFrames.clear()
        arrayListNewTrendingFrames.addAll(ourproduct)
        view.rvTrendingFrames.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        view.rvTrendingFrames.adapter = HomeProductsAdapter(activity, arrayListNewTrendingFrames, typeTrendingFrames)
        view.rvTrendingFrames.isNestedScrollingEnabled = false
    }

    private fun setIslamicArt(view: View, ourproduct: ArrayList<HomeModel.DataBean.PostersBean>) {
        arrayListIslamicArt.clear()
        arrayListIslamicArt.addAll(ourproduct)
        view.rvIslamicFrames.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        view.rvIslamicFrames.adapter = HomeProductsAdapter(activity, arrayListIslamicArt, typeIsalmicArt)
        view.rvIslamicFrames.isNestedScrollingEnabled = false
    }

    override fun onResume() {
        super.onResume()
        updateCartBadge()
        if (viewFragment != null) {
            setMoreFeatureMockArray(viewFragment!!)
            if (!isNetworkAvailable(activity)) {
                viewFragment?.rlHomeParent!!.visibility = View.VISIBLE
                showNoInternetLayout(llParentNoData)
            } else {
                if (!isLoadingData)
                    getHomeData(viewFragment!!)
            }
        }
    }

    private fun getHomeData(view: View) {
        isLoadingData = true
        showProgressDialog(activity, view)
        val map = HashMap<String, String>()
        val userCartId = SharedPref.getIntValue(activity, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()
        else {
            map["cart_id"] = ""
        }
        map["device_id"] = getAndroidId(activity)
        val userIdForHome = SharedPref.getStringValue(activity, userId)
        if (userIdForHome != "")
            map["user_id"] = userIdForHome
        RetrofitClientSingleton.getInstance()
                .getHomeData(map).enqueue(object : retrofit2.Callback<HomeModel> {
                    override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                        viewFragment?.rlHomeParent!!.visibility = View.VISIBLE
                        isLoadingData = false
                        stopProgress()
                        toast(activity, getString(R.string.something_went_wrong))
                        showSomethingWentWrongDataLayout(llParentNoData)
                    }

                    override fun onResponse(call: Call<HomeModel>, response: Response<HomeModel>) {
                        viewFragment?.rlHomeParent!!.visibility = View.VISIBLE
                        isLoadingData = false
                        stopProgress()
                        if (response.isSuccessful) when (response.body()?.status) {
                            SUCCESS -> {
                                val homeResponse = response.body()

                                if (homeResponse!!.data!![0].count == null || homeResponse.data!![0].cart_id == null) {
                                    SharedPref.setIntValue(activity, cartCount, 0)
                                    SharedPref.setIntValue(activity, cartId, 0)
                                } else {
                                    SharedPref.setIntValue(activity, cartCount, homeResponse.data!![0].count!!.toInt())
                                    SharedPref.setIntValue(activity, cartId, homeResponse.data!![0].cart_id!!)
                                }

                                updateCartBadge()

                                val mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager, homeResponse.data!![0].slideshowImage, viewPageHeight)
                                //        view.vpBanner.setPageTransformer(false, FadePageTransformer())
                                view.vpBanner.adapter = mSectionsPagerAdapter
                                view.ivBannerLeftArrow.setOnClickListener {
                                    view.vpBanner.currentItem = view.vpBanner.currentItem - 1
                                }

                                view.ivBannerRightArrow.setOnClickListener {
                                    view.vpBanner.currentItem = view.vpBanner.currentItem + 1
                                }

                                setPosters(view, homeResponse.data!![0].posters!!)
                                setTrendingFrames(view, homeResponse.data!![0].frame!!)
                                setNewArrival(view, homeResponse.data!![0].new_arrival!!)
                                setIslamicArt(view, homeResponse.data!![0].islamic_art!!)
                            }

                            FAILURE -> {
                                toast(activity, response.body()!!.message)
                                showSomethingWentWrongDataLayout(llParentNoData)
                            }

                            ORDER_ALREADY_PLACED -> {
                                toast(activity, response.body()!!.message)
                                showSomethingWentWrongDataLayout(llParentNoData)
                            }
                        } else {
                            toast(activity, R.string.something_went_wrong)
                            showSomethingWentWrongDataLayout(llParentNoData)
                        }
                    }
                })
    }
}