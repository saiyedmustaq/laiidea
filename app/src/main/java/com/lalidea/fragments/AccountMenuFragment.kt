package com.lalidea.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lalidea.R
import com.lalidea.adapters.AccountMenuAdapter
import kotlinx.android.synthetic.main.account_menu_fragment.*
import java.util.*

class AccountMenuFragment : Fragment() {
    var TAG: String = AccountMenuFragment::class.java.simpleName
    private var account: ArrayList<String>? = null

    companion object {
        private val TYPE = "type"

        fun newInstance(type: Int): AccountMenuFragment {
            val args = Bundle()
//            args.putSerializable(TYPE, type)
            args.putInt(TYPE, type)
            val fragment = AccountMenuFragment()
            fragment.arguments = args
            return fragment
        }
    }
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.account_menu_fragment, container, false)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setList()


    }
    private fun setList() {
        account = ArrayList()
       // account?.add(getString(R.string.account_dash))
        account?.add(getString(R.string.account_info))
        account?.add(getString(R.string.add_book))
        account?.add(getString(R.string.my_orders))
        account?.add(getString(R.string.my_reviews))
        account?.add(getString(R.string.wishlist))
        account?.add(getString(R.string.recently_viewed_products))

        account?.add(getString(R.string.news_letter))


        rvAccount.layoutManager = LinearLayoutManager(activity)

        rvAccount.addItemDecoration(DividerItemDecoration(activity,
                DividerItemDecoration.VERTICAL))
        rvAccount.adapter = AccountMenuAdapter(activity, account!!)
    }

}