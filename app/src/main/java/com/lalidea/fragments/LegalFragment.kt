package com.lalidea.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.adapters.LegalAdapter
import com.lalidea.models.FooterModel
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.legal_fragment.*
import kotlinx.android.synthetic.main.legal_fragment.view.*
import retrofit2.Call
import retrofit2.Response

class LegalFragment : Fragment() {
    var TAG: String = LegalFragment::class.java.simpleName
    private var arrayListLegal: ArrayList<String> = ArrayList()

    companion object {
        fun newInstance(): LegalFragment {
            return LegalFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.legal_fragment, container, false)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setList()

        footerData(view)
    }

    private fun setList() {
        arrayListLegal.clear()
        arrayListLegal.add(getString(R.string.privacy_policy))
        arrayListLegal.add(getString(R.string.refund_cancellation))
        arrayListLegal.add(getString(R.string.terms_and_conditions))

        rvLegal.layoutManager = LinearLayoutManager(activity)

        rvLegal.addItemDecoration(DividerItemDecoration(activity,
                DividerItemDecoration.VERTICAL))
        rvLegal.adapter = LegalAdapter(activity, arrayListLegal)
    }


    private fun footerData(view: View) {
        showProgressDialog(activity, rvLegal)

        RetrofitClientSingleton.getInstance().footerData().enqueue(object : retrofit2.Callback<FooterModel> {
            override fun onFailure(call: Call<FooterModel>?, t: Throwable?) {
                stopProgress()
                toast(activity, R.string.something_went_wrong)
            }

            override fun onResponse(call: Call<FooterModel>?, response: Response<FooterModel>) {
                stopProgress()
                if (response.isSuccessful) {
                    val responseProductCategory = response.body()
                    when (responseProductCategory?.status) {
                        SUCCESS -> {
                            view.tvCopyRights.text = "\u00a9 ${response.body()?.data?.copyright}"
                            Glide.with(view.context)
                                    .load(response.body()?.data?.payment)
                                    .dontAnimate()
                                    .into(view.ivPaymentOptions)

                        }

                        FAILURE -> {
                            toast(activity, responseProductCategory.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(activity, responseProductCategory.message)
                        }

                    }

                } else {
                    toast(activity, R.string.something_went_wrong)
                }
            }
        })
    }
}