package com.lalidea.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SizeReadyCallback
import com.lalidea.R
import kotlinx.android.synthetic.main.banner_fragment.view.*

class HomeBannerFragment : Fragment() {

    val TAG = HomeFragment::class.java.simpleName
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val slideshowImage = arguments.getString(ARG_SLIDE_SHOW)
        val imageHeight = arguments.getInt(ARG_SLIDE_VIEW_PAGER_HEIGHT)

        val rootView = inflater!!.inflate(R.layout.banner_fragment, container, false)

        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        params.height = imageHeight//left, top, right, bottom
        params.width= ViewGroup.LayoutParams.MATCH_PARENT


        rootView.ivBanner.layoutParams = params
        Glide.with(this)
                .load(slideshowImage)
                .fitCenter()
                .dontAnimate()
                .placeholder(R.drawable.ic_place_holder)
                .error(R.drawable.ic_place_holder)
                .into(rootView.ivBanner)
//                .getSize({ width, height ->
//                    Log.e(TAG, "width = $width , height = $height ")
//                })
        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the section number for this fragment.
         */
        private val ARG_SECTION_NUMBER = "section_number"
        private val ARG_SLIDE_SHOW = "slideshowImageBean"
        private val ARG_SLIDE_VIEW_PAGER_HEIGHT = "viewPagerHeight"

        /**
         * Returns a new instance of this fragment for the given section number.
         */
        fun newInstance(sectionNumber: Int, slideshowImage: String, viewPageHeight: Int): HomeBannerFragment {
            val fragment = HomeBannerFragment()
            val args = Bundle()
            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            args.putString(ARG_SLIDE_SHOW, slideshowImage)
            args.putInt(ARG_SLIDE_VIEW_PAGER_HEIGHT, viewPageHeight)
            fragment.arguments = args
            return fragment
        }
    }
}