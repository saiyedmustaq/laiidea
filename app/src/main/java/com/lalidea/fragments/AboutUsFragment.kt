package com.lalidea.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import com.lalidea.R
import com.lalidea.models.SuccessModel
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.browser_fragment.view.*
import retrofit2.Call
import retrofit2.Response

class AboutUsFragment : Fragment() {
    var TAG: String = AboutUsFragment::class.java.simpleName
    private var progDailog: ProgressDialog? = null

    companion object {
        fun newInstance(): AboutUsFragment {
            return AboutUsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.browser_fragment, container, false)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getDataFromService(view)

    }

    private fun getDataFromService(view: View) {

        showProgressDialog(activity, view)

        RetrofitClientSingleton.getInstance().getBrowserData("aboutus").enqueue(object :
                retrofit2.Callback<SuccessModel> {

            override fun onResponse(call: Call<SuccessModel>, response: Response<SuccessModel>) {
                stopProgress()
                if (response.isSuccessful) {

                    val browserResponse = response.body()

                    when (browserResponse?.status) {
                        SUCCESS -> {
                            setView(view, decodeBase64(browserResponse.data!!))
                        }

                        FAILURE -> {
                            toast(activity, browserResponse.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(activity, browserResponse.message)
                        }
                    }

                } else {
                    stopProgress()
                }
            }

            override fun onFailure(call: Call<SuccessModel>, error: Throwable) {
                stopProgress()
                Log.e(TAG, "onFailure ${error.message}")
                toast(activity, error.message)
            }
        })

    }

    private fun setView(view: View, webData: String) {

        progDailog = ProgressDialog(activity)
        progDailog?.setMessage(getString(R.string.please_wait))
        progDailog?.setCancelable(false)

        view.wvBrowser.webViewClient = MyWebViewClient()
        view.wvBrowser.webChromeClient = MyChromeClient()
        view.wvBrowser.settings.javaScriptEnabled = true

        view.wvBrowser.settings.loadWithOverviewMode = true
        view.wvBrowser.settings.useWideViewPort = true
        view.wvBrowser.settings.builtInZoomControls = true

        view.wvBrowser.settings.setSupportZoom(true)
        view.wvBrowser.settings.javaScriptCanOpenWindowsAutomatically = true
        view.wvBrowser.settings.allowFileAccess = true
        view.wvBrowser.settings.domStorageEnabled = true
        view.wvBrowser.settings.textZoom = 250

        view.wvBrowser.loadData(webData, "text/html; charset=utf-8", "UTF-8");
    }

    private inner class MyWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest?): Boolean {

            Log.d(TAG, "shouldOverrideUrlLoading : url >> ${view.url}")
            // view.loadUrl(url)
            return super.shouldOverrideUrlLoading(view, request)
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageStarted(view, url, null)
            Log.d(TAG, "onPageFinished : url >> $url")
        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            Log.d(TAG, "onReceivedError : error >> $error")
            super.onReceivedError(view, request, error)
        }
    }

    inner class MyChromeClient : WebChromeClient() {
        override fun onPermissionRequest(request: PermissionRequest) {
            activity.runOnUiThread({
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    request.grant(request.resources)
                }
            })
        }

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)
            if (newProgress == 100) {
                if (progDailog != null) {
                    progDailog?.dismiss()
                }

            } else {
                if (progDailog != null) {
                    progDailog?.show()
                }
            }
        }
    }


}