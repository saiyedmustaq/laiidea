package com.lalidea.fragments

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.lalidea.R
import com.lalidea.models.StatusMessageResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.dialog_get_invoice.*
import kotlinx.android.synthetic.main.fragment_contact_us.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class ContactUsFragment : Fragment(), View.OnClickListener {

    var TAG: String = ContactUsFragment::class.java.simpleName

    companion object {
        fun newInstance(): ContactUsFragment {
            val args = Bundle()
            val fragment = ContactUsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_contact_us, container, false)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()


    }

    private fun setViews() {
        val uid = SharedPref.getStringValue(activity, userId)

        if (uid != "") {
            val name = SharedPref.getStringValue(activity, firstname) +
                    " " + SharedPref.getStringValue(activity, lastname)
            edtName.setText(name)
            edtEmailAddress.setText(SharedPref.getStringValue(activity, email))
        }

        btnContactus.setOnClickListener(this)

    }

    override fun onClick(view: View?) {

        when (view?.id) {

            R.id.btnContactus -> {
                checkValidations()
            }

            else -> {
            }
        }
    }

    private fun checkValidations() {
        if (edtName.text.toString().trim().isEmpty()) {
            toast(activity, getString(R.string.enter_name))
            return
        } else if (edtEmailAddress.text.toString().trim().isEmpty()) {
            toast(activity, getString(R.string.enter_email))
            return
        } else if (!isValidMail(edtEmailAddress.text.toString().trim())) {
            toast(activity, getString(R.string.enter_valid_email))
            return
        } else if (!edttelephone.text.toString().trim().isEmpty()
                && edttelephone.text.toString().trim().length != 10) {
            toast(activity, getString(R.string.enter_tel_valid))
            return

        } else if (edtcomment.text.toString().trim().isEmpty()) {
            toast(activity, getString(R.string.enter_comment))
            return
        }

        callApi()
    }

    private fun callApi() {
        val map = HashMap<String, String>()

        map["name"] = edtName.text.toString().trim()
        map["email"] = edtEmailAddress.text.toString().trim()
        if (!edttelephone.text.toString().trim().isEmpty()) {
            map["telephone"] = edttelephone.text.toString().trim()
        }
        map["comment"] = edtcomment.text.toString().trim()

        showProgressDialog(activity, btnContactus as View)
        val apiService = RetrofitClientSingleton.getInstance()
        apiService.contactUs(map).enqueue(object : retrofit2.Callback<StatusMessageResponse> {
            override fun onFailure(call: Call<StatusMessageResponse>, t: Throwable) {
                stopProgress()
                toast(activity, getString(R.string.something_went_wrong))

            }

            override fun onResponse(call: Call<StatusMessageResponse>?, response: Response<StatusMessageResponse>) {
                stopProgress()
                if (response.isSuccessful) {
                    val statusMsgResponse = response.body()
                    when (statusMsgResponse?.status) {
                        SUCCESS -> {

                            val dialog = Dialog(activity)
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            dialog.setContentView(R.layout.dialog_get_invoice)
                            dialog.tvMessage.text = statusMsgResponse.message
                            dialog.show()

                            dialog.txtOk.setOnClickListener({
                                dialog.dismiss()
                            })
                            edtName.setText("")
                            edtEmailAddress.setText("")
                            edttelephone.setText("")
                            edtcomment.setText("")
                        }

                        FAILURE -> {
                            toast(activity, statusMsgResponse.message)
                        }

                    }

                }
            }
        })
    }
}