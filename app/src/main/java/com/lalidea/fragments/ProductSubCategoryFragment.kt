package com.lalidea.fragments

import android.content.Intent
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.view.inputmethod.EditorInfo
import com.lalidea.R
import com.lalidea.activities.ShoppingCartActivity
import com.lalidea.adapters.ProductSubCategoryAdapter
import com.lalidea.models.ProductSubCategoryModel
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.layout_no_data.*
import kotlinx.android.synthetic.main.product_sub_type_fragment.view.*
import retrofit2.Call
import retrofit2.Response

class ProductSubCategoryFragment : Fragment() {
    var TAG: String = ProductSubCategoryFragment::class.java.simpleName

    private val mArrayListProductSubTypes = ArrayList<ProductSubCategoryModel.DataBean>()
    lateinit var mProductSubTypeAdapter: ProductSubCategoryAdapter
    var mView: View? = null
    var type: Int = 0
    lateinit var cartBadge: LayerDrawable

    companion object {
        private val TYPE = "type"

        fun newInstance(type: Int): ProductSubCategoryFragment {
            val args = Bundle()
            args.putInt(TYPE, type)
            val fragment = ProductSubCategoryFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        type = arguments.getInt(TYPE, 0)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater!!.inflate(R.layout.product_sub_type_fragment, container, false)
        setHasOptionsMenu(true)
        return mView
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)

        val itemCart = menu.findItem(R.id.menuCart)
        cartBadge = itemCart.icon as LayerDrawable
        setBadgeCount(activity, cartBadge, SharedPref.getIntValue(activity, cartCount).toString())

        val searchItem: MenuItem = menu.findItem(R.id.menuSearch)


        val searchView = searchItem.actionView as SearchView
        searchView.imeOptions = EditorInfo.IME_ACTION_SEARCH

        searchView.setOnSearchClickListener { setItemsVisibility(menu, searchItem, false) }
        // Detect SearchView close
        searchView.setOnCloseListener {
            setItemsVisibility(menu, searchItem, true)
            false
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(txt: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.isIconified = true
                searchView.onActionViewCollapsed()
                setItemsVisibility(menu, searchItem, true)
                submitSearchQuery(activity, query)
                return false
            }

        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun setItemsVisibility(menu: Menu, exception: MenuItem,
                                   visible: Boolean) {
        for (i in 0 until menu.size()) {
            val item = menu.getItem(i)
            if (item !== exception)
                item.isVisible = visible
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menuSearch -> {

            }
            R.id.menuCart -> {
                if (SharedPref.getIntValue(activity, cartCount) > 0)
                    startActivity(Intent(activity, ShoppingCartActivity::class.java))
                else
                    toast(activity, R.string.no_item_in_cart)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.rvProductSubType.layoutManager = LinearLayoutManager(activity)
        mProductSubTypeAdapter = ProductSubCategoryAdapter(activity, mArrayListProductSubTypes)
        view.rvProductSubType.adapter = mProductSubTypeAdapter
        view.rvProductSubType.isNestedScrollingEnabled = false
        view.rvProductSubType.addItemDecoration(DividerItemDecoration(activity,
                DividerItemDecoration.VERTICAL))
        if (isNetworkAvailable(activity))
            getProductSubTypeArray(mView!!)
        else {
            showNoInternetLayout(llParentNoData)
        }
    }

    private fun getProductSubTypeArray(view: View) {

        if (mArrayListProductSubTypes.isEmpty()) {
            showProgressDialog(activity, view)
            val productType = when (type) {
                POSTERS -> getPostersSubcat
                FRAMES -> getFramesSubcat
                STATIONERY -> getStationerySubcat
                ACCESSORIES -> getAccessoriesSubcat
                ISLAMICART -> getIslamicSubcat
                WALLART -> getWallArt
                CORPORATE_TROPHY -> getCorporateTrophy
                else -> getPostersSubcat
            }

            RetrofitClientSingleton.getInstance().getProductCategory(productType).enqueue(object : retrofit2.Callback<ProductSubCategoryModel> {
                override fun onFailure(call: Call<ProductSubCategoryModel>?, t: Throwable?) {
                    stopProgress()
                    println("$TAG : getProductCategory : onFailure >> $t")
                    toast(activity, R.string.something_went_wrong)
                    showSomethingWentWrongDataLayout(llParentNoData)
                }

                override fun onResponse(call: Call<ProductSubCategoryModel>?, response: Response<ProductSubCategoryModel>) {
                    stopProgress()
                    if (response.isSuccessful) {
                        val responseProductCategory = response.body()
                        when (responseProductCategory?.status) {
                            SUCCESS -> {
                                val productCategoryList = ArrayList<ProductSubCategoryModel.DataBean>()
                                if (responseProductCategory.data != null) {
                                    if (!responseProductCategory.data!!.isEmpty()) {
                                        productCategoryList.addAll(responseProductCategory.data!!)
                                        mProductSubTypeAdapter.setList(productCategoryList)
                                    } else {
                                        showNoDataLayout(llParentNoData, "No categories!")
                                        toast(activity, "No categories!")
                                    }
                                } else {
                                    showSomethingWentWrongDataLayout(llParentNoData)
                                }

                            }

                            FAILURE -> {
                                toast(activity, responseProductCategory.message)
                                showSomethingWentWrongDataLayout(llParentNoData)
                            }

                        }

                    }
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()
        println("$TAG, onResume: ${::cartBadge.isInitialized}: ")
        println("$TAG, onResume: ${SharedPref.getIntValue(activity, cartCount)}: ")
        if (::cartBadge.isInitialized)
            setBadgeCount(activity, cartBadge, SharedPref.getIntValue(activity, cartCount).toString())

    }
}