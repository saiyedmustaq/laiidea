package com.lalidea.listeners

/**
 * Created by kashifa on 12/12/17.
 */
interface OnProductRemoveFromCompare {
    fun onItemClicked(pos: Int)

    fun onCompareRemove(pos:Int)

    fun onCompare_addtocart(pos: Int)

    fun onCompare_addtoWishlist(pos: Int)
}