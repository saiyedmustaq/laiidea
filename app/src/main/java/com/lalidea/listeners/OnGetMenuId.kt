package com.lalidea.listeners

import com.lalidea.models.MenuModel

interface OnGetMenuId{
    fun getMenuId(categoryId: String?, categoryName: String?, subcategory: ArrayList<MenuModel.DataBean.CategoryBean.SubCategory>?)
    fun onClicMenu(categoryId: String?, categoryName: String?)

}
