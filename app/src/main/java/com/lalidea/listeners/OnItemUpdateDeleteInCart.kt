package com.lalidea.listeners

interface OnItemUpdateDeleteInCart {
    fun onItemDelete(pos:Int)
    fun onItemUpdate(pos:Int)
}
