package com.lalidea.listeners

interface OnItemUpdateDeleteAddWishList {
    fun onItemDelete(pos:Int)
    fun onItemUpdate(pos:Int)
    fun onItemAddToCart(pos:Int)

}
