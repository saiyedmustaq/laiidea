package com.lalidea.listeners

interface OnClickAdapter {
    fun onClick(pos: Int)
    fun onClickSelect(strItemName: String, pos: Int)
}
