package com.lalidea.listeners

import com.lalidea.models.ProductDataModel


interface OnProductItemClickListener {
    fun onAddToCartClicked(productData: ProductDataModel)
    fun onAddToShareClicked(productData: ProductDataModel)
    fun onAddToWishListClicked(productData: ProductDataModel)
    fun onItemClicked(productData: ProductDataModel)
}