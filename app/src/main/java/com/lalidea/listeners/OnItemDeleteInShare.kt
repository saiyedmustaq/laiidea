package com.lalidea.listeners

interface OnItemDeleteInShare {
    fun onItemDelete(pos: Int)
}
