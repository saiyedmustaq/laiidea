package com.lalidea.listeners

import com.lalidea.models.SelfieModel

interface OnColorSelectedListener {
    fun onColorSelected(colorBean: SelfieModel.DataBean.ColorBean)
}
