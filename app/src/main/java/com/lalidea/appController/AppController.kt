package com.lalidea.appController

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.lalidea.R
import com.lalidea.database.databaseManager.DatabaseManager

class AppController : Application() {

    companion object {
        var database: DatabaseManager? = null
        var appInstance: AppController? = null
    }

    override fun onCreate() {
        super.onCreate()
        AppController.database = Room.databaseBuilder(this,
                DatabaseManager::class.java,
                getString(R.string.app_name) + ".db")
                .build()
        appInstance = this
    }

    fun getAppInstace(): Context {
        if (appInstance != null)
            appInstance = this
        return appInstance!!.applicationContext
    }


}