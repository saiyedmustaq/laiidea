package com.lalidea.models

import java.io.Serializable

class WishListModel : Serializable {
    var id: Int? = null
    var name: String? = null
    var description: String? = null
    var qty: String? = null
    var subTotal: String? = null
    var unitPrice: String? = null
    var thumb: String? = null
}
