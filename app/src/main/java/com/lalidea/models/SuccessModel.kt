package com.lalidea.models

/**
 * Created by malik on 8/1/18.
 */

class SuccessModel {

    /**
     * status : 1
     * message : Lalidea About Us Page
     * data : PGRpdiBjbGFzcz0icGFnZS10aXRsZSI+DQo8cCBkaXI9Imx0ciI+PHNwYW4+TGFsaWRlYSBpcyB5b3VyIGdvLXRvIGRlc3RpbmF0aW9uIGZvciBzaG9wcGluZyB3YWxsIGFydCBhbmQgb3RoZXIgZnVuIHByb2R1Y3RzIGV4cHJlc3NpbmcgJm5ic3A7eW91ciBwZXJzb25hbCBpbnRlcmVzdHMsIGxpZmUtbG9uZyBwYXNzaW9ucyBhbmQgb2YtdGhlLW1vbWVudCBvYnNlc3Npb25zLiA8L3NwYW4+PC9wPg0KPHAgZGlyPSJsdHIiPjxzcGFuPldpdGggYSB3aWRlIHJhbmdlIG9mIHNlbGVjdGlvbiBvZiBQb3N0ZXJzLCBsYWxpZGVhIGhhcyBzb21ldGhpbmcgZm9yIGV2ZXJ5b25lIGFuZCB0aGVpciB1bmlxdWUgZGVjb3JhdGluZyBzdHlsZS4gRmluZCB5b3VyIGZhdm91cml0ZSBhcnQgcHJpbnRzIGZyb20gY2xhc3NpYyBtYXN0ZXJzIHRvIHlvdXIgZmF2b3VyaXRlIHdlYiBzZXJpZXMuIEJyb3dzZSB0aGUgaG90dGVzdCBwb3N0ZXJzIGluIG11c2ljLCBtb3ZpZXMgYW5kIHNwb3J0cy4gWW91IGNhbiBldmVuIHR1cm4geW91ciBvd24gc2VsZmllIGludG8gYW4gYXJ0IG1hc3RlcnBpZWNlIHdpdGggbGFsaWRlYS4gUGx1cywgY2hvb3NlIGZyb20gdmFyaW91cyBmcmFtZXMgdG8gdHJ1bHkgbWFrZSB0aGUgcGllY2UgeW91ciBvd24gJm5kYXNoOyBhbGwgaGlnaC1xdWFsaXR5IGF0IGFtYXppbmcgcHJpY2VzLjwvc3Bhbj48L3A+DQo8cD48c3Bhbj4mbmJzcDs8L3NwYW4+PC9wPg0KPC9kaXY+
     */

    var status: Int = 0
    var message: String? = null
    var data: String? = null
}
