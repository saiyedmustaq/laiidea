package com.lalidea.models

/**
 * Created by rahil on 29/12/17.
 */

class PlaceOrderId {

    /**
     * status : 1
     * message : Your order has been received.
     * data : {"order_id":"100000053","order_detail":{"order_id":"53","created_at":"2017-12-28 06:43:13","billingAddress":{"entity_id":"105","parent_id":"53","customer_address_id":"37","quote_address_id":null,"region_id":null,"customer_id":"3","fax":"34433434","region":"ZCZXC","postcode":"435543","lastname":"LA","street":"VGCXG","city":"ZXZXC","email":"rahil@rahil.com","telephone":"343456767788788","country_id":"IN","firstname":"RAhil","address_type":"billing","prefix":null,"middlename":"J","suffix":null,"company":"ATT","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null},"shippingAddress":{"entity_id":"106","parent_id":"53","customer_address_id":"37","quote_address_id":null,"region_id":null,"customer_id":"3","fax":"34433434","region":"ZCZXC","postcode":"435543","lastname":"LA","street":"VGCXG","city":"ZXZXC","email":"rahil@rahil.com","telephone":"343456767788788","country_id":"IN","firstname":"RAhil","address_type":"shipping","prefix":null,"middlename":"J","suffix":null,"company":"ATT","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null},"shipping_method":"Free Shipping - Free","payment_method":"PayU Money","product":[{"product_id":"22","product_name":"Pacon Sketch Diary","product_sku":"diary1","product_price":"Rs. 111.00","qty":"2.0000","subtotal":"Rs. 222.00"},{"product_id":"34","product_name":"WOODEN FRAME A4 BLACK","product_sku":"a4frame1","product_price":"Rs. 1.00","qty":"1.0000","subtotal":"Rs. 1.00"}],"subtotal":"Rs. 223.00","shipping_amount":"Rs. 0.00","grand_total":"Rs. 223.00"}}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * order_id : 100000053
         * order_detail : {"order_id":"53","created_at":"2017-12-28 06:43:13","billingAddress":{"entity_id":"105","parent_id":"53","customer_address_id":"37","quote_address_id":null,"region_id":null,"customer_id":"3","fax":"34433434","region":"ZCZXC","postcode":"435543","lastname":"LA","street":"VGCXG","city":"ZXZXC","email":"rahil@rahil.com","telephone":"343456767788788","country_id":"IN","firstname":"RAhil","address_type":"billing","prefix":null,"middlename":"J","suffix":null,"company":"ATT","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null},"shippingAddress":{"entity_id":"106","parent_id":"53","customer_address_id":"37","quote_address_id":null,"region_id":null,"customer_id":"3","fax":"34433434","region":"ZCZXC","postcode":"435543","lastname":"LA","street":"VGCXG","city":"ZXZXC","email":"rahil@rahil.com","telephone":"343456767788788","country_id":"IN","firstname":"RAhil","address_type":"shipping","prefix":null,"middlename":"J","suffix":null,"company":"ATT","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null},"shipping_method":"Free Shipping - Free","payment_method":"PayU Money","product":[{"product_id":"22","product_name":"Pacon Sketch Diary","product_sku":"diary1","product_price":"Rs. 111.00","qty":"2.0000","subtotal":"Rs. 222.00"},{"product_id":"34","product_name":"WOODEN FRAME A4 BLACK","product_sku":"a4frame1","product_price":"Rs. 1.00","qty":"1.0000","subtotal":"Rs. 1.00"}],"subtotal":"Rs. 223.00","shipping_amount":"Rs. 0.00","grand_total":"Rs. 223.00"}
         */

        var order_id: String? = null
        var order_detail: OrderDetailBean? = null

        class OrderDetailBean {
            /**
             * order_id : 53
             * created_at : 2017-12-28 06:43:13
             * billingAddress : {"entity_id":"105","parent_id":"53","customer_address_id":"37","quote_address_id":null,"region_id":null,"customer_id":"3","fax":"34433434","region":"ZCZXC","postcode":"435543","lastname":"LA","street":"VGCXG","city":"ZXZXC","email":"rahil@rahil.com","telephone":"343456767788788","country_id":"IN","firstname":"RAhil","address_type":"billing","prefix":null,"middlename":"J","suffix":null,"company":"ATT","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null}
             * shippingAddress : {"entity_id":"106","parent_id":"53","customer_address_id":"37","quote_address_id":null,"region_id":null,"customer_id":"3","fax":"34433434","region":"ZCZXC","postcode":"435543","lastname":"LA","street":"VGCXG","city":"ZXZXC","email":"rahil@rahil.com","telephone":"343456767788788","country_id":"IN","firstname":"RAhil","address_type":"shipping","prefix":null,"middlename":"J","suffix":null,"company":"ATT","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null}
             * shipping_method : Free Shipping - Free
             * payment_method : PayU Money
             * product : [{"product_id":"22","product_name":"Pacon Sketch Diary","product_sku":"diary1","product_price":"Rs. 111.00","qty":"2.0000","subtotal":"Rs. 222.00"},{"product_id":"34","product_name":"WOODEN FRAME A4 BLACK","product_sku":"a4frame1","product_price":"Rs. 1.00","qty":"1.0000","subtotal":"Rs. 1.00"}]
             * subtotal : Rs. 223.00
             * shipping_amount : Rs. 0.00
             * grand_total : Rs. 223.00
             */

            var order_id: String? = null
            var created_at: String? = null
            var billingAddress: BillingAddressBean? = null
            var shippingAddress: ShippingAddressBean? = null
            var shipping_method: String? = null
            var payment_method: String? = null
            var subtotal: String? = null
            var shipping_amount: String? = null
            var grand_total: String? = null
            var product: List<ProductBean>? = null

            class BillingAddressBean {
                /**
                 * entity_id : 105
                 * parent_id : 53
                 * customer_address_id : 37
                 * quote_address_id : null
                 * region_id : null
                 * customer_id : 3
                 * fax : 34433434
                 * region : ZCZXC
                 * postcode : 435543
                 * lastname : LA
                 * street : VGCXG
                 * city : ZXZXC
                 * email : rahil@rahil.com
                 * telephone : 343456767788788
                 * country_id : IN
                 * firstname : RAhil
                 * address_type : billing
                 * prefix : null
                 * middlename : J
                 * suffix : null
                 * company : ATT
                 * vat_id : null
                 * vat_is_valid : null
                 * vat_request_id : null
                 * vat_request_date : null
                 * vat_request_success : null
                 */

                var entity_id: String? = null
                var parent_id: String? = null
                var customer_address_id: String? = null
                var quote_address_id: Any? = null
                var region_id: Any? = null
                var customer_id: String? = null
                var fax: String? = null
                var region: String? = null
                var postcode: String? = null
                var lastname: String? = null
                var street: String? = null
                var city: String? = null
                var email: String? = null
                var telephone: String? = null
                var country_id: String? = null
                var firstname: String? = null
                var address_type: String? = null
                var prefix: Any? = null
                var middlename: String? = null
                var suffix: Any? = null
                var company: String? = null
                var vat_id: Any? = null
                var vat_is_valid: Any? = null
                var vat_request_id: Any? = null
                var vat_request_date: Any? = null
                var vat_request_success: Any? = null
            }

            class ShippingAddressBean {
                /**
                 * entity_id : 106
                 * parent_id : 53
                 * customer_address_id : 37
                 * quote_address_id : null
                 * region_id : null
                 * customer_id : 3
                 * fax : 34433434
                 * region : ZCZXC
                 * postcode : 435543
                 * lastname : LA
                 * street : VGCXG
                 * city : ZXZXC
                 * email : rahil@rahil.com
                 * telephone : 343456767788788
                 * country_id : IN
                 * firstname : RAhil
                 * address_type : shipping
                 * prefix : null
                 * middlename : J
                 * suffix : null
                 * company : ATT
                 * vat_id : null
                 * vat_is_valid : null
                 * vat_request_id : null
                 * vat_request_date : null
                 * vat_request_success : null
                 */

                var entity_id: String? = null
                var parent_id: String? = null
                var customer_address_id: String? = null
                var quote_address_id: Any? = null
                var region_id: Any? = null
                var customer_id: String? = null
                var fax: String? = null
                var region: String? = null
                var postcode: String? = null
                var lastname: String? = null
                var street: String? = null
                var city: String? = null
                var email: String? = null
                var telephone: String? = null
                var country_id: String? = null
                var firstname: String? = null
                var address_type: String? = null
                var prefix: Any? = null
                var middlename: String? = null
                var suffix: Any? = null
                var company: String? = null
                var vat_id: Any? = null
                var vat_is_valid: Any? = null
                var vat_request_id: Any? = null
                var vat_request_date: Any? = null
                var vat_request_success: Any? = null
            }

            class ProductBean {
                /**
                 * product_id : 22
                 * product_name : Pacon Sketch Diary
                 * product_sku : diary1
                 * product_price : Rs. 111.00
                 * qty : 2.0000
                 * subtotal : Rs. 222.00
                 */

                var product_id: String? = null
                var product_name: String? = null
                var product_sku: String? = null
                var product_price: String? = null
                var qty: String? = null
                var subtotal: String? = null
            }
        }
    }
}
