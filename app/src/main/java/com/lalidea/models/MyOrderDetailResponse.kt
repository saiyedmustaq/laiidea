package com.lalidea.models

import com.google.gson.JsonObject
import java.io.Serializable

/**
 * Created by kashifa on 27/12/17.
 */
class MyOrderDetailResponse : Serializable {


        /**
         * status : 1
         * message : Order Detail
         * data : {"order_id":"35","created_at":"2017-12-26 12:40:38",
         * "billingAddress":{"entity_id":"69","parent_id":"35","customer_address_id":null,"quote_address_id":null,"region_id":null,"customer_id":"3","fax":"ww434343","region":"gujarat","postcode":"380015","lastname":"ladhani","street":"bhambha street,st road 3211","city":"ahmedabad","email":null,"telephone":"079245526","country_id":"IN","firstname":"rahil","address_type":"billing","prefix":null,"middlename":null,"suffix":null,"company":"atttune  WORLDWIDE","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null},
         * "shippingAddress":{"entity_id":"70","parent_id":"35","customer_address_id":null,"quote_address_id":null,"region_id":null,"customer_id":"3","fax":"ww434343","region":"gujarat","postcode":"380015","lastname":"ladhani","street":"bhambha street,st road 3211","city":"ahmedabad","email":null,"telephone":"079245526","country_id":"IN","firstname":"rahil","address_type":"shipping","prefix":null,"middlename":null,"suffix":null,"company":"atttune  WORLDWIDE","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null},
         * "product":[{"product_id":"36","product_name":"Three Sisters","product_sku":"threesister","product_price":"Rs. 300.00","qty":"1.0000","subtotal":"Rs. 300.00"},{"product_id":"25","product_name":"2015 Wooden Critter Desk Calendar by Night Owl Paper Goods","product_sku":"cal2","product_price":"Rs. 786.00","qty":"1.0000","subtotal":"Rs. 786.00"}],"subtotal":"Rs. 1,086.00","shipping_amount":"Rs. 0.00","grand_total":"Rs. 1,086.00"}
         */

        var status: Int = 0
        var message: String? = null
        var data: DataBean? = null

        class DataBean {
            /**
             * order_id : 35
             * created_at : 2017-12-26 12:40:38
             * billingAddress : {"entity_id":"69","parent_id":"35","customer_address_id":null,"quote_address_id":null,"region_id":null,"customer_id":"3","fax":"ww434343","region":"gujarat","postcode":"380015","lastname":"ladhani","street":"bhambha street,st road 3211","city":"ahmedabad","email":null,"telephone":"079245526","country_id":"IN","firstname":"rahil","address_type":"billing","prefix":null,"middlename":null,"suffix":null,"company":"atttune  WORLDWIDE","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null}
             * shippingAddress : {"entity_id":"70","parent_id":"35","customer_address_id":null,"quote_address_id":null,"region_id":null,"customer_id":"3","fax":"ww434343","region":"gujarat","postcode":"380015","lastname":"ladhani","street":"bhambha street,st road 3211","city":"ahmedabad","email":null,"telephone":"079245526","country_id":"IN","firstname":"rahil","address_type":"shipping","prefix":null,"middlename":null,"suffix":null,"company":"atttune  WORLDWIDE","vat_id":null,"vat_is_valid":null,"vat_request_id":null,"vat_request_date":null,"vat_request_success":null}
             * product : [{"product_id":"36","product_name":"Three Sisters","product_sku":"threesister","product_price":"Rs. 300.00","qty":"1.0000","subtotal":"Rs. 300.00"},{"product_id":"25","product_name":"2015 Wooden Critter Desk Calendar by Night Owl Paper Goods","product_sku":"cal2","product_price":"Rs. 786.00","qty":"1.0000","subtotal":"Rs. 786.00"}]
             * subtotal : Rs. 1,086.00
             * shipping_amount : Rs. 0.00
             * grand_total : Rs. 1,086.00
             */

            var order_id: String? = null
            var created_at: String? = null
            var billingAddress: BillingAddressBean? = null
            var shippingAddress: ShippingAddressBean? = null
            var shipping_method: String? = null
            var payment_method: String? = null
            var subtotal: String? = null
            var shipping_amount: String? = null
            var grand_total: String? = null
            var discount: String? = null
            var product: ArrayList<ProductBean>? = null

            class BillingAddressBean {
                /**
                 * entity_id : 69
                 * parent_id : 35
                 * customer_address_id : null
                 * quote_address_id : null
                 * region_id : null
                 * customer_id : 3
                 * fax : ww434343
                 * region : gujarat
                 * postcode : 380015
                 * lastname : ladhani
                 * street : bhambha street,st road 3211
                 * city : ahmedabad
                 * email : null
                 * telephone : 079245526
                 * country_id : IN
                 * firstname : rahil
                 * address_type : billing
                 * prefix : null
                 * middlename : null
                 * suffix : null
                 * company : atttune  WORLDWIDE
                 * vat_id : null
                 * vat_is_valid : null
                 * vat_request_id : null
                 * vat_request_date : null
                 * vat_request_success : null
                 */

                var entity_id: String? = null
                var parent_id: String? = null
                var customer_address_id: Any? = null
                var quote_address_id: Any? = null
                var region_id: Any? = null
                var customer_id: String? = null
                var fax: String? = null
                var region: String? = null
                var postcode: String? = null
                var lastname: String? = null
                var street: String? = null
                var city: String? = null
                var email: Any? = null
                var telephone: String? = null
                var country_id: String? = null
                var firstname: String? = null
                var address_type: String? = null
                var prefix: Any? = null
                var middlename: Any? = null
                var suffix: Any? = null
                var company: String? = null
                var vat_id: Any? = null
                var vat_is_valid: Any? = null
                var vat_request_id: Any? = null
                var vat_request_date: Any? = null
                var vat_request_success: Any? = null
            }

            class ShippingAddressBean {
                /**
                 * entity_id : 70
                 * parent_id : 35
                 * customer_address_id : null
                 * quote_address_id : null
                 * region_id : null
                 * customer_id : 3
                 * fax : ww434343
                 * region : gujarat
                 * postcode : 380015
                 * lastname : ladhani
                 * street : bhambha street,st road 3211
                 * city : ahmedabad
                 * email : null
                 * telephone : 079245526
                 * country_id : IN
                 * firstname : rahil
                 * address_type : shipping
                 * prefix : null
                 * middlename : null
                 * suffix : null
                 * company : atttune  WORLDWIDE
                 * vat_id : null
                 * vat_is_valid : null
                 * vat_request_id : null
                 * vat_request_date : null
                 * vat_request_success : null
                 */

                var entity_id: String? = null
                var parent_id: String? = null
                var customer_address_id: Any? = null
                var quote_address_id: Any? = null
                var region_id: Any? = null
                var customer_id: String? = null
                var fax: String? = null
                var region: String? = null
                var postcode: String? = null
                var lastname: String? = null
                var street: String? = null
                var city: String? = null
                var email: Any? = null
                var telephone: String? = null
                var country_id: String? = null
                var firstname: String? = null
                var address_type: String? = null
                var prefix: Any? = null
                var middlename: Any? = null
                var suffix: Any? = null
                var company: String? = null
                var vat_id: Any? = null
                var vat_is_valid: Any? = null
                var vat_request_id: Any? = null
                var vat_request_date: Any? = null
                var vat_request_success: Any? = null
            }

            class ProductBean {
                /**
                 * product_id : 36
                 * product_name : Three Sisters
                 * product_sku : threesister
                 * product_price : Rs. 300.00
                 * qty : 1.0000
                 * subtotal : Rs. 300.00
                 */

                var product_id: String? = null
                var product_name: String? = null
                var product_sku: String? = null
                var product_price: String? = null
                var qty: String? = null
                var subtotal: String? = null
                var custom_options: JsonObject? = null
                var product_image: String? = null
            }
        }

}