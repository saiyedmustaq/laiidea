package com.lalidea.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by kashifa on 12/12/17.
 */
class Product : Serializable {

    @SerializedName("product_id")
    @Expose
    var productId: String? = null

    @SerializedName("product_name")
    @Expose
    var productName: String? = null

    @SerializedName("product_price")
    @Expose
    var productPrice: String? = null

    @SerializedName("special_price")
    @Expose
    var special_price: String? = null

    @SerializedName("product_shortdescription")
    @Expose
    var productShortDescription: String? = null

    @SerializedName("product_description")
    @Expose
    var productDescription: String? = null

    @SerializedName("product_small_image")
    @Expose
    var productSmallImage: String? = null

    @SerializedName("product_detail_image")
    @Expose
    var productDetailImage: String? = null

    @SerializedName("is_in_stock")
    @Expose
    var isInStock: Int? = null

    @SerializedName("gallery")
    @Expose
    var galleryData: ArrayList<GalleryData>? = null
}