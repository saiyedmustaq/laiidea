package com.lalidea.models

/**
 * Created by rahil on 21/12/17.
 */

class AddToWishListModel {


    /**
     * status : 1
     * message : Three Sisters added to your wishlist
     * data : [{"count":1}]
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * count : 1
         */

        var count: Int = 0

    }
}
