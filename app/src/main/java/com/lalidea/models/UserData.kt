package com.lalidea.models

import java.io.Serializable

/**
 * Created by kashifa on 12/12/17.
 */
class UserData:Serializable{
    var email:String?=null

    var customer_id:String?=null
    var is_active:String?=null
    var store_id:String?=null
    var firstname:String?=null
    var middlename:String?=null
    var lastname:String?=null

    var default_billing:String?=null
    var default_shipping:String?=null
    var cart_id:Int?=null
    var count:Int?=null
    var is_subscribed:Int?=null

}