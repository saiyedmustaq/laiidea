package com.lalidea.models

import java.io.Serializable

class MoreFeaturesModel : Serializable {
    var titleInfo: String? = null
    var title: String? = null
    var description: String? = null
    var thumb: Int? = null
}
