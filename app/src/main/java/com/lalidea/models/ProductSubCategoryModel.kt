package com.lalidea.models

/**
 * Created by malik on 12/12/17.
 */

class ProductSubCategoryModel {

    /**
     * status : 1
     * message : Posters Categories List
     * data : [{"id":"7","name":"Posters A4"},{"id":"8","name":"Posters 12x18"},{"id":"9","name":"Posters 18x24"}]
     */

    var status: Int = 0
    var message: String? = null
    var data: ArrayList<DataBean>? = null

    class DataBean {
        /**
         * id : 7
         * name : Posters A4
         */

        var id: String? = null
        var name: String? = null
    }
}
