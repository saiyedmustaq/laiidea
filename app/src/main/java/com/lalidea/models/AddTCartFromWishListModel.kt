package com.lalidea.models

/**
 * Created by rahil on 22/12/17.
 */

class AddTCartFromWishListModel {


    /**
     * status : 1
     * message : successfully tranfser wishlist to cart
     * data : [{"wishlistcount":1,"count":38}]
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * wishlistcount : 1
         * count : 38
         */

        var wishlistcount: Int = 0
        var count: Int = 0
    }
}
