package com.lalidea.models

import java.io.Serializable

/**
 * Created by kashifa on 12/12/17.
 */
class ProductsCompareModel:Serializable{
    var thumb:String?=null
    var producttitle: String? = null
    var price: String? = null
    var description: String? = null
    var shortdescription:String?=null
    var sku:String?=null
}