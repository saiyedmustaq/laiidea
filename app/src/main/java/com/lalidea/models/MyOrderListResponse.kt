package com.lalidea.models

/**
 * Created by rahil on 26/12/17.
 */

class MyOrderListResponse {

    /**
     * status : 1
     * message : Order List
     * data : [{"order_id":"18","print_order_id":"100000018","date":"2017-12-26 07:18:21","ship_to":"rahil ladhani","grand_total":"320.0000"},{"order_id":"16","print_order_id":"100000016","date":"2017-12-21 07:28:12","ship_to":"RAhil J LA","grand_total":"50.0000"},{"order_id":"14","print_order_id":"100000014","date":"2017-12-20 05:22:21","ship_to":"RAhil J LA","grand_total":"90.0000"},{"order_id":"13","print_order_id":"100000013","date":"2017-12-16 10:45:40","ship_to":"RAhil J LA","grand_total":"90.0000"}]
     */

    var status: Int = 0
    var message: String? = null
    var total_record: Int = 0
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * order_id : 18
         * print_order_id : 100000018
         * date : 2017-12-26 07:18:21
         * ship_to : rahil ladhani
         * grand_total : 320.0000
         */

        var order_id: String? = null
        var status: String? = null
        var print_order_id: String? = null
        var date: String? = null
        var ship_to: String? = null
        var grand_total: String? = null
    }
}
