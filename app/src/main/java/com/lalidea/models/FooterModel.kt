package com.lalidea.models

/**
 * Created by malik on 24/1/18.
 */

class FooterModel {
    /**
     * status : 1
     * message : Footer Data
     * data : {"copyright":"2018 All rights reserved.","payment":"https://www.lalidea.com/media/wysiwyg/demo/payment-new.gif"}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * copyright : 2018 All rights reserved.
         * payment : https://www.lalidea.com/media/wysiwyg/demo/payment-new.gif
         */

        var copyright: String? = null
        var payment: String? = null
    }
}
