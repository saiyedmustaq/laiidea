package com.lalidea.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by kashifa on 12/12/17.
 */
class ReviewsData : Serializable {


    @SerializedName("count")
    @Expose
    var count: String? = null

    @SerializedName("review")
    @Expose
    var review: ArrayList<Reviews>? = null

   /* @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("entity_id")
    @Expose
    var entityId: String? = null

    @SerializedName("entity_pk_value")
    @Expose
    var entityPkValue: String? = null

    @SerializedName("status_id")
    @Expose
    var statusId: String? = null

    @SerializedName("detail_id")
    @Expose
    var detailId: String? = null

    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("detail")
    @Expose
    var detail: String? = null

    @SerializedName("nickname")
    @Expose
    var nickName: String? = null

    @SerializedName("customer_id")
    @Expose
    var customerId: String? = null

    @SerializedName("entity_code")
    @Expose
    var entityCode: String? = null*/

}