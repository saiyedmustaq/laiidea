package com.lalidea.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by kashifa on 12/12/17.
 */
class ProductDataModel : Serializable {

    @SerializedName("product_id")
    @Expose
    var productId: String? = null

    @SerializedName("product_name")
    @Expose
    var productName: String? = null

    @SerializedName("product_price")
    @Expose
    var productPrice: String? = null

    @SerializedName("special_price")
    @Expose
    var special_price: String? = null

    @SerializedName("product_shortdescription")
    @Expose
    var productDescription: String? = null

    @SerializedName("product_small_image")
    @Expose
    var productSmallImage: String? = null

    @SerializedName("product_detail_image")
    @Expose
    var productDetailImage: String? = null

    @SerializedName("inStock")
    @Expose
    var isInStock: Int? = null

    @SerializedName("gallery")
    @Expose
    var gallery: ArrayList<GalleryData>? = null

}