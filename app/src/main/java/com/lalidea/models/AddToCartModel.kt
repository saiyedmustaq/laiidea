package com.lalidea.models

class AddToCartModel {

    /**
     * status : 1
     * message : Test was added to your shopping cart.
     * data : {"count":1}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * count : 1
         */

        var count: Int = 0
        var cart_id: Int = 0
    }
}
