package com.lalidea.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by kashifa on 12/12/17.
 */
class ProductDetailResponse : Serializable {

    var status: Int? = null
    var message: String? = null

    @SerializedName("data")
    @Expose
    var data: ProductDetailData? = null

}