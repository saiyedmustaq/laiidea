package com.lalidea.models

import com.google.gson.annotations.SerializedName

/**
 * Created by malik on 30/12/17.
 */

class SelfieModel {

    /**
     * status : 1
     * message : Product Detail
     * data : {"product_id":40,"getName":"Selfie Print","getPrice":"600.0000","getSpecialPrice":null,"Color":[{"option_type_id":"3","option_id":"4","sku":"#00000","sort_order":"0","default_title":"Black","store_title":null,"title":"Black","default_price":"0.0000","default_price_type":"fixed","store_price":null,"store_price_type":null,"price":"0.0000","price_type":"fixed"},{"option_type_id":"5","option_id":"4","sku":"#cccccc","sort_order":"0","default_title":"Grey","store_title":null,"title":"Grey","default_price":"0.0000","default_price_type":"fixed","store_price":null,"store_price_type":null,"price":"0.0000","price_type":"fixed"},{"option_type_id":"4","option_id":"4","sku":"#ffffff","sort_order":"0","default_title":"White","store_title":null,"title":"White","default_price":"0.0000","default_price_type":"fixed","store_price":null,"store_price_type":null,"price":"0.0000","price_type":"fixed"}]}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * product_id : 40
         * getName : Selfie Print
         * getPrice : 600.0000
         * getSpecialPrice : null
         * Color : [{"option_type_id":"3","option_id":"4","sku":"#00000","sort_order":"0","default_title":"Black","store_title":null,"title":"Black","default_price":"0.0000","default_price_type":"fixed","store_price":null,"store_price_type":null,"price":"0.0000","price_type":"fixed"},{"option_type_id":"5","option_id":"4","sku":"#cccccc","sort_order":"0","default_title":"Grey","store_title":null,"title":"Grey","default_price":"0.0000","default_price_type":"fixed","store_price":null,"store_price_type":null,"price":"0.0000","price_type":"fixed"},{"option_type_id":"4","option_id":"4","sku":"#ffffff","sort_order":"0","default_title":"White","store_title":null,"title":"White","default_price":"0.0000","default_price_type":"fixed","store_price":null,"store_price_type":null,"price":"0.0000","price_type":"fixed"}]
         */

        var product_id: Int = 0
        var getName: String? = null
        var getPrice: Float? = null
        var getSpecialPrice: Any? = null


        @SerializedName("Frame color")
        var Color: List<ColorBean>? = null

        class ColorBean {
            /**
             * option_type_id : 3
             * option_id : 4
             * sku : #00000
             * sort_order : 0
             * default_title : Black
             * store_title : null
             * title : Black
             * default_price : 0.0000
             * default_price_type : fixed
             * store_price : null
             * store_price_type : null
             * price : 0.0000
             * price_type : fixed
             */

            var option_type_id: String? = null
            var option_id: String? = null
            var sku: String? = null
            var image: String? = null
            var sort_order: String? = null
            var default_title: String? = null
            var store_title: Any? = null
            var title: String? = null
            var default_price: String? = null
            var default_price_type: String? = null
            var store_price: Any? = null
            var store_price_type: Any? = null
            var price: String? = null
            var price_type: String? = null
            var selected:Boolean?=null
        }
    }
}
