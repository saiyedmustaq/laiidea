package com.lalidea.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by kashifa on 12/12/17.
 */
class ProductDetailData : Serializable {

    @SerializedName("product")
    @Expose
    var product: Product? = null

    @SerializedName("reviews")
    @Expose
    var reviews: ReviewsData? = null

}