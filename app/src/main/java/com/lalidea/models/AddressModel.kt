package com.lalidea.models

import java.io.Serializable

class AddressModel : Serializable {


    var fname: String? = null
    var lname: String? = null
    var mname: String? = null
    var company: String? = null
    var address: String? = null
    var tel: String? = null
    var fax: String? = null
    var street: String? = null

    var city: String? = null
    var state: String? = null
    var zipcode: String? = null
    var country: String? = null

}
