package com.lalidea.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by rahil on 13/12/17.
 */

class DefaultAddressMainResponse {
    @SerializedName("billing_address")
    @Expose
    var billingAddress: DefaultAddress? = null
    @SerializedName("shipping_address")
    @Expose
    var shippingAddress: DefaultAddress? = null
}
