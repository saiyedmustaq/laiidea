package com.lalidea.models;

import java.util.List;

/**
 * Created by malik on 4/1/18.
 */

public class SearchModel {

    /**
     * status : 1
     * message : Search results for frame
     * data : [{"product_id":"30","product_name":"Golden State Art, Black 12x12 Square Picture Frame - Matted to Fit Pictures 8x8 Inches or 12x12 Without Mat","product_image":"http://www.lalidea.com/media/catalog/product/f/r/frame1.jpg","product_price":"Rs. 1.00"},{"product_id":"31","product_name":"Golden State Art, 12x12 Square Wood Picture Frame, Matted to Fit Pictures 8x8 or 12x12 Without Mat ","product_image":"http://www.lalidea.com/media/catalog/product/f/r/frame2.jpg","product_price":"Rs. 1.00"},{"product_id":"32","product_name":"SE JT9214 Glass Top Display Box with Metal Clips, 14.5\" x 8\" x 0.75\"","product_image":"http://www.lalidea.com/media/catalog/product/b/o/boxframe.jpg","product_price":"Rs. 100.00"},{"product_id":"33","product_name":"Shadowbox Gallery Wood Frames - Black, 6 x 6","product_image":"http://www.lalidea.com/media/catalog/product/b/o/boxframe2.jpg","product_price":"Rs. 1.00"},{"product_id":"34","product_name":"WOODEN FRAME A4 BLACK","product_image":"http://www.lalidea.com/media/catalog/product/a/4/a4frame.jpg","product_price":"Rs. 1.00"},{"product_id":"35","product_name":"Wilko Easy Photo Frame Black A4","product_image":"http://www.lalidea.com/media/catalog/product/a/4/a4frame2.jpg","product_price":"Rs. 1.00"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * product_id : 30
         * product_name : Golden State Art, Black 12x12 Square Picture Frame - Matted to Fit Pictures 8x8 Inches or 12x12 Without Mat
         * product_image : http://www.lalidea.com/media/catalog/product/f/r/frame1.jpg
         * product_price : Rs. 1.00
         */

        private String product_id;
        private String product_name;
        private String product_image;
        private String product_price;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getProduct_price() {
            return product_price;
        }

        public void setProduct_price(String product_price) {
            this.product_price = product_price;
        }
    }
}
