package com.lalidea.models

import java.io.Serializable

class RecentViewedModel : Serializable {
    var id: Int? = null
    var name: String? = null
    var description: String? = null
    var thumb: String? = null
}
