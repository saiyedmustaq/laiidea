package com.lalidea.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by rahil on 13/12/17.
 */

class DefaultAddress : Serializable {

    @SerializedName("entity_id")
    @Expose
    var entityId: String? = null
    @SerializedName("entity_type_id")
    @Expose
    var entityTypeId: String? = null
    @SerializedName("attribute_set_id")
    @Expose
    var attributeSetId: String? = null
    @SerializedName("increment_id")
    @Expose
    var incrementId: Any? = null
    @SerializedName("parent_id")
    @Expose
    var parentId: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("is_active")
    @Expose
    var isActive: String? = null
    @SerializedName("firstname")
    @Expose
    var firstname: String? = null
    @SerializedName("lastname")
    @Expose
    var lastname: String? = null
    @SerializedName("company")
    @Expose
    var company: String? = null
    @SerializedName("city")
    @Expose
    var city: String? = null
    @SerializedName("country_id")
    @Expose
    var countryId: String? = null
    @SerializedName("postcode")
    @Expose
    var postcode: String? = null
    @SerializedName("telephone")
    @Expose
    var telephone: String? = null
    @SerializedName("fax")
    @Expose
    var fax: String? = null
    @SerializedName("street")
    @Expose
    var street: String? = null
    @SerializedName("middlename")
    @Expose
    var middlename: String? = null
    @SerializedName("region")
    @Expose
    var region: String? = null

}
