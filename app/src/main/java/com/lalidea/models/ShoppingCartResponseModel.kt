package com.lalidea.models

import com.google.gson.JsonObject

/**
 * Created by malik on 19/12/17.
 */

class ShoppingCartResponseModel {
    /**
     * status : 1
     * message : Cart List
     * data : {"product":[{"product_id":"2","product_image":"http://www.lalidea.com/media/catalog/product/a/l/always_keep_it_opern_12x18.jpg","product_name":"always keep it opern 12x18","custom_options":{},"qty":6,"unit_price":"Rs. 1.00","product_sub_total":"Rs. 6.00"},{"product_id":"40","product_image":"http://www.lalidea.com/media/selfyimage/cropped-137678080.jpg","product_name":"Selfie Print","custom_options":{"upload_selfy_image":"cropped-137678080.jpg","frame_color":"#000000"},"qty":2,"unit_price":"Rs. 600.00","product_sub_total":"Rs. 1,200.00"},{"product_id":"11","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic10.jpg","product_name":"Graphic10","custom_options":{"upload_selfy_image":"cropped-137678080.jpg","frame_color":"#000000"},"qty":1,"unit_price":"Rs. 150.00","product_sub_total":"Rs. 150.00"},{"product_id":"40","product_image":"http://www.lalidea.com/media/selfyimage/cropped-650312106.jpg","product_name":"Selfie Print","custom_options":{"upload_selfy_image":"cropped-650312106.jpg","frame_color":"#000000"},"qty":1,"unit_price":"Rs. 600.00","product_sub_total":"Rs. 600.00"}],"total":{"subtotal":"Rs. 1,956.00","discount":"","grand_total":"Rs. 1,956.00"}}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * product : [{"product_id":"2","product_image":"http://www.lalidea.com/media/catalog/product/a/l/always_keep_it_opern_12x18.jpg","product_name":"always keep it opern 12x18","custom_options":{},"qty":6,"unit_price":"Rs. 1.00","product_sub_total":"Rs. 6.00"},{"product_id":"40","product_image":"http://www.lalidea.com/media/selfyimage/cropped-137678080.jpg","product_name":"Selfie Print","custom_options":{"upload_selfy_image":"cropped-137678080.jpg","frame_color":"#000000"},"qty":2,"unit_price":"Rs. 600.00","product_sub_total":"Rs. 1,200.00"},{"product_id":"11","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic10.jpg","product_name":"Graphic10","custom_options":{"upload_selfy_image":"cropped-137678080.jpg","frame_color":"#000000"},"qty":1,"unit_price":"Rs. 150.00","product_sub_total":"Rs. 150.00"},{"product_id":"40","product_image":"http://www.lalidea.com/media/selfyimage/cropped-650312106.jpg","product_name":"Selfie Print","custom_options":{"upload_selfy_image":"cropped-650312106.jpg","frame_color":"#000000"},"qty":1,"unit_price":"Rs. 600.00","product_sub_total":"Rs. 600.00"}]
         * total : {"subtotal":"Rs. 1,956.00","discount":"","grand_total":"Rs. 1,956.00"}
         */

        var total: TotalBean? = null
        var product: List<ProductBean>? = null

        class TotalBean {
            /**
             * subtotal : Rs. 1,956.00
             * discount :
             * count : 0
             * grand_total : Rs. 1,956.00
             */

            var subtotal: String? = null
            var discount: String? = null
            var grand_total: String? = null
            var count: Int = 0
        }

        class ProductBean {
            /**
             * product_id : 2
             * product_image : http://www.lalidea.com/media/catalog/product/a/l/always_keep_it_opern_12x18.jpg
             * product_name : always keep it opern 12x18
             * custom_options : {}
             * qty : 6
             * cart_item_id : 1053
             * unit_price : Rs. 1.00
             * product_sub_total : Rs. 6.00
             */

            var product_id: String? = null
            var cart_item_id: String? = null
            var product_image: String? = null
            var product_name: String? = null
            var custom_options: JsonObject? = null
            var qty: String = "0"
            var unit_price: String? = null
            var product_sub_total: String? = null
            var inStock: Int = 0

        }
    }
}
