package com.lalidea.models

import android.os.Parcel
import android.os.Parcelable

class MenuModel {

    /**
     * status : 1
     * message :
     * data : {"category":[{"category_id":"32","category_name":"Canvas Print","subcategory":[]},{"category_id":"4","category_name":"Photo Frame","subcategory":[{"category_id":"10","category_name":"Square Frames"},{"category_id":"6","category_name":"Box Frames"},{"category_id":"11","category_name":"Rectangle Frames"},{"category_id":"27","category_name":"TRENDING RECTANGLE FRAMES"},{"category_id":"28","category_name":"TRENDING SQUARE FRAMES"}]},{"category_id":"33","category_name":"Speciality Print","subcategory":[]},{"category_id":"29","category_name":"Islamic Art","subcategory":[]},{"category_id":"3","category_name":"Poster","subcategory":[{"category_id":"7","category_name":"Rectangle Posters"},{"category_id":"8","category_name":"Posters 12x18"},{"category_id":"9","category_name":"Posters 18x24"},{"category_id":"26","category_name":"TRENDING POSTER"}]},{"category_id":"34","category_name":"Wall art","subcategory":[]},{"category_id":"35","category_name":"Corporate Trophy","subcategory":[]},{"category_id":"12","category_name":"Stationery","subcategory":[{"category_id":"17","category_name":"Diaries"}]}]}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean() : Parcelable {
        var category: List<CategoryBean>? = null

        constructor(parcel: Parcel) : this() {
            category = parcel.createTypedArrayList(CategoryBean)
        }

        class CategoryBean() : Parcelable {
            /**
             * category_id : 32
             * category_name : Canvas Print
             * subcategory : []
             */

            var category_id: String? = null
            var category_name: String? = null
            var icon: String? = null
            var subcategory: ArrayList<SubCategory>? = null

            constructor(parcel: Parcel) : this() {
                category_id = parcel.readString()
                category_name = parcel.readString()
                icon = parcel.readString()
            }

            class SubCategory() : Parcelable {
                /**
                 * category_id : 32
                 * category_name : Canvas Print
                 */

                var category_id: String? = null
                var category_name: String? = null

                constructor(parcel: Parcel) : this() {
                    category_id = parcel.readString()
                    category_name = parcel.readString()
                }

                override fun writeToParcel(parcel: Parcel, flags: Int) {
                    parcel.writeString(category_id)
                    parcel.writeString(category_name)
                }

                override fun describeContents(): Int {
                    return 0
                }

                companion object CREATOR : Parcelable.Creator<SubCategory> {
                    override fun createFromParcel(parcel: Parcel): SubCategory {
                        return SubCategory(parcel)
                    }

                    override fun newArray(size: Int): Array<SubCategory?> {
                        return arrayOfNulls(size)
                    }
                }
            }

            override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(category_id)
                parcel.writeString(category_name)
            }

            override fun describeContents(): Int {
                return 0
            }

            companion object CREATOR : Parcelable.Creator<CategoryBean> {
                override fun createFromParcel(parcel: Parcel): CategoryBean {
                    return CategoryBean(parcel)
                }

                override fun newArray(size: Int): Array<CategoryBean?> {
                    return arrayOfNulls(size)
                }
            }
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeTypedList(category)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<DataBean> {
            override fun createFromParcel(parcel: Parcel): DataBean {
                return DataBean(parcel)
            }

            override fun newArray(size: Int): Array<DataBean?> {
                return arrayOfNulls(size)
            }
        }
    }
}
