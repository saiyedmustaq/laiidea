package com.lalidea.models

/**
 * Created by reena on 1/1/18.
 */
class ShareRequestModel {
    var name: String? = null
    var message: String? = null
    var email: String? = null
    var product_id: Int? = null

    var recipient: List<RecipientDataBean>? = null

     class RecipientDataBean {
        var name: String? = null
        var email: String? = null
    }
}