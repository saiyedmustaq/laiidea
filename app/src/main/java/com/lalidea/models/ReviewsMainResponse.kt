package com.lalidea.models

/**
 * Created by rahil on 15/12/17.
 */

class ReviewsMainResponse {


    /**
     * status : 1
     * message : Review List
     * data : [{"detail":"Graphics 8 very good product","date":"2017-12-14 11:05:55","product_id":"9","product_name":"Graphic8","product_price":"150.0000","product_shortdescription":"Graphic8","product_small_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic8.jpg","product_detail_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic8.jpg"},{"detail":"very good product","date":"2017-12-14 11:05:24","product_id":"2","product_name":"always keep it opern 12x18","product_price":"150.0000","product_shortdescription":"always keep it opern 12x18","product_small_image":"http://www.lalidea.com/media/catalog/product/a/l/always_keep_it_opern_12x18.jpg","product_detail_image":"http://www.lalidea.com/media/catalog/product/a/l/always_keep_it_opern_12x18.jpg"}]
     */

    var status: Int? = null
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * detail : Graphics 8 very good product
         * date : 2017-12-14 11:05:55
         * product_id : 9
         * product_name : Graphic8
         * product_price : 150.0000
         * product_shortdescription : Graphic8
         * product_small_image : http://www.lalidea.com/media/catalog/product/g/r/graphic8.jpg
         * product_detail_image : http://www.lalidea.com/media/catalog/product/g/r/graphic8.jpg
         */

        var detail: String? = null
        var date: String? = null
        var product_id: String? = null
        var product_name: String? = null
        var product_price: String? = null
        var product_shortdescription: String? = null
        var product_small_image: String? = null
        var product_detail_image: String? = null
    }
}
