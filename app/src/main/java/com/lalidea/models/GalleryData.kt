package com.lalidea.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by reena on 16/12/17.
 */
class GalleryData : Serializable {

    @SerializedName("gallery_url")
    @Expose
    var galleryUrl: String? = null
}