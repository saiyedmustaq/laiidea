package com.lalidea.models

/**
 * Created by rahil on 15/12/17.
 */

class WishlistMainResponse {



    var status: Int? = null
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {


        var product_id: String? = null
        var product_name: String? = null
        var product_description: String? = null
        var qty: String? = null
        var price: String? = null
        var product_image: String? = null
        var inStock: Int? = null
    }
}
