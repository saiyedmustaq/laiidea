package com.lalidea.models

import java.io.Serializable

/**
 * Created by rahil on 12/12/17.
 */
class StatusMessageResponse :Serializable{
    var status:Int?=null
    var message:String?=null
}