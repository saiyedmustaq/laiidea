package com.lalidea.models

/**
 * Created by reena on 30/12/17.
 */

class PaymentOrderStatusResponse {


    /**
     * status : 1
     * message : Your order has been received
     * data : [{"print_order_id":"100000054","status":0}]
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * print_order_id : 100000054
         * status : 0
         */

        var print_order_id: String? = null
        var status: Int = 0
    }
}
