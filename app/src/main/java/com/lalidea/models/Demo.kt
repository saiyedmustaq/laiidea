package com.lalidea.models

class Demo {


    /**
     * status : 1
     * message : Home Page
     * data : [{"slideshowImage":[["https://www.lalidea.com/media/wysiwyg/infortis/Buy-1-get-1-offer.jpg"],["https://www.lalidea.com/skin/frontend/rwd/lalidea/images/banner.png"]],"posters":[{"product_id":"185","product_name":"Best of the best","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/1/s/1st_4.jpg","product_price":"Rs. 199.00"},{"product_id":"187","product_name":"More Of What Makes You Happy","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/5/t/5th_4.jpg","product_price":"Rs. 199.00"},{"product_id":"188","product_name":"Mistakes Are Proof","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/6/t/6th_4.jpg","product_price":"Rs. 199.00"},{"product_id":"190","product_name":"Have the courage","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/8/t/8th_5.jpg","product_price":"Rs. 199.00"}],"new_arrival":[{"product_id":"1826","product_name":"Mashallah Islamic calligraphy","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00079.jpg","product_price":"Rs. 600.00"},{"product_id":"1825","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00010_2.jpg","product_price":"Rs. 1,100.00"},{"product_id":"1824","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00009_2.jpg","product_price":"Rs. 1,100.00"},{"product_id":"1823","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00008_2.jpg","product_price":"Rs. 1,100.00"}],"islamic_art":[{"product_id":"1024","product_name":"Mohammad","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/m/o/mohammad.jpg","product_price":"Rs. 1,500.00"},{"product_id":"1653","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00001_3.jpg","product_price":"Rs. 1,100.00"},{"product_id":"1654","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00002_1_2.jpg","product_price":"Rs. 1,100.00"},{"product_id":"1655","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00003_2.jpg","product_price":"Rs. 1,100.00"}],"frame":[{"product_id":"30","product_name":"Golden State Art  Frame","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/1/s/1st_2.jpg","product_price":"Rs. 699.00"},{"product_id":"31","product_name":"Golden State Wood  Frame","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/2/n/2nd_2.jpg","product_price":"Rs. 699.00"},{"product_id":"76","product_name":"Sore today strong tomorrow","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/3/r/3rd_2.jpg","product_price":"Rs. 699.00"},{"product_id":"77","product_name":"Mistakes are 2019-09-09 15:54:59.480 4851-4893/com.lalidea D/OkHttp:  proof","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/6/t/6th_2.jpg","product_price":"Rs. 699.00"}],"cart_id":"61","count":0}]
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * slideshowImage : [["https://www.lalidea.com/media/wysiwyg/infortis/Buy-1-get-1-offer.jpg"],["https://www.lalidea.com/skin/frontend/rwd/lalidea/images/banner.png"]]
         * posters : [{"product_id":"185","product_name":"Best of the best","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/1/s/1st_4.jpg","product_price":"Rs. 199.00"},{"product_id":"187","product_name":"More Of What Makes You Happy","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/5/t/5th_4.jpg","product_price":"Rs. 199.00"},{"product_id":"188","product_name":"Mistakes Are Proof","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/6/t/6th_4.jpg","product_price":"Rs. 199.00"},{"product_id":"190","product_name":"Have the courage","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/8/t/8th_5.jpg","product_price":"Rs. 199.00"}]
         * new_arrival : [{"product_id":"1826","product_name":"Mashallah Islamic calligraphy","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00079.jpg","product_price":"Rs. 600.00"},{"product_id":"1825","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00010_2.jpg","product_price":"Rs. 1,100.00"},{"product_id":"1824","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00009_2.jpg","product_price":"Rs. 1,100.00"},{"product_id":"1823","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00008_2.jpg","product_price":"Rs. 1,100.00"}]
         * islamic_art : [{"product_id":"1024","product_name":"Mohammad","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/m/o/mohammad.jpg","product_price":"Rs. 1,500.00"},{"product_id":"1653","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00001_3.jpg","product_price":"Rs. 1,100.00"},{"product_id":"1654","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00002_1_2.jpg","product_price":"Rs. 1,100.00"},{"product_id":"1655","product_name":"la ilaha illallah muhammadur rasulullah","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/i/s/islamic_00003_2.jpg","product_price":"Rs. 1,100.00"}]
         * frame : [{"product_id":"30","product_name":"Golden State Art  Frame","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/1/s/1st_2.jpg","product_price":"Rs. 699.00"},{"product_id":"31","product_name":"Golden State Wood  Frame","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/2/n/2nd_2.jpg","product_price":"Rs. 699.00"},{"product_id":"76","product_name":"Sore today strong tomorrow","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/3/r/3rd_2.jpg","product_price":"Rs. 699.00"},{"product_id":"77","product_name":"Mistakes are 2019-09-09 15:54:59.480 4851-4893/com.lalidea D/OkHttp:  proof","product_image":"https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/6/t/6th_2.jpg","product_price":"Rs. 699.00"}]
         * cart_id : 61
         * count : 0
         */

        var cart_id: String? = null
        var count: Int = 0
        var slideshowImage: List<List<String>>? = null
        var posters: List<PostersBean>? = null
        var new_arrival: List<PostersBean>? = null
        var islamic_art: List<PostersBean>? = null
        var frame: List<PostersBean>? = null

        class PostersBean {
            /**
             * product_id : 185
             * product_name : Best of the best
             * product_image : https://www.lalidea.com/media/catalog/product/cache/1/image/210x/040ec09b1e35df139433887a97daa66f/1/s/1st_4.jpg
             * product_price : Rs. 199.00
             */

            var product_id: String? = null
            var product_name: String? = null
            var product_image: String? = null
            var product_price: String? = null
        }
    }
}
