package com.lalidea.models

import java.io.Serializable

class MyOrderModel : Serializable {
    var id: Int? = null
    var txtorderNumber: Int? = null
    var txtorderDate: String? = null
    var txtordershipto: String? = null
    var txtordertotal: Int? = null
    var txtorderstatus: String? = null

}
