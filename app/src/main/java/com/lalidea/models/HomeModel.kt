package com.lalidea.models

import java.io.Serializable

/**
 * Created by malik on 30/12/17.
 */

class HomeModel : Serializable {


    /**
     * status : 1
     * message : Home Page
     * data : [{"slideshowImage":[{"slider":"http://www.lalidea.com/media/wysiwyg/infortis/banner2.jpg"}],"ourproduct":[{"product_id":"2","product_name":"always keep it opern 12x18","product_image":"http://www.lalidea.com/media/catalog/product/a/l/always_keep_it_opern_12x18.jpg","product_price":"Rs. 1.00"},{"product_id":"3","product_name":"Graphic1","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic1.jpg","product_price":"Rs. 1.00"},{"product_id":"4","product_name":"Graphic2","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic2.jpg","product_price":"Rs. 1.00"},{"product_id":"5","product_name":"Graphic4","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic4.jpg","product_price":"Rs. 150.00"},{"product_id":"6","product_name":"Graphic5","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic5.jpg","product_price":"Rs. 150.00"},{"product_id":"7","product_name":"Graphic6","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic6.jpg","product_price":"Rs. 150.00"},{"product_id":"8","product_name":"Graphic7","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic7.jpg","product_price":"Rs. 150.00"},{"product_id":"9","product_name":"Graphic8","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic8.jpg","product_price":"Rs. 150.00"},{"product_id":"10","product_name":"Graphic9","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic9.jpg","product_price":"Rs. 150.00"},{"product_id":"11","product_name":"Graphic10","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic10.jpg","product_price":"Rs. 150.00"}],"cart_id":"424","count":"2.0000"}]
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * slideshowImage : [{"slider":"http://www.lalidea.com/media/wysiwyg/infortis/banner2.jpg"}]
         * posters : [{"product_id":"2","product_name":"always keep it opern 12x18","product_image":"http://www.lalidea.com/media/catalog/product/a/l/always_keep_it_opern_12x18.jpg","product_price":"Rs. 1.00"},{"product_id":"3","product_name":"Graphic1","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic1.jpg","product_price":"Rs. 1.00"},{"product_id":"4","product_name":"Graphic2","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic2.jpg","product_price":"Rs. 1.00"},{"product_id":"5","product_name":"Graphic4","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic4.jpg","product_price":"Rs. 150.00"},{"product_id":"6","product_name":"Graphic5","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic5.jpg","product_price":"Rs. 150.00"},{"product_id":"7","product_name":"Graphic6","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic6.jpg","product_price":"Rs. 150.00"},{"product_id":"8","product_name":"Graphic7","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic7.jpg","product_price":"Rs. 150.00"},{"product_id":"9","product_name":"Graphic8","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic8.jpg","product_price":"Rs. 150.00"},{"product_id":"10","product_name":"Graphic9","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic9.jpg","product_price":"Rs. 150.00"},{"product_id":"11","product_name":"Graphic10","product_image":"http://www.lalidea.com/media/catalog/product/g/r/graphic10.jpg","product_price":"Rs. 150.00"}]
         * cart_id : 424
         * count : 2.0000
         */

        var cart_id: Int? = null
        var count: Float? = null
        var slideshowImage: ArrayList<SlideshowImageBean>? = null
        var posters: ArrayList<PostersBean>? = null
        var new_arrival: ArrayList<PostersBean>? = null
        var frame: ArrayList<PostersBean>? = null
        var islamic_art: ArrayList<PostersBean>? = null

        class SlideshowImageBean {
            /**
             * slider : http://www.lalidea.com/media/wysiwyg/infortis/banner2.jpg
             */

            var slider: String? = null
        }

        class PostersBean {
            /**
             * product_id : 2
             * product_name : always keep it opern 12x18
             * product_image : http://www.lalidea.com/media/catalog/product/a/l/always_keep_it_opern_12x18.jpg
             * product_price : Rs. 1.00
             */

            var product_id: String? = null
            var product_name: String? = null
            var product_image: String? = null
            var product_price: String? = null
        }
    }
}
