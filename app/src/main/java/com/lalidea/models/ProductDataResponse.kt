package com.lalidea.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by kashifa on 12/12/17.
 */
class ProductDataResponse : Serializable {

    var status: Int? = null
    var message: String? = null
    var total_record: Int = 0

    @SerializedName("data")
    @Expose
    var data: ArrayList<ProductDataModel>? = null


}