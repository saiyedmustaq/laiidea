package com.lalidea.models

/**
 * Created by malik on 27/12/17.
 */

class ReOrderModel {

    /**
     * status : 1
     * message : Reorder
     * data : {"count":5}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * count : 5
         */

        var count: Int = 0
    }
}
