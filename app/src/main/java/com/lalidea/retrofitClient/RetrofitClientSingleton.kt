package com.lalidea.retrofitClient

import android.util.Log
import com.lalidea.BuildConfig
import com.lalidea.appController.AppController
import com.lalidea.utils.baseUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedInputStream
import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager


class RetrofitClientSingleton {

    companion object {
        val TAG = RetrofitClientSingleton::class.java.simpleName
        fun getInstance(): ServiceGenerator {
            val cf = CertificateFactory.getInstance("X.509")
            val cert = BufferedInputStream(AppController.appInstance!!.assets.open("certificate_new.crt"))
            val ca: Certificate
//            val cert = AppController.appInstance!!.resources.openRawResource(R.raw.lalidea_certificate)
//            val ca: Certificate

            // creating a KeyStore containing our trusted CAs
            val keyStoreType = KeyStore.getDefaultType()
            val keyStore = KeyStore.getInstance(keyStoreType)
            keyStore.load(null, null)

            try {
                ca = cf.generateCertificate(cert)
                keyStore.setCertificateEntry("ca", ca)
                println("$TAG >> ca= " + (ca as X509Certificate).subjectDN)
            } catch (e: Exception) {
                Log.e(TAG, "getInstance >> $e")
//                Log.e(TAG, "getInstance >> $e")
            } finally {
                cert.close()
            }


            // creating a TrustManager that trusts the CAs in our KeyStore
            val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
            val tmf = TrustManagerFactory.getInstance(tmfAlgorithm)
            tmf.init(keyStore)
            val trustManagers = tmf.trustManagers
            if (trustManagers.size != 1 || trustManagers[0] !is X509TrustManager) {
                throw IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers))
            }
            val trustManager = trustManagers[0] as X509TrustManager

            // creating an SSLSocketFactory that uses our TrustManager
            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, tmf.trustManagers, null)

            // creating an OkHttpClient that uses our SSLSocketFactory
//            val okHttpClient = OkHttpClient()
//            okHttpClient.sslSocketFactory()


            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val okHttpBuilder = OkHttpClient.Builder()

            if (BuildConfig.DEBUG) {
                okHttpBuilder.addInterceptor(loggingInterceptor)
//                okHttpBuilder.sslSocketFactory(sslContext.socketFactory,trustManager)
            }

            val client = okHttpBuilder.connectTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .readTimeout(120, TimeUnit.SECONDS).build()
//            client.connectTimeout(120, TimeUnit.SECONDS)
//                    .writeTimeout(120, TimeUnit.SECONDS)
//                    .readTimeout(120, TimeUnit.SECONDS)

//            client.sslSocketFactory()


            val retrofit = Retrofit.Builder().baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()

            return retrofit.create(ServiceGenerator::class.java)
        }
    }

}