package com.lalidea.utils

/**
 * Created by malik on 10/11/17.
 */

val baseUrl = "https://www.lalidea.com/attune-myws/index/"

val getPostersSubcat = "getPostersSubcat"
val getFramesSubcat = "getFramesSubcat"
val getStationerySubcat = "getStationerySubcat"
val getAccessoriesSubcat = "getAccessoriesSubcat"
val getIslamicSubcat = ""
val getWallArt = ""
val getCorporateTrophy = ""
val getSubCategory="sgetcategories"