package com.lalidea.retrofitClient

import com.lalidea.models.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import java.util.*

interface ServiceGenerator {

    @FormUrlEncoded
    @POST("login")
    fun getLogin(@FieldMap map: HashMap<String, String>): Call<UserDataResponse>

    @FormUrlEncoded
    @POST("registration")
    fun getSignUp(@FieldMap map: HashMap<String, String>): Call<UserDataResponse>

    @FormUrlEncoded
    @POST("forgotPassword")
    fun getForgotPassword(@Field("email") email: String): Call<UserDataResponse>


    @GET("{product_type}")
    fun getProductCategory(@Path("product_type") product_type: String): Call<ProductSubCategoryModel>


    ////Edit profile
    @FormUrlEncoded
    @POST("editProfile")
    fun editProfile(@FieldMap map: HashMap<String, String>): Call<UserDataResponse>


    ////add new address
    @FormUrlEncoded
    @POST("addAddress")
    fun addNewAddress(@FieldMap map: HashMap<String, String>): Call<StatusMessageResponse>

    /////edit addresss
    @FormUrlEncoded
    @POST("editAddress")
    fun editAddress(@FieldMap map: HashMap<String, String>): Call<StatusMessageResponse>


    ///default  addresslist
    @FormUrlEncoded
    @POST("listAddress")
    fun getDefaultAddress(@FieldMap map: HashMap<String, String>): Call<DefaultAddressResponse>


    @FormUrlEncoded
    @POST("getProduct")
    fun getProduct(@FieldMap map: HashMap<String, String>): Call<ProductDataResponse>

    @FormUrlEncoded
    @POST("productDetail")
    fun productDetail(@FieldMap map: HashMap<String, String>): Call<ProductDetailResponse>


    ///Additional addresslist
    @FormUrlEncoded
    @POST("listAddtionalAddress")
    fun getAddidionalAddress(@FieldMap map: HashMap<String, String>): Call<AditionalAddressMainResponse>


    ////delete  address
    @FormUrlEncoded
    @POST("deleteAddress")
    fun deleteAddress(@FieldMap map: HashMap<String, String>): Call<StatusMessageResponse>


    /////get reviews of users
    @FormUrlEncoded
    @POST("getReviewByUserId")
    fun getReviewsByUserId(@FieldMap map: HashMap<String, String>): Call<ReviewsMainResponse>

    @FormUrlEncoded
    @POST("addReview")
    fun addReview(@FieldMap map: HashMap<String, String>): Call<StatusMessageResponse>

    @FormUrlEncoded
    @POST("addtocartProduct")
    fun addToCart(@FieldMap map: HashMap<String, String>): Call<AddToCartModel>


    ///Add to wish list///
    @FormUrlEncoded
    @POST("addtowishlist")
    fun addToWishlist(@FieldMap map: HashMap<String, String>): Call<AddToWishListModel>

    ///Add to wish list///
    @FormUrlEncoded
    @POST("wishlisttocart")
    fun addAllToWishlist(@FieldMap map: HashMap<String, String>): Call<AddToWishListModel>


    ///add to cart from wish list
    @FormUrlEncoded
    @POST("wishlisttocartUsingProductId")
    fun addtoCartFromWishList(@FieldMap map: HashMap<String, String>): Call<AddTCartFromWishListModel>


    ///delete from wishlist
    @FormUrlEncoded
    @POST("removeProductFromWishlist")
    fun deleteFromWishList(@FieldMap map: HashMap<String, String>): Call<AddToWishListModel>


    @FormUrlEncoded
    @POST("getCartList")
    fun getShoppingCartItems(@FieldMap map: HashMap<String, String>): Call<ShoppingCartResponseModel>


    @FormUrlEncoded
    @POST("editCart")
    fun editCart(@FieldMap map: HashMap<String, String>): Call<ShoppingCartResponseModel>

    @FormUrlEncoded
    @POST("removeProductFromCart")
    fun deleteItemFromCart(@FieldMap map: HashMap<String, String>): Call<ShoppingCartResponseModel>

    @FormUrlEncoded
    @POST("getWishlist")
    fun getWishList(@FieldMap map: HashMap<String, String>): Call<WishlistMainResponse>


    @FormUrlEncoded
    @POST("newsletter")
    fun newsSubscription(@FieldMap map: HashMap<String, String>): Call<StatusMessageResponse>


    ///Additional addresslist
    @FormUrlEncoded
    @POST("myOrderList")
    fun getMyOrderList(@FieldMap map: HashMap<String, String>): Call<MyOrderListResponse>

    ///add to cart from wish list
    @FormUrlEncoded
    @POST("createOrder")
    fun placeOrder(@FieldMap map: HashMap<String, String>): Call<PlaceOrderId>

    ///add to cart from wish list
    @FormUrlEncoded
    @POST("createPaypalOrder")
    fun placePaypalOrder(@FieldMap map: HashMap<String, String>): Call<PlaceOrderId>

    ///re order
    @FormUrlEncoded
    @POST("reorder")
    fun reOrder(@FieldMap map: HashMap<String, String>): Call<ReOrderModel>

    // order details
    @FormUrlEncoded
    @POST("OrderDetail")
    fun getOrderDetail(@FieldMap map: HashMap<String, String>): Call<MyOrderDetailResponse>

    // remove discount from cart
    @FormUrlEncoded
    @POST("removeDiscount")
    fun removeDiscount(@FieldMap map: HashMap<String, String>): Call<ShoppingCartResponseModel>

    // apply discount from cart
    @FormUrlEncoded
    @POST("applyDiscount")
    fun applyDiscount(@FieldMap map: HashMap<String, String>): Call<ShoppingCartResponseModel>

    // get home data
    @FormUrlEncoded
    @POST("homePageDetail")
    fun getHomeData(@FieldMap map: HashMap<String, String>): Call<HomeModel>


    // get drawer menu
    @GET("getcategories")
    fun getDrawerMenu(): Call<MenuModel>

    // get selfie data
    @GET("productSelfie")
    fun getSelfieData(): Call<SelfieModel>

    // payment Apply For Order- Success or Failure
    @FormUrlEncoded
    @POST("paymentApplyForOrder")
    fun paymentApplyForOrder(@FieldMap map: HashMap<String, String>): Call<PaymentOrderStatusResponse>

    @POST("addtocartProduct")
    @Multipart
    fun addSelfieToCart(@PartMap map: HashMap<String, RequestBody>): Call<AddToCartModel>

    // share product via email
    @POST("shareProductLink")
    fun shareProduct(@Body model: ShareRequestModel): Call<StatusMessageResponse>


    @FormUrlEncoded
    @POST("search")
    fun searchProduct(@FieldMap map: HashMap<String, String>): Call<ProductDataResponse>

    @FormUrlEncoded
    @POST("sendInvoice")
    fun sendInvoice(@FieldMap map: HashMap<String, String>): Call<StatusMessageResponse>

    @FormUrlEncoded
    @POST("verifycartId")
    fun verifycartId(@FieldMap map: HashMap<String, String>): Call<StatusMessageResponse>

    @FormUrlEncoded
    @POST("contactus")
    fun contactUs(@FieldMap map: HashMap<String, String>): Call<StatusMessageResponse>

    @GET("{path}")
    fun getBrowserData(@Path("path") path: String): Call<SuccessModel>

    @GET("footerData")
    fun footerData(): Call<FooterModel>

    @FormUrlEncoded
    @POST("orderCancel")
    fun cancelOrder(@FieldMap map: HashMap<String, String>): Call<StatusMessageResponse>
}