package com.lalidea.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.provider.Settings
import android.support.v4.app.ActivityCompat.finishAffinity
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.text.TextUtils
import android.util.Base64.encodeToString
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import com.lalidea.R
import com.lalidea.activities.MainActivity
import com.lalidea.activities.ProductListActivity
import kotlinx.android.synthetic.main.dialog_get_invoice.*
import kotlinx.android.synthetic.main.layout_no_data.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import java.net.Inet4Address
import java.net.NetworkInterface
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

var DEVICE_TYPE = "Android"
//var popupWindow: PopupWindow? = null

var progressDialog: ProgressDialog? = null
val TAG = "AppUtils"
fun getStatusBarHeight(context: Context): Int {
    var result = 0
    val resourceId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = context.resources.getDimensionPixelSize(resourceId)
    }
    return result
}

fun showProgressDialog(context: Context, view: View) {
    try {
//        val layout = LayoutInflater.from(context).inflate(R.layout.popup_loading, null)
//        popupWindow = PopupWindow(layout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false)
//        popupWindow!!.showAtLocation(view, Gravity.CENTER, 0, getStatusBarHeight(context))
        progressDialog = ProgressDialog(context)
        if (!progressDialog!!.isShowing) {
            progressDialog?.setMessage("Please wait...")
            progressDialog?.setCancelable(false)
            progressDialog?.show()
        }
    } catch (e: Throwable) {
        Log.e(TAG, e.message)
        e.printStackTrace()
    }

}

fun stopProgress() {
    try {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog?.dismiss()
        }
    } catch (e: Throwable) {
        e.printStackTrace()
    }
}


fun toast(context: Context?, text: String?) {
    if (context != null && text != null) {
        val toast = android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT)
//        toast.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0, 200)
        toast.show()
    }
}

fun toast(context: Context?, text: Int?) {
    if (context != null && text != null) {
        val toast = android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT)
//        toast.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0, 200)
        toast.show()
    }


}


fun isValidText(str: String): Boolean {
    val expression = "^[a-zA-Z\\s]+"
    return str.matches(expression.toRegex())
}

fun isValidMail(mailString: String): Boolean {
    return !TextUtils.isEmpty(mailString) && android.util.Patterns.EMAIL_ADDRESS.matcher(mailString).matches()
}


fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}

@SuppressLint("MissingPermission", "HardwareIds")
fun getAndroidId(context: Context): String {
    val deviceId = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    println("$TAG, getAndroidId : deviceId >> $deviceId")
    return deviceId
}

fun getImageRequestBody(file: File): RequestBody {
    return RequestBody.create(MediaType.parse("image/*"), file)
}

fun getStringRequestBody(strParam: String): RequestBody {
    return RequestBody.create(MediaType.parse("text/plain"), strParam)
}

fun getIntRequestBody(intParam: Int): RequestBody {
    return RequestBody.create(MediaType.parse("text/plain"), intParam.toString())
}

fun getLocalIpAddress(): String {
    try {
        val en = NetworkInterface.getNetworkInterfaces()
        while (en.hasMoreElements()) {
            val intf = en.nextElement()
            val enumIpAddr = intf.inetAddresses
            while (enumIpAddr.hasMoreElements()) {
                val inetAddress = enumIpAddr.nextElement()
                if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
                    return inetAddress.getHostAddress()
                }
            }
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }

    return ""
}

fun validatePassword(password: String): Boolean {
    val strPattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
    var pattern: Pattern = Pattern.compile(strPattern)
    var matcher: Matcher = pattern.matcher(password)
    return matcher.matches()
}

fun setBadgeCount(context: Context, icon: LayerDrawable, count: String) {

    val badge: BadgeDrawable

    // Reuse drawable if possible
    val reuse = icon.findDrawableByLayerId(R.id.ic_badge)
    badge = if (reuse != null && reuse is BadgeDrawable) {
        reuse
    } else {
        BadgeDrawable(context)
    }

    badge.setCount(count)
    icon.mutate()
    icon.setDrawableByLayerId(R.id.ic_badge, badge)


}

fun convertDateTime(fromFormat: String, toFormat: String, dateOriginalGot: String): String {

    try {
        //SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //// Getting Source format here
        val fmt = SimpleDateFormat(fromFormat)

        fmt.timeZone = TimeZone.getDefault()

        val date = fmt.parse(dateOriginalGot)

        //SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, MMM d, ''yyyy");

        //// Setting Destination format here
        val fmtOut = SimpleDateFormat(toFormat)

        return fmtOut.format(date)

    } catch (e: Exception) {

        e.printStackTrace()

        e.message

    }

    return ""

}


fun showNoInternetLayout(view: View) {
    view.llParentNoData.visibility = View.VISIBLE
    view.txtNodata.text = view.context.getString(R.string.network_error)
//    view.ivNoData.setImageResource(R.drawable.ic_no_connection)
}

fun hideNoInternetLayout(view: View) {
    view.llParentNoData.visibility = View.GONE
}


fun showNoDataLayout(view: View, str: String = "No data!") {
    view.llParentNoData.visibility = View.VISIBLE
    view.txtNodata.text = str
//    view.ivNoData.setImageResource(R.drawable.ic_no_data)
}

fun hideNoDataLayout(view: View) {
    view.llParentNoData.visibility = View.GONE
}


fun showSomethingWentWrongDataLayout(view: View) {
    view.llParentNoData.visibility = View.VISIBLE
    view.txtNodata.text = view.resources.getString(R.string.something_went_wrong)
//    view.ivNoData.setImageResource(R.drawable.ic_something_wrong)
}

fun formatString(d: Double): String {
    return if (d == d.toLong().toDouble())
        String.format("%d", d.toLong())
    else
        String.format("%s", d)
}


fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null) {
        for (permission in permissions) {
            if (checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
    }
    return true
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun submitSearchQuery(context: Context, query: String) {
//    toast(context, query)
    val intent = Intent(context, ProductListActivity::class.java)
    intent.putExtra("search_query", query)
    context.startActivity(intent)
}

fun orderAlreadyPlacedDialog(context: Context, strMessage: String) {
    val dialog = Dialog(context)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    dialog.setContentView(R.layout.dialog_get_invoice)
    dialog.tvMessage.text = strMessage
    dialog.setCancelable(false)
    dialog.show()

    dialog.txtOk.setOnClickListener({
        dialog.dismiss()
        finishAffinity(context as Activity?)
        context.startActivity(Intent(context, MainActivity::class.java))
    })
}

fun encodeStringToBase64(str: String): String {
    val data = str.toByteArray(charset("UTF-8"))
    return encodeToString(data, android.util.Base64.DEFAULT)
}

fun decodeBase64(base64: String): String {
    return try {
        val data = android.util.Base64.decode(base64, android.util.Base64.DEFAULT)
        String(data, charset("UTF-8"))
    } catch (e: Exception) {
        Log.e(TAG, "decodeBase64 >> $e")
        base64
    }
}

fun isColorValid(colorString: String): Boolean {
    val colorPattern = Pattern.compile("#([0-9a-f]{3}|[0-9a-f]{6}|[0-9a-f]{8})")
    val m = colorPattern.matcher(colorString)
    return m.matches()
}

