package com.lalidea.utils

// key set

var userId = "userId"
val phone = "phone"
val email = "email"

val is_active = "is_active"
val store_id = "store_id"
val firstname = "firstname"
val middlename = "middlename"
val lastname = "lastname"
val default_billing = "default_billing"
val default_shipping = "default_shipping"

val status = "1"

// permission constants
val REQUEST_WRITE_STORAGE_PERMISSION = 1
val WRITE_PERMISSION_CODE = 2
val REQUEST_IMAGE = 3
val SELECT_IMAGE_GALLERY = 4
val REQUEST_READ_STORAGE_PERMISSION = 5
val REQUEST_READ_STATE_PERMISSION = 6
val CAMERA_PERMISSION_CODE = 7
val REQUEST_CAMERA_IMAGE = 8
val REQUEST_GALLERY_IMAGE = 9

// app constants
val statusPending = "0"
val statusApproved = "1"

val subsribed = 0

// product sub type
val HOME = 0
val POSTERS = 1
val FRAMES = 2
val STATIONERY = 3
val ACCESSORIES = 4
val ACCOUNT = 5
val CONTACT = 6
val ISLAMICART = 7
val WALLART = 8
val CORPORATE_TROPHY = 9
val fromactivity = "from"
val menuactivity = "menu"
val chnageactivity = "change"

val SHIPPING = 1

val BILLING = 2


//response constants
val SUCCESS = 1
val FAILURE = 2
val ORDER_ALREADY_PLACED = 3

//cart
val cartCount = "cartCount"
val cartId = "cartId"
val coupenCode = "coupenCode"
val news_subscription = "is_subscribed"

// home grid types
val typePosters = 1
val typeNewArrival = 2
val typeTrendingFrames = 3
val typeIsalmicArt = 4