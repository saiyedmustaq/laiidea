package com.lalidea.utils

import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.view.ViewTreeObserver
import android.view.Window
import android.widget.TextView
import com.lalidea.R
import kotlinx.android.synthetic.main.dialo_view_more_details.*

/**
 * Created by kashifa on 13/12/17.
 */

object ResizableCustomView {
    fun doResizeTextView(context: Context, tv: TextView, maxLine: Int, expandText: String, viewMore: Boolean) {

        if (tv.tag == null) {
            tv.tag = tv.text
        }
        val vto = tv.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {

                // val obs = tv.viewTreeObserver
                removeOnGlobalLayoutListener(tv, this)
                // obs.removeGlobalOnLayoutListener(this)
                if (maxLine == 0) {
                    val lineEndIndex = tv.layout.getLineEnd(0)
                    val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + " " + expandText
                    tv.text = text
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    tv.setText(
                            addClickablePartTextViewResizable(context, fromHtml(tv.text.toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE)
                } else if (maxLine > 0 && tv.lineCount >= maxLine) {
                    val lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                    val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + " " + expandText
                    tv.text = text
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    tv.setText(
                            addClickablePartTextViewResizable(context, fromHtml(tv.text.toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE)
                } else {
                    val lineEndIndex = tv.layout.getLineEnd(tv.layout.lineCount - 1)
                    val text = tv.text.subSequence(0, lineEndIndex).toString() + " " + expandText
                    tv.text = text
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    tv.setText(
                            addClickablePartTextViewResizable(context, fromHtml(tv.text.toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE)
                }
            }
        })

    }

    private fun addClickablePartTextViewResizable(mcontext: Context, strSpanned: Spanned, tv: TextView,
                                                  maxLine: Int, spanableText: String, viewMore: Boolean): SpannableStringBuilder {
        val str = strSpanned.toString()
        val ssb = SpannableStringBuilder(strSpanned)

        if (str.contains(spanableText)) {
            ssb.setSpan(object : ClickableSpan() {
                override fun updateDrawState(ds: TextPaint) {
                    ds.bgColor = Color.TRANSPARENT
                    ds.isUnderlineText = false
                    super.updateDrawState(ds)
                }

                override fun onClick(widget: View) {

                    if (viewMore) {
                        val dialog = Dialog(mcontext)
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        dialog.setContentView(R.layout.dialo_view_more_details)
                        dialog.show()

                        tv.highlightColor = Color.TRANSPARENT
                        tv.layoutParams = tv.layoutParams
                        dialog.etTextview.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                        tv.invalidate()

                        dialog.llOkViewMore.setOnClickListener {

                            dialog.dismiss()

                        }

                    } else {
                        tv.layoutParams = tv.layoutParams
                        tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                        tv.invalidate()
                        doResizeTextView(mcontext, tv, 3, "View More", true)
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length, 0)

        }
        return ssb
    }

    @Suppress("DEPRECATION")
    fun fromHtml(html: String): Spanned {
        return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    fun removeOnGlobalLayoutListener(v: View, listener: ViewTreeObserver.OnGlobalLayoutListener) {
        if (Build.VERSION.SDK_INT < 16) {
            v.viewTreeObserver.removeGlobalOnLayoutListener(listener)
        } else {
            v.viewTreeObserver.removeOnGlobalLayoutListener(listener)
        }
    }
}