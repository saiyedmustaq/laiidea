package com.lalidea.utils

import android.content.Context
import android.content.SharedPreferences

object SharedPref {

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }

    fun getStringValue(context: Context, key: String, default: String = ""): String {
        return getSharedPreferences(context).getString(key, default)
    }

    fun getUserId(context: Context): String = getSharedPreferences(context).getString(userId, "")

    fun setStringValue(context: Context, key: String, newValue: String?) {
        val editor = getSharedPreferences(context).edit()
        editor.putString(key, newValue)
        editor.apply()
    }


    fun getIntValue(context: Context, key: String, default: Int = 0): Int {
        return getSharedPreferences(context).getInt(key, default)
    }

    fun setIntValue(context: Context, key: String, newValue: Int) {
        val editor = getSharedPreferences(context).edit()
        editor.putInt(key, newValue)
        editor.apply()
    }

    fun getBooleanValue(context: Context, key: String, default: Boolean = false): Boolean {
        return getSharedPreferences(context).getBoolean(key, default)
    }

    fun setBooleanValue(context: Context, key: String, newValue: Boolean) {
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(key, newValue)
        editor.apply()
    }

    fun clear(context: Context) {
        val editor = getSharedPreferences(context).edit()
        editor.clear()
        editor.apply()
    }
}


///tetessbhbsh