package com.lalidea.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.lalidea.R
import com.lalidea.models.StatusMessageResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_newsletter.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class NewsLetterSubscriptionActivity : AppCompatActivity(), View.OnClickListener {
    val TAG = NewsLetterSubscriptionActivity::class.java.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newsletter)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.news_letter)

        setViews()

    }

    private fun setViews() {

        btnSave.setOnClickListener(this)
        chSubscription.isChecked = SharedPref.getIntValue(this, news_subscription) == 1
    }

    override fun onClick(v: View) {
        when (v.id) {

            R.id.btnSave -> {

                if (!isNetworkAvailable(this)) {
                    toast(this, getString(R.string.network_error))

                } else {
                    saveSubscription()
                }

            }


            else -> {
            }
        }
    }

    private fun saveSubscription() {
        showProgressDialog(this, chSubscription)
        val map = HashMap<String, String>()
        map["email"] = SharedPref.getStringValue(this, email)
        if (chSubscription.isChecked) {
            map["is_subscribed"] = "1"
        } else {
            map["is_subscribed"] = "0"
        }

        val apiService = RetrofitClientSingleton.getInstance()
        apiService.newsSubscription(map).enqueue(object : retrofit2.Callback<StatusMessageResponse> {
            override fun onFailure(call: Call<StatusMessageResponse>, t: Throwable) {
                toast(this@NewsLetterSubscriptionActivity, getString(R.string.something_went_wrong))
                stopProgress()
            }

            override fun onResponse(call: Call<StatusMessageResponse>, response: Response<StatusMessageResponse>) {
                stopProgress()
                if (response.isSuccessful) {
                    val statusMsgResponse = response.body()
                    when (statusMsgResponse?.status) {

                        SUCCESS -> {
                            if (chSubscription.isChecked) {
                                SharedPref.setIntValue(this@NewsLetterSubscriptionActivity, news_subscription, 1)
                            } else {
                                SharedPref.setIntValue(this@NewsLetterSubscriptionActivity, news_subscription, 0)
                            }
                            toast(this@NewsLetterSubscriptionActivity, statusMsgResponse.message)
                            finish()
                        }

                        FAILURE -> {
                            toast(this@NewsLetterSubscriptionActivity, statusMsgResponse.message)
                        }
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
