package com.lalidea.activities


import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.Window
import com.lalidea.R
import com.lalidea.adapters.MyOrdersListingAdapter
import com.lalidea.listeners.onCancelOrderClickListener
import com.lalidea.models.MyOrderListResponse
import com.lalidea.models.ReOrderModel
import com.lalidea.models.StatusMessageResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_my_order.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_delete_confirm.*
import kotlinx.android.synthetic.main.layout_no_data.*
import retrofit2.Call
import retrofit2.Response
import java.util.*


class MyOrderActivity : AppCompatActivity(), onCancelOrderClickListener {


    val TAG = MyOrderActivity::class.java.simpleName
    private var orderList = ArrayList<MyOrderListResponse.DataBean>()
    var page: Int? = 1
    var totalOrderCount: Int? = 0

    var isLoadingFollowedCategory = false
    var isLastPageFollowedCategory = false
    var layoutManager: LinearLayoutManager? = null

    lateinit var mAdapter: MyOrdersListingAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_order)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.my_orders)

        setViews()
    }

    private fun setViews() {

        layoutManager = LinearLayoutManager(this)
        rvorder.layoutManager = layoutManager
        rvorder.setHasFixedSize(true)
        rvorder.addOnScrollListener(recyclerViewOnScrollListenerFollowedCategory)

        orderList = ArrayList()
        mAdapter = MyOrdersListingAdapter(this@MyOrderActivity, orderList, "all", this)
        rvorder.adapter = mAdapter

        if (!isNetworkAvailable(this@MyOrderActivity)) {
            showNoInternetLayout(llParentNoData)
        } else {
            callMyOrderApi()

        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun callMyOrderApi() {

        showProgressDialog(this, rvorder)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getStringValue(this, userId)
        map["page"] = "" + page
        val apiService = RetrofitClientSingleton.getInstance()
        apiService.getMyOrderList(map).enqueue(object : retrofit2.Callback<MyOrderListResponse> {
            override fun onFailure(call: Call<MyOrderListResponse>, t: Throwable) {
                // toast(this@MyOrderActivity, getString(R.string.something_went_wrong))
                stopProgress()
                toast(this@MyOrderActivity, getString(R.string.something_went_wrong))
                showSomethingWentWrongDataLayout(llParentNoData)
                /* noDataOrder.visibility = View.VISIBLE*/
                isLoadingFollowedCategory = false;
            }

            override fun onResponse(call: Call<MyOrderListResponse>, response: Response<MyOrderListResponse>) {
                stopProgress()
                isLoadingFollowedCategory = false;
                if (response.isSuccessful) {

                    val orderResponse = response.body()
                    when (orderResponse?.status) {
                        SUCCESS -> {
                            totalOrderCount = orderResponse.total_record
                            val list = orderResponse.data

                            Log.e(TAG, "$totalOrderCount , ${mAdapter.itemCount}")

                            if (list != null) {
                                if (list.isNotEmpty()) {
                                    mAdapter.addAll(list);
                                }
                            }

                            if (totalOrderCount == mAdapter.itemCount) {

                                isLastPageFollowedCategory = true
                            }

                        }
                        FAILURE -> {
                            toast(this@MyOrderActivity, orderResponse.message)
                            showSomethingWentWrongDataLayout(llParentNoData)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@MyOrderActivity, orderResponse.message)
                            showSomethingWentWrongDataLayout(llParentNoData)
                        }

                    }

                }
                if (orderList.isEmpty()) {
                    showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                } else {
                    hideNoDataLayout(llParentNoData)
                }

            }

        })

    }

    override fun onCancelOrderClick(orderId: String, position: Int) {
       // callReOrder(orderId)


        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_delete_confirm)
        dialog.show()
        dialog.tvMessageDetailsDele.text = getString(R.string.order_cancel_confirmation)
        dialog.llCancelDialogDelete.setOnClickListener({ dialog.dismiss() })
        dialog.llOkDialogDelete.setOnClickListener({
            dialog.dismiss()
            showProgressDialog(this, rvorder)
            val map = HashMap<String, String>()
            map["order_id"] = orderId!!
            val apiService = RetrofitClientSingleton.getInstance()
            apiService.cancelOrder(map).enqueue(object : retrofit2.Callback<StatusMessageResponse> {
                override fun onFailure(call: Call<StatusMessageResponse>, t: Throwable) {
                    toast(this@MyOrderActivity, getString(R.string.something_went_wrong))
                    stopProgress()
                }

                override fun onResponse(call: Call<StatusMessageResponse>, response: Response<StatusMessageResponse>) {
                    stopProgress()
                    if (response.isSuccessful) {
                        val statusMsgResponse = response.body()
                        when (statusMsgResponse?.status) {
                            SUCCESS -> {
                                toast(this@MyOrderActivity, statusMsgResponse.message)
                                orderList[position].status = "Canceled"
                                mAdapter.notifyItemChanged(position)
                            }

                            FAILURE -> {
                                toast(this@MyOrderActivity, statusMsgResponse.message)
                            }
                        }
                    }
                }
            })
        })








    }



    private fun callReOrder(orderId: String) {
        showProgressDialog(this, rvorder)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getStringValue(this, userId)
        map["order_id"] = orderId
        map["cart_id"] = SharedPref.getIntValue(this, cartId).toString()
        map["device_id"] = getAndroidId(this)

        RetrofitClientSingleton.getInstance().reOrder(map).enqueue(object : retrofit2.Callback<ReOrderModel> {
            override fun onFailure(call: Call<ReOrderModel>, t: Throwable) {
                // toast(this@MyOrderActivity, getString(R.string.something_went_wrong))
                stopProgress()
                toast(this@MyOrderActivity, getString(R.string.something_went_wrong))
            }

            override fun onResponse(call: Call<ReOrderModel>, response: Response<ReOrderModel>) {
                stopProgress()

                if (response.isSuccessful) {
                    val reOrderResponse = response.body()
                    when (reOrderResponse?.status) {
                        SUCCESS -> {
                            if (reOrderResponse.data != null) {
                                SharedPref.setIntValue(this@MyOrderActivity, cartCount, reOrderResponse.data!!.count)
                            } else {
                                toast(this@MyOrderActivity, reOrderResponse.message)
                            }
                            val intent = Intent(this@MyOrderActivity, ShoppingCartActivity::class.java)
                            startActivity(intent)
                        }
                        FAILURE -> {
                            toast(this@MyOrderActivity, reOrderResponse.message)

                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@MyOrderActivity, reOrderResponse.message)

                        }

                    }

                }
            }

        })

    }

    private fun loadMoreItemsFollowedCategory() {
        isLoadingFollowedCategory = true

        page = page?.plus(1)

        callMyOrderApi()
    }

    private val recyclerViewOnScrollListenerFollowedCategory = object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = layoutManager!!.childCount
            val totalItemCount = mAdapter.itemCount
            val firstVisibleItemPosition = layoutManager!!.findFirstVisibleItemPosition()
            Log.e(TAG, "$visibleItemCount , $totalItemCount, $firstVisibleItemPosition")

            if (!isLoadingFollowedCategory && !isLastPageFollowedCategory) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {

                    loadMoreItemsFollowedCategory()
                }
            }
        }
    }
}
