package com.lalidea.activities


import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.view.Window
import com.lalidea.R
import com.lalidea.adapters.WishListAdapter
import com.lalidea.listeners.OnItemUpdateDeleteAddWishList
import com.lalidea.models.AddTCartFromWishListModel
import com.lalidea.models.AddToWishListModel
import com.lalidea.models.WishlistMainResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_my_wishlist.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_delete_confirm.*
import kotlinx.android.synthetic.main.dialog_update_qty.*
import kotlinx.android.synthetic.main.layout_no_data.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class WishListActivity : AppCompatActivity(), View.OnClickListener, OnItemUpdateDeleteAddWishList {
    val TAG = WishListActivity::class.java.simpleName

    private var mAdapter: WishListAdapter? = null
    private var wishList = ArrayList<WishlistMainResponse.DataBean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_wishlist)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.wishlist)
        setViews()
    }

    override fun onResume() {
        super.onResume()
        if (!isNetworkAvailable(this@WishListActivity)) {
            toast(this@WishListActivity, getString(R.string.network_error))
            showNointernet()
        } else {
            callGetWishListApi()
        }
    }

    private fun callGetWishListApi() {
        showProgressDialog(this, rvWishlist)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getStringValue(this, userId)
        val apiService = RetrofitClientSingleton.getInstance()
        apiService.getWishList(map).enqueue(object : retrofit2.Callback<WishlistMainResponse> {
            override fun onFailure(call: Call<WishlistMainResponse>, t: Throwable) {
                // toast(this@WishListActivity, getString(R.string.something_went_wrong))
                stopProgress()
                toast(this@WishListActivity, getString(R.string.something_went_wrong))
                showSomethingWrong()
            }

            override fun onResponse(call: Call<WishlistMainResponse>, response: Response<WishlistMainResponse>) {
                stopProgress()

                if (response.isSuccessful) {
                    val wishListResponse = response.body()
                    when (wishListResponse?.status) {
                        SUCCESS -> {
                            if (wishListResponse.data != null) {
                                if (wishListResponse.data?.size!! > 0) {
                                    wishList.clear()
                                    wishList.addAll(wishListResponse.data!!)
                                    mAdapter?.notifyDataSetChanged()
                                } else {
                                    wishList.clear()
                                    showNodata()
                                    toast(this@WishListActivity, R.string.no_data_fnd)
                                }
                            }
                        }
                        FAILURE -> {
                            toast(this@WishListActivity, wishListResponse.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@WishListActivity, wishListResponse.message)
                        }

                    }

                }
                if (wishList.isEmpty()) {
                    showNodata()
                    noDataWishList.visibility = View.VISIBLE
                } else {
                    noDataWishList.visibility = View.GONE
                    for (wishListModel in wishList) {
                        val inStock = wishListModel.inStock

                        if (inStock == 0) {
                            btnaddalltocart.visibility = View.GONE
                            break
                        } else {
                            btnaddalltocart.visibility = View.VISIBLE
                        }
                    }
                }
            }
        })
    }

    private fun setViews() {
        btnaddalltocart.setOnClickListener(this)

        rvWishlist.layoutManager = LinearLayoutManager(this@WishListActivity)
        mAdapter = WishListAdapter(this@WishListActivity, wishList, this@WishListActivity)
        rvWishlist.adapter = mAdapter
        rvWishlist.addItemDecoration(DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL))

    }

    private fun showNointernet() {
        llWishList.visibility = View.GONE
        btnaddalltocart.visibility = View.GONE

        noDataWishList.visibility = View.VISIBLE
        btnRetry.visibility = View.GONE
        txtNodata.text = getString(R.string.network_error)
//        ivNoData.setImageResource(R.drawable.ic_no_connection)

    }

    private fun showNodata() {

        llWishList.visibility = View.GONE
        btnaddalltocart.visibility = View.GONE
        btnRetry.visibility = View.GONE

        noDataWishList.visibility = View.VISIBLE
        txtNodata.text = getString(R.string.no_items_in_wish_list)
//        ivNoData.setImageResource(R.drawable.ic_no_data)
    }

    private fun showSomethingWrong() {
        btnRetry.visibility = View.GONE
        btnaddalltocart.visibility = View.GONE

        noDataWishList.visibility = View.VISIBLE
        txtNodata.text = getString(R.string.something_went_wrong)
//        ivNoData.setImageResource(R.drawable.ic_something_wrong)

    }


    override fun onClick(v: View) {
        when (v.id) {

            R.id.btnaddalltocart -> {
                callAddAllToCartApi()
            }

            R.id.btnRetry -> {

                if (!isNetworkAvailable(this@WishListActivity)) {
                    toast(this@WishListActivity, getString(R.string.network_error))
                    showNointernet()
                } else {
                    callGetWishListApi()
                }

            }

            else -> {
            }
        }
    }

    private fun callAddAllToCartApi() {
        if (!isNetworkAvailable(this@WishListActivity)) {
            toast(this@WishListActivity, getString(R.string.network_error))
        } else {
            callAddAllApi()
        }
    }

    private fun callAddAllApi() {
        showProgressDialog(this, rvWishlist)

        val map = HashMap<String, String>()

        map["user_id"] = SharedPref.getStringValue(this, userId)

        RetrofitClientSingleton.getInstance().addAllToWishlist(map).enqueue(object : retrofit2.Callback<AddToWishListModel> {
            override fun onFailure(call: Call<AddToWishListModel>?, t: Throwable?) {
                stopProgress()
                toast(this@WishListActivity, R.string.something_went_wrong)
            }

            override fun onResponse(call: Call<AddToWishListModel>?, response: Response<AddToWishListModel>) {
                stopProgress()
                if (response.isSuccessful) {
                    val responseProductCategory = response.body()
                    when (responseProductCategory?.status) {
                        SUCCESS -> {
                            SharedPref.setIntValue(this@WishListActivity, cartCount, responseProductCategory.data!!.count)
                            val intent = Intent(this@WishListActivity, ShoppingCartActivity::class.java)
                            startActivity(intent)
                            finish()
                        }

                        FAILURE -> {
                            toast(this@WishListActivity, responseProductCategory.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@WishListActivity, responseProductCategory.message)
                        }

                    }

                } else {
                    toast(this@WishListActivity, R.string.something_went_wrong)
                }
            }
        })
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onItemUpdate(pos: Int) {
        dialogEditQty(pos)
    }

    override fun onItemAddToCart(pos: Int) {
        callItemAddToCart(pos)
    }

    private fun callItemAddToCart(pos: Int) {
        val map = HashMap<String, String>()

        map["user_id"] = SharedPref.getStringValue(this, userId)
        map["product_id"] = "" + wishList[pos].product_id
        map["qty"] = "" + wishList[pos].qty


        showProgressDialog(this@WishListActivity, rvWishlist)
        val apiService = RetrofitClientSingleton.getInstance()
        apiService.addtoCartFromWishList(map).enqueue(object : retrofit2.Callback<AddTCartFromWishListModel> {
            override fun onFailure(call: Call<AddTCartFromWishListModel>, t: Throwable) {
                stopProgress()
                toast(this@WishListActivity, getString(R.string.something_went_wrong))
            }

            override fun onResponse(call: Call<AddTCartFromWishListModel>?, response: Response<AddTCartFromWishListModel>) {
                stopProgress()
                if (response.isSuccessful) {
                    val statusMsgResponse = response.body()
                    when (statusMsgResponse?.status) {
                        SUCCESS -> {
                            SharedPref.setIntValue(this@WishListActivity, cartCount, statusMsgResponse.data!![0].count)

                            toast(this@WishListActivity, statusMsgResponse.message)

                            wishList.removeAt(pos)
                            mAdapter?.notifyItemRemoved(pos)

                            if (wishList.size == 0) {
                                showNodata()
                            } else {
                                for (wishListModel in wishList) {
                                    val inStock = wishListModel.inStock

                                    if (inStock == 0) {
                                        btnaddalltocart.visibility = View.GONE
                                        break
                                    } else {
                                        btnaddalltocart.visibility = View.VISIBLE
                                    }
                                }
                            }
                        }
                        FAILURE -> {
                            toast(this@WishListActivity, statusMsgResponse.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@WishListActivity, statusMsgResponse.message)
                        }

                    }

                }
            }


        })
    }


    override fun onItemDelete(pos: Int) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_delete_confirm)

        dialog.show()
        dialog.llCancelDialogDelete.setOnClickListener({ dialog.dismiss() })

        dialog.llOkDialogDelete.setOnClickListener({
            dialog.dismiss()

            val map = HashMap<String, String>()
            map["user_id"] = SharedPref.getStringValue(this, userId)
            map["product_id"] = "" + wishList[pos].product_id
            showProgressDialog(this@WishListActivity, rvWishlist)
            val apiService = RetrofitClientSingleton.getInstance()
            apiService.deleteFromWishList(map).enqueue(object : retrofit2.Callback<AddToWishListModel> {
                override fun onFailure(call: Call<AddToWishListModel>, t: Throwable) {
                    stopProgress()
                    toast(this@WishListActivity, getString(R.string.something_went_wrong))

                }

                override fun onResponse(call: Call<AddToWishListModel>?, response: Response<AddToWishListModel>) {
                    stopProgress()
                    if (response.isSuccessful) {
                        val statusMsgResponse = response.body()
                        when (statusMsgResponse?.status) {
                            SUCCESS -> {
                                toast(this@WishListActivity, getString(R.string.removed_wishlist))
                                wishList.removeAt(pos)
                                mAdapter?.notifyItemRemoved(pos)
                                if (wishList.size == 0) {
                                    showNodata()
                                } else {
                                    for (wishListModel in wishList) {
                                        val inStock = wishListModel.inStock

                                        if (inStock == 0) {
                                            btnaddalltocart.visibility = View.GONE
                                            break
                                        } else {
                                            btnaddalltocart.visibility = View.VISIBLE
                                        }
                                    }
                                }
                            }

                            FAILURE -> {
                                toast(this@WishListActivity, statusMsgResponse.message)
                            }

                            ORDER_ALREADY_PLACED -> {
                                toast(this@WishListActivity, statusMsgResponse.message)
                            }
                        }
                    }
                }
            })

        })
    }


    private fun dialogEditQty(pos: Int) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_update_qty)

        dialog.etDialogQty.setText(formatString(wishList[pos].qty!!.toDouble()))
        dialog.etDialogQty.setSelection(dialog.etDialogQty.text.length)
        dialog.show()

        dialog.tvQtyDialogCancel.setOnClickListener { dialog.dismiss() }

        dialog.tvQtyDialogOk.setOnClickListener {
            if (dialog.etDialogQty.text.toString().trim().isEmpty()) {
                toast(this@WishListActivity, getString(R.string.please_enter_proper_qty))
            } else {
                if (!isNetworkAvailable(this@WishListActivity)) {
                    toast(this@WishListActivity, getString(R.string.network_error))
                } else {
                    val map = HashMap<String, String>()

                    map["user_id"] = SharedPref.getStringValue(this, userId)
                    map["product_id"] = "" + wishList[pos].product_id
                    map["qty"] = "" + dialog.etDialogQty.text.toString()

                    showProgressDialog(this@WishListActivity, rvWishlist)
                    val apiService = RetrofitClientSingleton.getInstance()
                    apiService.addToWishlist(map).enqueue(object : retrofit2.Callback<AddToWishListModel> {
                        override fun onFailure(call: Call<AddToWishListModel>, t: Throwable) {
                            stopProgress()
                            toast(this@WishListActivity, getString(R.string.something_went_wrong))
                            dialog.dismiss()
                        }

                        override fun onResponse(call: Call<AddToWishListModel>?, response: Response<AddToWishListModel>) {
                            stopProgress()
                            dialog.dismiss()
                            if (response.isSuccessful) {
                                val statusMsgResponse = response.body()
                                when (statusMsgResponse?.status) {
                                    SUCCESS -> {
                                        toast(this@WishListActivity, statusMsgResponse.message)

                                        wishList[pos].qty = dialog.etDialogQty.text.toString()
                                        mAdapter?.notifyDataSetChanged()
                                        dialog.dismiss()
                                    }

                                    FAILURE -> {
                                        toast(this@WishListActivity, statusMsgResponse.message)
                                    }

                                    ORDER_ALREADY_PLACED -> {
                                        toast(this@WishListActivity, statusMsgResponse.message)
                                    }

                                }

                            }
                        }
                    })
                }
            }
        }
    }

}
