package com.lalidea.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import com.lalidea.R
import com.lalidea.models.UserDataResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_forgot_password.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    val TAG = LoginActivity::class.java.simpleName
    private lateinit var strEmail: String
    private lateinit var strPassword: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setView()
    }

    private fun setView() {
        btnLogin.setOnClickListener(this)
        tvLoginRegister.setOnClickListener(this)
        tvForgotPassword.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnLogin ->
                onClickSignIn()
            R.id.tvLoginRegister -> {
                startActivity(Intent(this, RegistrationActivity::class.java))
//                finish()
            }

            R.id.tvForgotPassword -> {
                forgotPasswordDialog()
            }

            else -> {
            }
        }
    }

    private fun onClickSignIn() {

        if (etEmailLogin?.text.toString().trim().isEmpty()) {
            toast(this@LoginActivity, getString(R.string.enteremail))
        } else if (etPasswordLogin?.text.toString().trim().isEmpty()) {
            toast(this@LoginActivity, getString(R.string.enterpass))
        } else if (!isValidMail(etEmailLogin.text.toString().trim())) {
            toast(this@LoginActivity, getString(R.string.validemail))
        } else {
            strEmail = etEmailLogin?.text.toString()
            strPassword = etPasswordLogin?.text.toString()
            if (!isNetworkAvailable(this@LoginActivity)) {
                toast(this@LoginActivity, getString(R.string.network_error))
            } else {
                btnLogin.hideKeyboard()
                doLogin()

            }

        }
    }

    private fun doLogin() {
        showProgressDialog(this@LoginActivity, btnLogin as View)

        val map = HashMap<String, String>()
        map["email"] = strEmail
        map["password"] = strPassword

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()
        else
            map["cart_id"] = ""

        map["device_id"] = getAndroidId(this)

        RetrofitClientSingleton.getInstance().getLogin(map)
                .enqueue(object : retrofit2.Callback<UserDataResponse> {
                    override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                        stopProgress()
                        toast(this@LoginActivity, getString(R.string.something_went_wrong))
                    }

                    override fun onResponse(call: Call<UserDataResponse>, response: Response<UserDataResponse>) {
                        stopProgress()
                        if (response.isSuccessful) {
                            val userDataResponse = response.body()

                            when (response.body()?.status) {
                                SUCCESS -> {
                                    val userdata = userDataResponse!!.userData
                                    userdata?.let {
                                        SharedPref.setStringValue(this@LoginActivity, email, userdata.email)
                                        SharedPref.setStringValue(this@LoginActivity, userId, userdata.customer_id)
                                        SharedPref.setStringValue(this@LoginActivity, is_active, userdata.is_active)
                                        SharedPref.setStringValue(this@LoginActivity, store_id, userdata.store_id)
                                        SharedPref.setStringValue(this@LoginActivity, firstname, userdata.firstname)
                                        SharedPref.setStringValue(this@LoginActivity, middlename, userdata.middlename)
                                        SharedPref.setStringValue(this@LoginActivity, lastname, userdata.lastname)
                                        SharedPref.setStringValue(this@LoginActivity, default_billing, userdata.default_billing)
                                        SharedPref.setStringValue(this@LoginActivity, default_shipping, userdata.default_shipping)
                                        SharedPref.setIntValue(this@LoginActivity, cartId, userdata.cart_id!!)
                                        SharedPref.setIntValue(this@LoginActivity, cartCount, userdata.count!!)
                                        SharedPref.setIntValue(this@LoginActivity, news_subscription, userdata.is_subscribed!!)
                                        SharedPref.setStringValue(this@LoginActivity, "VIEW", "GRID")
                                        finish()
                                    }

                                    toast(this@LoginActivity, userDataResponse.message!!)

                                }
                                FAILURE -> {
                                    toast(this@LoginActivity, userDataResponse!!.message!!)
                                }
                                else -> {
                                    toast(this@LoginActivity, R.string.something_went_wrong)
                                }
                            }
                        } else {
                            toast(this@LoginActivity, R.string.something_went_wrong)
                        }
                    }
                })
    }

    private fun forgotPasswordDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_forgot_password)

        dialog.show()

        dialog.llCancel.setOnClickListener { dialog.dismiss() }

        dialog.llOk.setOnClickListener {
            val forgotPassEmail: String
            if (dialog.etForgotPassEmail.text.toString().trim().isEmpty()) {
                toast(this@LoginActivity, getString(R.string.enteremail))
            } else if (!isValidMail(dialog.etForgotPassEmail.text.toString().trim())) {
                toast(this@LoginActivity, getString(R.string.validemail))
            } else {
                forgotPassEmail = dialog.etForgotPassEmail.text.toString().trim()
                if (!isNetworkAvailable(this@LoginActivity)) {
                    toast(this@LoginActivity, getString(R.string.network_error))
                } else {
                    callForgotPasswordService(forgotPassEmail, dialog)
                }
            }
        }
    }

    private fun callForgotPasswordService(forgotPassEmail: String, dialog: Dialog) {
        showProgressDialog(this@LoginActivity, llLoginParent)

        val apiService = RetrofitClientSingleton.getInstance()
        apiService.getForgotPassword(forgotPassEmail).enqueue(object : retrofit2.Callback<UserDataResponse> {
            override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                stopProgress()
                Log.e(TAG, t.message)
                toast(this@LoginActivity, t.message!!)
            }

            override fun onResponse(call: Call<UserDataResponse>, response: Response<UserDataResponse>) {
                stopProgress()
                if (response.isSuccessful) {
                    val userDataResponse = response.body()
                    if (dialog.isShowing) dialog.dismiss()
                    userDataResponse?.let {
                        if (userDataResponse.status == SUCCESS) {
                            toast(this@LoginActivity, userDataResponse.message!!)
                        } else {
                            toast(this@LoginActivity, userDataResponse.message!!)
                        }
                    }
                    toast(this@LoginActivity, userDataResponse?.message!!)
                }
            }
        })
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
