package com.lalidea.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.widget.LinearLayout
import com.lalidea.R
import com.lalidea.adapters.ReviewsAdapter
import com.lalidea.models.ProductDetailResponse
import com.lalidea.models.Reviews
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_view_more_review.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.layout_no_data.*
import retrofit2.Call
import retrofit2.Response

class ViewMoreReviewActivity : AppCompatActivity() {

    val TAG = ViewMoreReviewActivity::class.java.simpleName
    private var viewMoreReviewsList: ArrayList<Reviews> = ArrayList()
    var mReviewsAdapter: ReviewsAdapter? = null
    var product_id: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_more_review)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.view_more_reviews)
        setView()
    }

    private fun setView() {
        if (intent.extras != null) {
            product_id = intent.extras.getString("PRODUCT_ID")
        }

        rvViewMoreReview.layoutManager = LinearLayoutManager(this@ViewMoreReviewActivity, LinearLayout.VERTICAL, false)
        mReviewsAdapter = ReviewsAdapter(this@ViewMoreReviewActivity, viewMoreReviewsList)
        rvViewMoreReview.adapter = mReviewsAdapter
        rvViewMoreReview.addItemDecoration(DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL))

        if (!isNetworkAvailable(this@ViewMoreReviewActivity)) {
            showNoInternetLayout(llParentNoData)
        } else {
            getAllReviews()
        }
    }

    private fun getAllReviews() {
        showProgressDialog(this@ViewMoreReviewActivity, rvViewMoreReview)
        val map = HashMap<String, String>()
        map["product_id"] = product_id!!

        val apiService = RetrofitClientSingleton.getInstance()
        apiService.productDetail(map)
                .enqueue(object : retrofit2.Callback<ProductDetailResponse> {

                    override fun onResponse(call: Call<ProductDetailResponse>, response: Response<ProductDetailResponse>) {

                        stopProgress()

                        if (response.isSuccessful) {

                            when (response.body()?.status) {
                                SUCCESS -> {
                                    val productDetailResponse = response.body()
                                    productDetailResponse?.let {

                                        viewMoreReviewsList.clear()

                                        if (productDetailResponse.status!! == SUCCESS) {


                                            val productReviewsList = productDetailResponse.data?.reviews?.review as java.util.ArrayList<Reviews>


                                            //Reviews
                                            viewMoreReviewsList.addAll(productReviewsList)
                                            mReviewsAdapter?.notifyDataSetChanged()


                                        } else {
                                            toast(this@ViewMoreReviewActivity, productDetailResponse.message!!)
                                        }
                                    }
                                }


                                FAILURE -> {
                                    toast(this@ViewMoreReviewActivity, response.body()!!.message)
                                    showSomethingWentWrongDataLayout(llParentNoData)
                                }

                                ORDER_ALREADY_PLACED -> {
                                    toast(this@ViewMoreReviewActivity, response.body()!!.message)
                                    showSomethingWentWrongDataLayout(llParentNoData)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<ProductDetailResponse>, t: Throwable) {
                        stopProgress()
                        Log.e(TAG, t.message)
                        toast(this@ViewMoreReviewActivity, t.message!!)
                        showSomethingWentWrongDataLayout(llParentNoData)
                    }
                })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
