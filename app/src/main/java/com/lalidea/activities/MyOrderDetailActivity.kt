package com.lalidea.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import com.lalidea.R
import com.lalidea.adapters.MyOrdersDetailAdapter
import com.lalidea.models.MyOrderDetailResponse
import com.lalidea.models.ReOrderModel
import com.lalidea.models.StatusMessageResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_my_order_detail.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_get_invoice.*
import kotlinx.android.synthetic.main.layout_no_data.*
import org.json.JSONException
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap


class MyOrderDetailActivity : AppCompatActivity(), View.OnClickListener {
    val TAG = MyOrderDetailActivity::class.java.simpleName
    private var rvProducts: ArrayList<MyOrderDetailResponse.DataBean.ProductBean>? = null
    var order_id: String? = null
    var status: String? = null
    var print_order_id: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_order_detail)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        setViews()

    }

    private fun setViews() {

        if (intent.extras != null) {

            order_id = intent.extras.getString("order_id")
            status = intent.extras.getString("status")
            print_order_id = intent.extras.getString("print_order_id")

            //Setting Title Here
            supportActionBar?.title = "Order #" + print_order_id + " - " + status

            if (!isNetworkAvailable(this@MyOrderDetailActivity)) {
                llOrderDetailParent.visibility = View.GONE
                showNoInternetLayout(llParentNoData)
            } else {
                getViewOrderApi()
            }

        }
        btnReorder.setOnClickListener(this)
        btnGenerateInvoice.setOnClickListener(this)

    }

    private fun getViewOrderApi() {

        showProgressDialog(this, rvItems)
        val map = HashMap<String, String>()
        map["order_id"] = order_id!!

        val apiService = RetrofitClientSingleton.getInstance()
        apiService.getOrderDetail(map).enqueue(object : retrofit2.Callback<MyOrderDetailResponse> {
            override fun onResponse(call: Call<MyOrderDetailResponse>?,
                                    response: Response<MyOrderDetailResponse>?) {

                stopProgress()
                if (response!!.isSuccessful) {
                    val myOrderDetailResponse = response.body()
                    when (myOrderDetailResponse?.status) {

                        SUCCESS -> {

                            if (myOrderDetailResponse.data?.created_at != null) {

                                val convertedDateTime = convertDateTime("yyyy-MM-dd HH:mm:ss", "MMMM dd, yyyy", myOrderDetailResponse.data?.created_at!!)
                                txtDateOrder.text = convertedDateTime
                            }

                            txtDateOrderStatus.text = status
                            if (status != null) {

                                if (status!! == "Canceled") {
                                    txtDateOrderStatus.setTextColor(Color.parseColor("#FF0000"))

                                } else if (status!! == "Complete") {
                                    txtDateOrderStatus.setTextColor(Color.parseColor("#00FF00"))

                                } else {
                                    txtDateOrderStatus.setTextColor(Color.parseColor("#E1823A"))

                                }
                            }

                            txtShippingName.text = myOrderDetailResponse.data?.shippingAddress?.firstname
                            if (myOrderDetailResponse.data?.shippingAddress?.middlename != null) {
                                txtShippingName.append(" " + myOrderDetailResponse.data?.shippingAddress?.middlename)
                            }
                            txtShippingName.append(" " + myOrderDetailResponse.data?.shippingAddress?.lastname)
                            if (myOrderDetailResponse.data?.shippingAddress?.company == null) {
                                txtShippingcompany.visibility = View.GONE
                            } else {
                                txtShippingcompany.text = myOrderDetailResponse.data?.shippingAddress?.company
                            }
                            txtShippingAdd.text = myOrderDetailResponse.data?.shippingAddress?.street
                            txtShippingAdd.append(" " + myOrderDetailResponse.data?.shippingAddress?.city)
                            txtShippingAdd.append(" " + myOrderDetailResponse.data?.shippingAddress?.region)
                            txtShippingAdd.append(" - " + myOrderDetailResponse.data?.shippingAddress?.postcode)
                            txtShippingtelephone.text = resources.getString(R.string.mobile) + myOrderDetailResponse.data?.shippingAddress?.telephone
                            if (myOrderDetailResponse.data?.shippingAddress?.fax == null) {
                                txtShippingfax.visibility = View.GONE
                            } else {
                                txtShippingfax.text = resources.getString(R.string.fax1) + myOrderDetailResponse.data?.shippingAddress?.fax
                            }

                            //Billing info
                            txtBillingName.text = myOrderDetailResponse.data?.billingAddress?.firstname
                            if (myOrderDetailResponse.data?.billingAddress?.middlename != null) {
                                txtBillingName.append(" " + myOrderDetailResponse.data?.billingAddress?.middlename)
                            }
                            txtBillingName.append(" " + myOrderDetailResponse.data?.billingAddress?.lastname)
                            if (myOrderDetailResponse.data?.billingAddress?.company == null) {
                                txtBillingcompany.visibility = View.GONE
                            } else {
                                txtBillingcompany.text = myOrderDetailResponse.data?.billingAddress?.company
                            }
                            txtBillingAdd.text = myOrderDetailResponse.data?.billingAddress?.street
                            txtBillingAdd.append(" " + myOrderDetailResponse.data?.billingAddress?.city)
                            txtBillingAdd.append(" " + myOrderDetailResponse.data?.billingAddress?.region)
                            txtBillingAdd.append(" - " + myOrderDetailResponse.data?.billingAddress?.postcode)
                            txtBillingtelephone.text = resources.getString(R.string.mobile) + myOrderDetailResponse.data?.billingAddress?.telephone
                            if (myOrderDetailResponse.data?.billingAddress?.fax == null) {
                                txtBillingfax.visibility = View.GONE
                            } else {
                                txtBillingfax.text = resources.getString(R.string.fax1) + myOrderDetailResponse.data?.billingAddress?.fax
                            }

                            txtshippingmethod.text = myOrderDetailResponse.data?.shipping_method
                            txtpaymentmethod.text = myOrderDetailResponse.data?.payment_method
                            txtSubTotal.text = myOrderDetailResponse.data?.subtotal
                            txtShippingCharges.text = myOrderDetailResponse.data?.shipping_amount
                            txtGrandTotal.text = myOrderDetailResponse.data?.grand_total

                            if (!myOrderDetailResponse.data?.discount!!.isEmpty()) {
                                llOrderdiscount.visibility = View.VISIBLE
                                txtOrderDiscount.text = myOrderDetailResponse.data?.discount
                            } else {
                                llOrderdiscount.visibility = View.GONE
                            }

                            rvItems.layoutManager = LinearLayoutManager(this@MyOrderDetailActivity, LinearLayout.VERTICAL, false)
                            rvProducts = myOrderDetailResponse.data?.product
                            rvItems.adapter = MyOrdersDetailAdapter(this@MyOrderDetailActivity, rvProducts!!)
                        }
                        FAILURE -> {
                            toast(this@MyOrderDetailActivity, myOrderDetailResponse.message)
                            llOrderDetailParent.visibility = View.GONE
                            showSomethingWentWrongDataLayout(llParentNoData)
                        }
                        ORDER_ALREADY_PLACED -> {
                            toast(this@MyOrderDetailActivity, myOrderDetailResponse.message)
                            llOrderDetailParent.visibility = View.GONE
                            showSomethingWentWrongDataLayout(llParentNoData)
                        }

                    }
                }
                if (rvProducts != null) {
                    if (rvProducts!!.isEmpty()) {
                        llOrderDetailParent.visibility = View.GONE
                        showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                    } else {
                        llOrderDetailParent.visibility = View.VISIBLE
                        hideNoDataLayout(llParentNoData)
                    }
                } else {
                    llOrderDetailParent.visibility = View.GONE
                    showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                }
            }

            override fun onFailure(call: Call<MyOrderDetailResponse>?, t: Throwable?) {
                stopProgress()
                toast(this@MyOrderDetailActivity, getString(R.string.something_went_wrong))
                llOrderDetailParent.visibility = View.GONE
                showSomethingWentWrongDataLayout(llParentNoData)
                /* noOrderDetail.visibility = View.VISIBLE*/
            }

        })
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnReorder -> {
                if (!isNetworkAvailable(this@MyOrderDetailActivity)) {
                    llOrderDetailParent.visibility = View.GONE
                    showNoInternetLayout(llParentNoData)
                    /*toast(this@MyOrderDetailActivity, getString(R.string.network_error))
                    showNointernet()*/
                } else {
                    getReorderApi()
                }
            }
            R.id.btnGenerateInvoice -> {

                if (!isNetworkAvailable(this@MyOrderDetailActivity)) {
                    toast(this@MyOrderDetailActivity, getString(R.string.network_error))
                } else {
                    generateInvoice()
                }

            }

            else -> {
            }
        }
    }

    private fun generateInvoice() {
        showProgressDialog(this, btnGenerateInvoice)

        try {
            val map = java.util.HashMap<String, String>()
            map["order_id"] = order_id!!

            RetrofitClientSingleton.getInstance().sendInvoice(map).enqueue(object :
                    retrofit2.Callback<StatusMessageResponse> {

                override fun onResponse(call: Call<StatusMessageResponse>, response: Response<StatusMessageResponse>) {
                    stopProgress()
                    if (response.isSuccessful) {

                        val generateInvoiceResponse = response.body()

                        when (generateInvoiceResponse?.status) {
                            SUCCESS -> {
                                openInvoice()
                            }

                            FAILURE -> {
                                toast(this@MyOrderDetailActivity, generateInvoiceResponse.message)
                            }

                            ORDER_ALREADY_PLACED -> {
                                toast(this@MyOrderDetailActivity, generateInvoiceResponse.message)
                            }
                        }

                    } else {
                        stopProgress()
                    }
                }

                override fun onFailure(call: Call<StatusMessageResponse>, error: Throwable) {
                    stopProgress()
                    Log.e("Error", error.message)
                    toast(this@MyOrderDetailActivity, error.message)
                }
            })
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    private fun getReorderApi() {
        showProgressDialog(this, btnReorder)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getStringValue(this, userId)
        map["order_id"] = order_id!!
        map["cart_id"] = SharedPref.getIntValue(this, cartId).toString()
        map["device_id"] = getAndroidId(this)

        RetrofitClientSingleton.getInstance().reOrder(map).enqueue(object : retrofit2.Callback<ReOrderModel> {
            override fun onFailure(call: Call<ReOrderModel>, t: Throwable) {
                // toast(this@MyOrderActivity, getString(R.string.something_went_wrong))
                stopProgress()
                toast(this@MyOrderDetailActivity, getString(R.string.something_went_wrong))
                llOrderDetailParent.visibility = View.GONE
                showSomethingWentWrongDataLayout(llParentNoData)
            }

            override fun onResponse(call: Call<ReOrderModel>, response: Response<ReOrderModel>) {
                stopProgress()

                if (response.isSuccessful) {
                    val reOrderResponse = response.body()
                    when (reOrderResponse?.status) {
                        SUCCESS -> {
                            if (reOrderResponse.data != null) {
                                SharedPref.setIntValue(this@MyOrderDetailActivity, cartCount, reOrderResponse.data!!.count)
                            }
                            val intent = Intent(this@MyOrderDetailActivity, ShoppingCartActivity::class.java)
                            startActivity(intent)
                        }
                        FAILURE -> {
                            toast(this@MyOrderDetailActivity, reOrderResponse.message)
                            llOrderDetailParent.visibility = View.GONE
                            showSomethingWentWrongDataLayout(llParentNoData)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@MyOrderDetailActivity, reOrderResponse.message)
                            llOrderDetailParent.visibility = View.GONE
                            showSomethingWentWrongDataLayout(llParentNoData)
                        }

                    }

                }
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun openInvoice() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_get_invoice)

        dialog.show()

        dialog.txtOk.setOnClickListener({
            dialog.dismiss()
        })
    }
}
