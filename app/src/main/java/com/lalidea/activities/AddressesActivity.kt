package com.lalidea.activities


import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.RadioGroup
import com.lalidea.R
import com.lalidea.adapters.AddressListingAdapter
import com.lalidea.listeners.OnClickAdapter
import com.lalidea.models.AditionalAddressMainResponse
import com.lalidea.models.DefaultAddress
import com.lalidea.models.DefaultAddressResponse
import com.lalidea.models.StatusMessageResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_addresses.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_delete_confirm.*
import kotlinx.android.synthetic.main.layout_no_data.*
import retrofit2.Call
import retrofit2.Response
import java.util.*


class AddressesActivity : AppCompatActivity(), View.OnClickListener, RadioGroup.OnCheckedChangeListener, OnClickAdapter {
    val TAG = AddressesActivity::class.java.simpleName
    private var billingDefault: DefaultAddress? = null
    private var shippingDefault: DefaultAddress? = null
    var fromWhere: String? = null
    private var additionalAddressList = ArrayList<DefaultAddress>()
    var mAdapter: AddressListingAdapter? = null
    var sameAddForBillShip = "n"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addresses)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.add_book)
        setViews()
    }

    override fun onResume() {
        super.onResume()
        if (!isNetworkAvailable(this@AddressesActivity)) {
            toast(this@AddressesActivity, getString(R.string.network_error))
            showNoInternet()
        } else {
            callDefaultADDApi()

        }
    }

    private fun setViews() {
        if (intent.hasExtra(fromactivity)) {
            fromWhere = intent.extras.getString(fromactivity)
        }
        fabNewAddress.setOnClickListener(this)
        cardDefaultB.setOnClickListener(this)
        cardDefaultS.setOnClickListener(this)
        ivEditDefaultS.setOnClickListener(this)
        ivEditDefaultB.setOnClickListener(this)
        rdGrp.setOnCheckedChangeListener(this)
        rvAddress.layoutManager = LinearLayoutManager(this)
        mAdapter = AddressListingAdapter(this@AddressesActivity, additionalAddressList, this@AddressesActivity)
        rvAddress.adapter = mAdapter


        rvAddress.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (dy < 0 && !fabNewAddress.isShown())
                    fabNewAddress.show()
                else if (dy > 0 && fabNewAddress.isShown())
                    fabNewAddress.hide()
            }
        })
    }

    private fun showNoInternet() {
        noDataAddress.visibility = View.VISIBLE
        txtNodata.text = getString(R.string.network_error)
//        ivNoData.setImageResource(R.drawable.ic_no_connection)
    }

    private fun callAdditionalAddressApi() {
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getStringValue(this, userId)
        val apiService = RetrofitClientSingleton.getInstance()
        apiService.getAddidionalAddress(map).enqueue(object : retrofit2.Callback<AditionalAddressMainResponse> {
            override fun onFailure(call: Call<AditionalAddressMainResponse>, t: Throwable) {
                toast(this@AddressesActivity, getString(R.string.something_went_wrong))
                when {
                    rdBilling.isChecked -> setBilling()
                    rdShipping.isChecked -> setShipping()
                    rdAddiditional.isChecked -> setAdditional()
                    else -> setBilling()
                }
                stopProgress()
            }

            override fun onResponse(call: Call<AditionalAddressMainResponse>, response: Response<AditionalAddressMainResponse>) {
                stopProgress()

                if (response.isSuccessful) {
                    val additionalAddress = response.body()
                    when (additionalAddress?.status) {
                        SUCCESS -> {

                            if (additionalAddress.data != null) {
                                additionalAddressList.clear()
                                if (additionalAddress.data?.size!! > 0) {

                                    additionalAddressList.addAll(additionalAddress.data!!)
                                } else {
                                    //  toast(this@AddressesActivity, "No additional address found")
                                }
                                mAdapter?.notifyDataSetChanged()

                            }

                            when {
                                rdBilling.isChecked -> setBilling()
                                rdShipping.isChecked -> setShipping()
                                rdAddiditional.isChecked -> setAdditional()
                                else -> setBilling()
                            }
                        }
                        FAILURE -> {
                            toast(this@AddressesActivity, additionalAddress.message)
                            when {
                                rdBilling.isChecked -> setBilling()
                                rdShipping.isChecked -> setShipping()
                                rdAddiditional.isChecked -> setAdditional()
                                else -> setBilling()
                            }
                        }
                    }

                }

            }

        })


    }

    private fun callDefaultADDApi() {
        showProgressDialog(this, rdGrp)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getStringValue(this, userId)
        val apiService = RetrofitClientSingleton.getInstance()
        apiService.getDefaultAddress(map).enqueue(object : retrofit2.Callback<DefaultAddressResponse> {
            override fun onFailure(call: Call<DefaultAddressResponse>, t: Throwable) {
                toast(this@AddressesActivity, getString(R.string.something_went_wrong))

                callAdditionalAddressApi()
            }

            override fun onResponse(call: Call<DefaultAddressResponse>, response: Response<DefaultAddressResponse>) {

                callAdditionalAddressApi()
                if (response.isSuccessful) {
                    val defaultAddress = response.body()
                    when (defaultAddress?.status) {
                        SUCCESS -> {
                            if (defaultAddress.data != null) {
                                if (defaultAddress.data?.billingAddress!!.entityId != null) {
                                    billingDefault = defaultAddress.data?.billingAddress
                                    txtDefaultNameB.text = billingDefault!!.firstname + " " + billingDefault!!.middlename + " " + billingDefault!!.lastname
                                    txtDefaultcompanyB.text = billingDefault!!.company
                                    txtDefaultAtelephoneB.text = "Mobile: " + billingDefault!!.telephone
                                    txtDefaultAtelefaxB.text = "Fax: " + billingDefault!!.fax

                                    txtDefaultAddB.text = billingDefault!!.street + ", " + billingDefault!!.city + ", " + billingDefault!!.region + ", " + billingDefault!!.postcode + "-" + "India."
                                }
                                if (defaultAddress.data?.shippingAddress!!.entityId != null) {
                                    shippingDefault = defaultAddress.data?.shippingAddress
                                    txtDefaultNameS.text = shippingDefault!!.firstname + " " + shippingDefault!!.middlename + " " + shippingDefault!!.lastname
                                    txtDefaultcompanyS.text = shippingDefault!!.company
                                    txtDefaultAtelephoneS.text = "Mobile: " + shippingDefault!!.telephone
                                    txtDefaultAtelefaxS.text = "Fax: " + shippingDefault!!.fax
                                    txtDefaultAddS.text = shippingDefault!!.street + ", " + shippingDefault!!.city + ", " + shippingDefault!!.region + ", " + shippingDefault!!.postcode + "-" + "India."
                                }
                                if (billingDefault != null && shippingDefault != null) {
                                    sameAddForBillShip = if (billingDefault!!.entityId.toString()
                                            == shippingDefault!!.entityId.toString()) {
                                        "y"
                                    } else {
                                        "n"
                                    }
                                }

                            }
                        }
                        FAILURE -> {
                            toast(this@AddressesActivity, defaultAddress.message)

                        }
                    }
                }
            }
        })


    }


    override fun onClick(v: View) {
        when (v.id) {

            R.id.fabNewAddress -> {
                val intent = Intent(this, AddNewAddressActivity::class.java)
                intent.putExtra("FROM", "NEW")
                startActivity(intent)
            }
            R.id.cardDefaultB -> {
                if (fromWhere == chnageactivity) {
                    val intent = Intent()
                    intent.putExtra("address", txtDefaultAddB.text.toString())
                    intent.putExtra("address_id", billingDefault?.entityId)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }

            R.id.cardDefaultS -> {
                if (fromWhere == chnageactivity) {
                    val intent = Intent()
                    intent.putExtra("address", txtDefaultAddS.text.toString())
                    intent.putExtra("address_id", shippingDefault?.entityId)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }
            R.id.ivEditDefaultB -> {
                if (billingDefault != null) {
                    if (billingDefault!!.entityId != null) {
                        val intent = Intent(this, AddNewAddressActivity::class.java)
                        intent.putExtra("FROM", "EDITDEFAULT")
                        intent.putExtra("DEFAULT", "BILLING")
                        intent.putExtra("COMMONADDRESS", sameAddForBillShip)
                        intent.putExtra("ADDDATA", billingDefault)
                        startActivity(intent)
                    }
                }
            }
            R.id.ivEditDefaultS -> {
                if (shippingDefault != null) {

                    if (shippingDefault!!.entityId != null) {
                        val intent = Intent(this, AddNewAddressActivity::class.java)
                        intent.putExtra("FROM", "EDITDEFAULT")
                        intent.putExtra("DEFAULT", "SHIPPING")
                        intent.putExtra("COMMONADDRESS", sameAddForBillShip)
                        intent.putExtra("ADDDATA", shippingDefault)
                        startActivity(intent)
                    }
                }
            }
            else -> {
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCheckedChanged(group: RadioGroup, @IdRes checkedId: Int) {
        when (checkedId) {
            R.id.rdBilling -> setBilling()
            R.id.rdShipping -> setShipping()
            R.id.rdAddiditional -> setAdditional()
        }
    }

    private fun setAdditional() {
        fabNewAddress.show()
        rdBilling.isChecked = false
        rdShipping.isChecked = false
        rdAddiditional.isChecked = true
        cardDefaultB.visibility = View.GONE
        cardDefaultS.visibility = View.GONE
        Log.d("additionalAddressList", "" + additionalAddressList.size)
        if (!additionalAddressList.isEmpty()) {
            rvAddress.visibility = View.VISIBLE
            // txtNoAddress.visibility = View.GONE
            noDataAddress.visibility = View.GONE
        } else {
            // txtNoAddress.visibility = View.VISIBLE

            noDataAddress.visibility = View.VISIBLE
            txtNodata.text = getString(R.string.no_address_found)
//            ivNoData.setImageResource(R.drawable.ic_no_data)

        }
    }

    private fun setShipping() {
        fabNewAddress.show()

        rdBilling.isChecked = false
        rdShipping.isChecked = true
        rdAddiditional.isChecked = false
        rvAddress.visibility = View.GONE
        cardDefaultB.visibility = View.GONE
        if (shippingDefault != null) {

            cardDefaultS.visibility = View.VISIBLE
            //   txtNoAddress.visibility = View.GONE

            noDataAddress.visibility = View.GONE
        } else {
            cardDefaultS.visibility = View.GONE
            // txtNoAddress.visibility = View.VISIBLE

            noDataAddress.visibility = View.VISIBLE
            txtNodata.text = getString(R.string.no_address_found)
//            ivNoData.setImageResource(R.drawable.ic_no_data)
        }
    }

    private fun setBilling() {
        fabNewAddress.show()
        rdBilling.isChecked = true
        rdShipping.isChecked = false
        rdAddiditional.isChecked = false
        rvAddress.visibility = View.GONE
        cardDefaultS.visibility = View.GONE
        if (billingDefault != null) {
            cardDefaultB.visibility = View.VISIBLE
            //txtNoAddress.visibility = View.GONE
            noDataAddress.visibility = View.GONE
        } else {
            cardDefaultB.visibility = View.GONE
            // txtNoAddress.visibility = View.VISIBLE
            noDataAddress.visibility = View.VISIBLE
            txtNodata.text =  getString(R.string.no_address_found)
//            ivNoData.setImageResource(R.drawable.ic_no_data)
        }
    }

    override fun onClickSelect(strItemName: String, pos: Int) {
        if (fromWhere == chnageactivity) {
            val intent = Intent()
            intent.putExtra("address", strItemName)
            intent.putExtra("address_id", additionalAddressList[pos].entityId)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onClick(pos: Int) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_delete_confirm)
        dialog.show()
        dialog.tvMessageDetailsDele.text = getString(R.string.address_delete_confirmation)
        dialog.llCancelDialogDelete.setOnClickListener({ dialog.dismiss() })
        dialog.llOkDialogDelete.setOnClickListener({
            dialog.dismiss()
            showProgressDialog(this, rvAddress)
            val map = HashMap<String, String>()
            map["address_id"] = "" + additionalAddressList[pos].entityId

            val apiService = RetrofitClientSingleton.getInstance()
            apiService.deleteAddress(map).enqueue(object : retrofit2.Callback<StatusMessageResponse> {
                override fun onFailure(call: Call<StatusMessageResponse>, t: Throwable) {
                    toast(this@AddressesActivity, getString(R.string.something_went_wrong))
                    stopProgress()
                }

                override fun onResponse(call: Call<StatusMessageResponse>, response: Response<StatusMessageResponse>) {
                    stopProgress()
                    if (response.isSuccessful) {
                        val statusMsgResponse = response.body()
                        when (statusMsgResponse?.status) {
                            SUCCESS -> {
                                toast(this@AddressesActivity, statusMsgResponse.message)
                                additionalAddressList.removeAt(pos)
                                mAdapter?.notifyItemRemoved(pos)
                            }

                            FAILURE -> {
                                toast(this@AddressesActivity, statusMsgResponse.message)
                            }
                        }
                    }
                }
            })
        })
    }
}
