package com.lalidea.activities

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.webkit.*
import com.lalidea.R
import com.lalidea.models.SuccessModel
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import org.json.JSONException
import retrofit2.Call
import retrofit2.Response

class BrowserActivity : AppCompatActivity() {
    private var progDailog: ProgressDialog? = null
    var TAG = BrowserActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        val pos = intent.extras.getInt("position", 0)
        getDataFromService(pos)
    }

    private fun getDataFromService(pos: Int) {
        val title: String
        val path: String
        when (pos) {
            0 -> {
                path = "privacypolicy"
                title = getString(R.string.privacy_policy)
            }
            1 -> {
                path = "refundsandcancellationpolicy"
                title = getString(R.string.refund_cancellation)
            }
            2 -> {
                path = "termsandconditions"
                title = getString(R.string.terms_and_conditions)

            }
            else -> {
                path = "privacypolicy"
                title = getString(R.string.privacy_policy)
            }
        }

        supportActionBar?.title = title

        showProgressDialog(this, wvPayment)

        try {

            RetrofitClientSingleton.getInstance().getBrowserData(path).enqueue(object :
                    retrofit2.Callback<SuccessModel> {

                override fun onResponse(call: Call<SuccessModel>, response: Response<SuccessModel>) {
                    stopProgress()
                    if (response.isSuccessful) {

                        val browserResponse = response.body()

                        when (browserResponse?.status) {
                            SUCCESS -> {
                                setView(decodeBase64(browserResponse.data!!))
                            }

                            FAILURE -> {
                                toast(this@BrowserActivity, browserResponse.message)
                            }

                            ORDER_ALREADY_PLACED -> {
                                toast(this@BrowserActivity, browserResponse.message)
                            }
                        }

                    } else {
                        stopProgress()
                    }
                }

                override fun onFailure(call: Call<SuccessModel>, error: Throwable) {
                    stopProgress()
                    Log.e(TAG, "onFailure >> ${error.message}")
                    toast(this@BrowserActivity, error.message)
                }
            })
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    private fun setView(webData: String) {
        progDailog = ProgressDialog(this)
        progDailog?.setMessage(getString(R.string.please_wait))
        progDailog?.setCancelable(false)

        wvPayment.webViewClient = MyWebViewClient()
        wvPayment.webChromeClient = MyChromeClient()
        wvPayment.settings.javaScriptEnabled = true

        wvPayment.settings.loadWithOverviewMode = true
        wvPayment.settings.useWideViewPort = true
        wvPayment.settings.builtInZoomControls = true

        wvPayment.settings.setSupportZoom(true)
        wvPayment.settings.javaScriptCanOpenWindowsAutomatically = true
        wvPayment.settings.allowFileAccess = true
        wvPayment.settings.domStorageEnabled = true
        wvPayment.settings.textZoom = 250

        wvPayment.loadData(webData, "text/html; charset=utf-8", "UTF-8");
    }

    private inner class MyWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest?): Boolean {

            Log.e(TAG, "shouldOverrideUrlLoading : url >> ${view.url}")
            // view.loadUrl(url)
            return super.shouldOverrideUrlLoading(view, request)
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageStarted(view, url, null)
            Log.d(TAG, "onPageFinished : url >> $url")
        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            Log.d(TAG, "onReceivedError : error >> $error")
            super.onReceivedError(view, request, error)
        }
    }

    inner class MyChromeClient : WebChromeClient() {
        override fun onPermissionRequest(request: PermissionRequest) {
            this@BrowserActivity.runOnUiThread({
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    request.grant(request.resources)
                }
            })
        }

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)
            if (newProgress == 100) {
                if (progDailog != null) {
                    progDailog?.dismiss()
                }
            } else {
                if (progDailog != null) {
                    progDailog?.show()
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (wvPayment.canGoBack()) {
            wvPayment.goBack()
        } else {
            // Let the system handle the back button
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                if (wvPayment.canGoBack()) {
                    wvPayment.goBack()
                } else {
                    // Let the system handle the back button
                    super.onBackPressed()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}