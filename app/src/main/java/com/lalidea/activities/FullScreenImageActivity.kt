package com.lalidea.activities

import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.lalidea.R
import kotlinx.android.synthetic.main.activity_fullscreen_image.*
import kotlinx.android.synthetic.main.custom_toolbar.*


class FullScreenImageActivity : AppCompatActivity() {

    private val TAG = FullScreenImageActivity::class.java.simpleName

    private var strImageUrl = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen_image)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        if (intent.hasExtra("strImageUrl")) {

            strImageUrl = intent.getStringExtra("strImageUrl")

            Glide.with(this).load(Uri.parse(strImageUrl))
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder).dontAnimate()
                    .into(imageViewFullScreen)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}