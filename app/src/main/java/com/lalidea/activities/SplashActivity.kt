package com.lalidea.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      //  Fabric.with(this, Crashlytics())

        setContentView(com.lalidea.R.layout.activity_splash)
//forceCrash()
        Handler().postDelayed({
            val intent = Intent(this@SplashActivity,
                    MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 2000)

    }

    fun forceCrash() {
        throw RuntimeException("This is a crash")
    }

}
