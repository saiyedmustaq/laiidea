package com.lalidea.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import com.lalidea.R
import com.lalidea.adapters.MyReviewsAdapter
import com.lalidea.models.ReviewsMainResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_my_reviews.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.layout_no_data.*
import kotlinx.android.synthetic.main.layout_per_page.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class MyReviewsActivity : AppCompatActivity() {
    val TAG = MyReviewsActivity::class.java.simpleName

    private var reviewsList = ArrayList<ReviewsMainResponse.DataBean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_reviews)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.my_reviews)
        setViews()

    }

    private fun setViews() {

        val spinnerpage = ArrayList<String>()

        spinnerpage.add("10")
        spinnerpage.add("20")
        spinnerpage.add("50")
        val userAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerpage)
        userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_page.adapter = userAdapter

        if (!isNetworkAvailable(this@MyReviewsActivity)) {
            toast(this@MyReviewsActivity, getString(R.string.network_error))
            showNoInternet()
        } else {
            callMyReviewsApi()
        }


    }

    private fun showNoInternet() {

        noDataReviews.visibility = View.VISIBLE
        txtNodata.text = getString(R.string.network_error)
//        ivNoData.setImageResource(R.drawable.ic_no_connection)

    }

    private fun callMyReviewsApi() {
        showProgressDialog(this, rvReviews)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getStringValue(this, userId)
        val apiService = RetrofitClientSingleton.getInstance()
        apiService.getReviewsByUserId(map).enqueue(object : retrofit2.Callback<ReviewsMainResponse> {
            override fun onFailure(call: Call<ReviewsMainResponse>, t: Throwable) {
                // toast(this@MyReviewsActivity, getString(R.string.something_went_wrong))
                stopProgress()
                toast(this@MyReviewsActivity, getString(R.string.noreviews))
                noDataReviews.visibility = View.VISIBLE
            }

            override fun onResponse(call: Call<ReviewsMainResponse>, response: Response<ReviewsMainResponse>) {
                stopProgress()

                if (response.isSuccessful) {
                    val reviewResponse = response.body()
                    when (reviewResponse?.status) {
                        SUCCESS -> {

                            if (reviewResponse.data != null) {
                                if (reviewResponse.data?.size!! > 0) {
                                    reviewsList = reviewResponse.data as ArrayList<ReviewsMainResponse.DataBean>
                                    rvReviews.layoutManager = LinearLayoutManager(this@MyReviewsActivity)
                                    rvReviews.adapter = MyReviewsAdapter(this@MyReviewsActivity, reviewsList, "all")
                                } else {
                                    toast(this@MyReviewsActivity, getString(R.string.no_review))
                                }
                            }

                        }
                        FAILURE -> {
                            toast(this@MyReviewsActivity, reviewResponse.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@MyReviewsActivity, reviewResponse.message)
                        }

                    }

                }
                if (reviewsList.isEmpty()) {
                    noDataReviews.visibility = View.VISIBLE
                } else {
                    noDataReviews.visibility = View.GONE
                }
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
