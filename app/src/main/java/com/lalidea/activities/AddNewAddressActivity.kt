package com.lalidea.activities


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import com.lalidea.R
import com.lalidea.models.DefaultAddress
import com.lalidea.models.StatusMessageResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_new_address.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class AddNewAddressActivity : AppCompatActivity(), View.OnClickListener {
    val TAG = AddNewAddressActivity::class.java.simpleName
    var fromWhere: String? = null
    var address_id: String = ""
    var sameAddForBillShipp = ""
    var defaultType = ""
    private var defaultAddress: DefaultAddress? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_address)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.add_new_address)
        setViews()
    }

    private fun setViews() {

        btnAddnewaddress.setOnClickListener(this)

        val country = ArrayList<String>()
        country.add("India")
        val userAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, country)
        userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCountry.adapter = userAdapter

        if (intent.extras != null) {
            fromWhere = intent.extras.getString("FROM")
        }
        if (fromWhere == "EDIT") {
            supportActionBar?.title = getString(R.string.edit_add)
            defaultAddress = intent.getSerializableExtra("ADDRESSMODEL") as DefaultAddress

            setData()
        } else if (fromWhere == "EDITDEFAULT") {


            chdefaultbilling.visibility = View.GONE
            chdefaultshipping.visibility = View.GONE
            supportActionBar?.title = getString(R.string.edit_add)

            defaultAddress = intent.getSerializableExtra("ADDDATA") as DefaultAddress
            sameAddForBillShipp = intent.extras.getString("COMMONADDRESS")

            if (sameAddForBillShipp == "y") {
                chdefaultbilling.isChecked = true
                chdefaultshipping.isChecked = true
                chdefaultbilling.isClickable = false
                chdefaultshipping.isClickable = false

                chdefaultbilling.visibility = View.GONE
                chdefaultshipping.visibility = View.GONE

            } else {
                defaultType = intent.extras.getString("DEFAULT")

                if (defaultType == "BILLING") {
                    chdefaultbilling.isChecked = true
                    chdefaultbilling.isClickable = false

                    chdefaultbilling.visibility = View.GONE
                    chdefaultshipping.visibility = View.VISIBLE
                    chdefaultshipping.isChecked = false
                    chdefaultshipping.isClickable = true


                } else {
                    chdefaultbilling.visibility = View.VISIBLE
                    chdefaultshipping.visibility = View.GONE
                    chdefaultshipping.isChecked = true
                    chdefaultshipping.isClickable = false
                    chdefaultbilling.isChecked = false
                    chdefaultbilling.isClickable = true
                }
            }
            setData()

        }
    }

    private fun setData() {


        if (defaultAddress!!.firstname != null) {
            edtAddFname?.setText(defaultAddress!!.firstname)
        }
        if (defaultAddress!!.lastname != null) {
            edtAddLname?.setText(defaultAddress!!.lastname)
        }
        if (defaultAddress!!.middlename != null) {
            edtAddMname?.setText(defaultAddress!!.middlename)
        }
        if (defaultAddress!!.company != null) {
            edtAddCompany?.setText(defaultAddress!!.company)
        }
        if (defaultAddress!!.telephone != null) {
            edtAddtel?.setText(defaultAddress!!.telephone)
        }
        if (defaultAddress!!.fax != null) {
            edtAddfax?.setText(defaultAddress!!.fax)
        }
        if (defaultAddress!!.street != null) {
            edtAddstreet1?.setText(defaultAddress!!.street)
        }
        if (defaultAddress!!.city != null) {
            edtAddcity?.setText(defaultAddress!!.city)
        }
        if (defaultAddress!!.city != null) {
            edtAddstate?.setText(defaultAddress!!.region)
        }
        if (defaultAddress!!.postcode != null) {
            edtAddczip?.setText(defaultAddress!!.postcode)
        }

        address_id = defaultAddress!!.entityId.toString()
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnAddnewaddress -> {
                if (!isNetworkAvailable(this)) {
                    toast(this, getString(R.string.network_error))

                } else {
                    checkValidations()
                }

            }

            else -> {
            }
        }
    }

    private fun checkValidations() {
        if (edtAddFname.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_fname))
            return
        }else if (!isValidText(edtAddFname.text.toString().trim())) {
            toast(this, getString(R.string.enter_fname))
            return
        } else if (edtAddLname.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_lname))
            return
        } else if (!isValidText(edtAddLname.text.toString().trim())) {
            toast(this, getString(R.string.enter_lname))
            return
        } else if (edtAddtel.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_tel))
            return
        } else if (edtAddtel.text.toString().trim().length != 10) {
            toast(this, getString(R.string.enter_tel_valid))
            return
        } else if (edtAddstreet1.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_street))
            return
        } else if (edtAddstreet1.text.toString().trim().length < 3) {
            toast(this, getString(R.string.enter_street_valid))
            return
        } else if (edtAddcity.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_city))
            return
        } else if (edtAddcity.text.toString().trim().length < 3) {
            toast(this, getString(R.string.enter_city_valid))
            return
        } else if (!isValidText(edtAddcity.text.toString().trim())) {
            toast(this, getString(R.string.enter_city_valid))
            return
        } else if (edtAddstate.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_state))
            return
        } else if (edtAddstate.text.toString().trim().length < 3) {
            toast(this, getString(R.string.enter_state_valid))
            return
        } else if (!isValidText(edtAddstate.text.toString().trim())) {
            toast(this, getString(R.string.enter_state_valid))
            return
        } else if (edtAddczip.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_zipcode))
            return
        } else if (edtAddczip.text.toString().trim().length != 6) {
            toast(this, getString(R.string.enter_zipcode_valid))
            return
        }
        if (!isNetworkAvailable(this@AddNewAddressActivity)) {
            toast(this@AddNewAddressActivity, getString(R.string.network_error))
        } else {


            if (fromWhere == "EDIT" || fromWhere == "EDITDEFAULT") {
                callUpdateApi()
            } else {
                callNewAddApi()
            }
        }


    }

    private fun callUpdateApi() {
        val map = HashMap<String, String>()

        map["user_id"] = SharedPref.getStringValue(this, userId)
        map["address_id"] = address_id
        map["firstname"] = edtAddFname.text.toString().trim()
        map["lastname"] = edtAddLname.text.toString().trim()
        if (!edtAddMname.text.toString().trim().isEmpty()) {
            map["middlename"] = edtAddMname.text.toString().trim()
        }
        if (!edtAddCompany.text.toString().trim().isEmpty()) {
            map["company"] = edtAddCompany.text.toString().trim()
        }
        map["telephone"] = edtAddtel.text.toString().trim()
        if (!edtAddfax.text.toString().trim().isEmpty()) {
            map["fax"] = edtAddfax.text.toString().trim()
        }
        map["street"] = edtAddstreet1.text.toString().trim()
        map["city"] = edtAddcity.text.toString().trim()
        map["state"] = edtAddstate.text.toString().trim()
        map["postcode"] = edtAddczip.text.toString().trim()
        map["country_id"] = "IN"
        if (chdefaultbilling.isChecked) {
            map["default_billing"] = "1"
        } else {
            map["default_billing"] = "0"

        }
        if (chdefaultshipping.isChecked) {
            map["default_shipping"] = "1"
        } else {
            map["default_shipping"] = "0"

        }

        showProgressDialog(this@AddNewAddressActivity, btnAddnewaddress as View)

        val apiService = RetrofitClientSingleton.getInstance()
        apiService.editAddress(map).enqueue(object : retrofit2.Callback<StatusMessageResponse> {
            override fun onFailure(call: Call<StatusMessageResponse>, t: Throwable) {
                stopProgress()
                toast(this@AddNewAddressActivity, getString(R.string.something_went_wrong))


            }


            override fun onResponse(call: Call<StatusMessageResponse>, response: Response<StatusMessageResponse>) {
                stopProgress()
                if (response.isSuccessful) {
                    val statusMsgResponse = response.body()
                    when (statusMsgResponse?.status) {
                        SUCCESS -> {
                            toast(this@AddNewAddressActivity, statusMsgResponse.message)
                            finish()

                        }

                        FAILURE -> {
                            toast(this@AddNewAddressActivity, statusMsgResponse.message)
                        }
                    }

                }
            }

        })
    }


    private fun callNewAddApi() {
        val map = HashMap<String, String>()

        map["user_id"] = SharedPref.getStringValue(this, userId)
        map["firstname"] = edtAddFname.text.toString().trim()
        map["lastname"] = edtAddLname.text.toString().trim()
        if (!edtAddMname.text.toString().trim().isEmpty()) {
            map["middlename"] = edtAddMname.text.toString().trim()
        }
        if (!edtAddCompany.text.toString().trim().isEmpty()) {
            map["company"] = edtAddCompany.text.toString().trim()
        }
        map["telephone"] = edtAddtel.text.toString().trim()
        if (!edtAddfax.text.toString().trim().isEmpty()) {
            map["fax"] = edtAddfax.text.toString().trim()
        }
        map["street"] = edtAddstreet1.text.toString().trim()
        map["city"] = edtAddcity.text.toString().trim()
        map["state"] = edtAddstate.text.toString().trim()
        map["postcode"] = edtAddczip.text.toString().trim()
        map["country_id"] = "IN"
        if (chdefaultbilling.isChecked) {
            map["default_billing"] = "1"
        } else {
            map["default_billing"] = "0"

        }
        if (chdefaultshipping.isChecked) {
            map["default_shipping"] = "1"
        } else {
            map["default_shipping"] = "0"

        }

        showProgressDialog(this@AddNewAddressActivity, btnAddnewaddress as View)
        val apiService = RetrofitClientSingleton.getInstance()
        apiService.addNewAddress(map).enqueue(object : retrofit2.Callback<StatusMessageResponse> {
            override fun onFailure(call: Call<StatusMessageResponse>, t: Throwable) {
                stopProgress()
                toast(this@AddNewAddressActivity, getString(R.string.something_went_wrong))

            }

            override fun onResponse(call: Call<StatusMessageResponse>?, response: Response<StatusMessageResponse>) {
                stopProgress()
                if (response.isSuccessful) {
                    val statusMsgResponse = response.body()
                    when (statusMsgResponse?.status) {
                        SUCCESS -> {
                            toast(this@AddNewAddressActivity, statusMsgResponse.message)

                            finish()
                        }

                        FAILURE -> {
                            toast(this@AddNewAddressActivity, statusMsgResponse.message)
                        }

                    }

                }
            }


        })
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
