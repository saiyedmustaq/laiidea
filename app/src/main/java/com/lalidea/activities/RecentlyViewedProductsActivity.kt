package com.lalidea.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.lalidea.R
import com.lalidea.adapters.RecentViewedProductsAdapter
import com.lalidea.appController.AppController
import com.lalidea.database.models.RecentlyViewedProductsModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_recently_viwed.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import java.util.*

class RecentlyViewedProductsActivity : AppCompatActivity() {
    val TAG = RecentlyViewedProductsActivity::class.java.simpleName
    private var recent: ArrayList<RecentlyViewedProductsModel> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recently_viwed)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.recently_viewed_products)

        setViews()
    }

    private fun setViews() {


        rvRecent.layoutManager = LinearLayoutManager(this)
        rvRecent.addItemDecoration(DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL))
        val mAdapter = RecentViewedProductsAdapter(this, recent)
        rvRecent.adapter = mAdapter

        AppController.database?.recentlyViewedProductsDao()!!.getAllRecentlyViewedProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ mAdapter.mRecentProductArrayList = it.asReversed() })

        //        mAdapter.setList(AppController.database?.recentlyViewedProductsDao()!!.getAllRecentlyViewedProducts())

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
