package com.lalidea.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.lalidea.R
import com.lalidea.models.UserDataResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class EditProfileActivity : AppCompatActivity(), View.OnClickListener {
    val TAG = EditProfileActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.account_info)

        setViews()
    }

    private fun setViews() {

        btnUpdateProfile.setOnClickListener(this)
        edtFirstName.setText(SharedPref.getStringValue(this, firstname))
        edtLastName.setText(SharedPref.getStringValue(this, lastname))
        edtMiddleName.setText(SharedPref.getStringValue(this, middlename))
        edtEmail.setText(SharedPref.getStringValue(this, email))
        chChangePwd.setOnCheckedChangeListener { _, _ ->

            if (ll_password.isShown) {
                ll_password.visibility = View.GONE
            } else {
                ll_password.visibility = View.VISIBLE

            }
        }
    }


    override fun onClick(v: View) {
        when (v.id) {

            R.id.btnUpdateProfile -> {

                checkValidations()
            }

            else -> {
            }
        }
    }

    private fun checkValidations() {
        if (edtFirstName.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_fname))
            return
        } else if (edtLastName.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_lname))
            return
        } else if (edtEmail.text.toString().trim().isEmpty()) {
            toast(this, getString(R.string.enter_email))
            return
        } else if (!isValidMail(edtEmail.text.toString().trim())) {
            toast(this, getString(R.string.enter_valid_email))
            return
        } else if (ll_password.isShown) {

            if (edtCurrentPassword.text.toString().trim().isEmpty()) {
                toast(this, getString(R.string.enter_current_pwd))
                return
            } else if (edtNewPassword.text.toString().trim().isEmpty()) {
                toast(this, getString(R.string.enter_new_pwd))
                return
            } else if (edtNewPassword.text.toString().trim().length < 6) {
                toast(this, getString(R.string.enterpass_lentgh))
                return
            } else if (edtConfirmPassword.text.toString().trim().isEmpty()) {
                toast(this, getString(R.string.enter_confirm_pwd))
                return
            } else if (edtNewPassword.text.toString().trim() != edtConfirmPassword.text.toString().trim()) {
                toast(this, getString(R.string.pwd_desnt_match))
                return
            }
        }
        if (!isNetworkAvailable(this)) {
            toast(this, getString(R.string.network_error))
        } else {
            callApi()
        }

    }

    private fun callApi() {
        showProgressDialog(this, btnUpdateProfile as View)

        val map = HashMap<String, String>()

        map["firstname"] = edtFirstName.text.toString().trim()
        map["lastname"] = edtLastName.text.toString().trim()
        if (!edtMiddleName.text.toString().trim().isEmpty()) {
            map["middlename"] = edtMiddleName.text.toString().trim()

        }
        map["email"] = edtEmail.text.toString().trim()
        map["user_id"] = SharedPref.getStringValue(this, userId)

        if (ll_password.isShown) {
            map["change_password"] = "1"
            map["password"] = edtCurrentPassword.text.toString().trim()
            map["new_password"] = edtNewPassword.text.toString().trim()
            map["confirm_new_password"] = edtConfirmPassword.text.toString().trim()
        }
        RetrofitClientSingleton.getInstance().editProfile(map).enqueue(object :
                retrofit2.Callback<UserDataResponse> {
            override fun onResponse(call: Call<UserDataResponse>, response: Response<UserDataResponse>) {
                stopProgress()
                if (response.isSuccessful) {
                    val userDataResponse = response.body()

                    val statusMsgResponse = response.body()
                    when (statusMsgResponse?.status) {
                        SUCCESS -> {
                            userDataResponse?.let {
                                val userdata = userDataResponse.userData
                                userdata?.let {
                                    SharedPref.setStringValue(this@EditProfileActivity, userId, userdata.customer_id)
                                    SharedPref.setStringValue(this@EditProfileActivity, is_active, userdata.is_active)
                                    SharedPref.setStringValue(this@EditProfileActivity, store_id, userdata.store_id)
                                    SharedPref.setStringValue(this@EditProfileActivity, firstname, userdata.firstname)
                                    SharedPref.setStringValue(this@EditProfileActivity, middlename, userdata.middlename)
                                    SharedPref.setStringValue(this@EditProfileActivity, lastname, userdata.lastname)
                                    SharedPref.setStringValue(this@EditProfileActivity, default_billing, userdata.default_billing)
                                    SharedPref.setStringValue(this@EditProfileActivity, default_shipping, userdata.default_shipping)

                                    toast(this@EditProfileActivity, userDataResponse.message!!)
                                    finish()
                                }

                            }

                        }

                        FAILURE -> {
                            toast(this@EditProfileActivity, statusMsgResponse.message)
                        }
                    }


                } else {
                    toast(this@EditProfileActivity, getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<UserDataResponse>, error: Throwable) {
                stopProgress()
                toast(this@EditProfileActivity, getString(R.string.something_went_wrong))
            }

        })

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
