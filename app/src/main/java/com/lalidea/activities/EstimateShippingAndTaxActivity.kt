package com.lalidea.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.ArrayAdapter
import com.lalidea.R
import kotlinx.android.synthetic.main.activity_estimate_shpping_and_tax.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import java.util.*

class EstimateShippingAndTaxActivity : AppCompatActivity() {

    val TAG = EstimateShippingAndTaxActivity::class.java.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_estimate_shpping_and_tax)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.title_estimate_shipping_and_tax)

        val mArrayListCountry = ArrayList<String>()

        mArrayListCountry.add("India")
        mArrayListCountry.add("USA")
        mArrayListCountry.add("UK")

        val mAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item,
                mArrayListCountry)
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spCountry.adapter = mAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
