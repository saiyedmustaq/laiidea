package com.lalidea.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.lalidea.R
import com.lalidea.adapters.ProductAdapter
import com.lalidea.listeners.OnProductItemClickListener
import com.lalidea.models.AddToCartModel
import com.lalidea.models.AddToWishListModel
import com.lalidea.models.ProductDataModel
import com.lalidea.models.ProductDataResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.content_product.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.layout_no_data.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

/**
 * Created by reena on 6/12/17.
 */
class ProductListActivity : AppCompatActivity(), View.OnClickListener, OnProductItemClickListener {

    val TAG = ProductListActivity::class.java.simpleName

    lateinit var dialog: Dialog
    private var sortSelection: Int = 0
    private var showPerPageSelection: Int = 0

    lateinit var productTitle: String
    lateinit var categoryId: String
    lateinit var seachQuery: String

    private var productList = java.util.ArrayList<ProductDataModel>()
    var mAdapter: ProductAdapter? = null

    var isSwitched = true

    var selection: String = "position"

    private var uid: String = ""

    lateinit var cartBadge: LayerDrawable

    var page: Int = 1
    var totalRecords = 0
    var limit = "10"

    var isLoadingProducts = false
    var isLastPageProduct = false
    var linearLayoutManager: LinearLayoutManager? = null
    var gridlayoutManager: GridLayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        setSupportActionBar(toolbar)

        ivGridList.setImageResource(R.drawable.grid)

        ivGridList.setOnClickListener(this)
        ivSort.setOnClickListener(this)
        ivShowItem.setOnClickListener(this)

        linearLayoutManager = LinearLayoutManager(this)
        gridlayoutManager = GridLayoutManager(this, 2)
        rvProduct.layoutManager = linearLayoutManager
        rvProduct.setHasFixedSize(true)
        mAdapter = ProductAdapter(this@ProductListActivity, productList, this@ProductListActivity)
        rvProduct.adapter = mAdapter
        rvProduct.addOnScrollListener(recyclerViewOnScrollListener)

        if (intent.hasExtra("CategoryId")) {
            productTitle = intent.getStringExtra("Name")
            categoryId = intent.getStringExtra("CategoryId")

        } else {
            selection = "Relevance"
            seachQuery = intent.getStringExtra("search_query")
            productTitle = seachQuery
        }

        if (!isNetworkAvailable(this@ProductListActivity)) {
            showNoInternetLayout(llParentNoData)
        } else {
            getListData()
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = productTitle

    }

    override fun onResume() {
        super.onResume()

        uid = SharedPref.getStringValue(this, userId)
        println("$TAG, onResume: ${::cartBadge.isInitialized}: ")
        println("$TAG, onResume: ${SharedPref.getIntValue(this, cartCount)}: ")
        if (::cartBadge.isInitialized)
            setBadgeCount(this, cartBadge, SharedPref.getIntValue(this, cartCount).toString())

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)

        val itemCart = menu.findItem(R.id.menuCart)
        cartBadge = itemCart.icon as LayerDrawable
        setBadgeCount(this, cartBadge, SharedPref.getIntValue(this, cartCount).toString())

        val searchItem: MenuItem = menu.findItem(R.id.menuSearch)

        val searchView = searchItem.actionView as SearchView

        searchView.setOnSearchClickListener { setItemsVisibility(menu, searchItem, false) }
        // Detect SearchView close
        searchView.setOnCloseListener {
            setItemsVisibility(menu, searchItem, true)
            false
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(txt: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.isIconified = true
                searchView.onActionViewCollapsed()
                setItemsVisibility(menu, searchItem, true)
                submitSearchQuery(this@ProductListActivity, query)
                return false
            }

        })

        return true
    }

    private fun setItemsVisibility(menu: Menu, exception: MenuItem,
                                   visible: Boolean) {
        for (i in 0 until menu.size()) {
            val item = menu.getItem(i)
            if (item !== exception)
                item.isVisible = visible
        }
    }

    private fun getProductData() {

        val map = HashMap<String, String>()
        map["cat_id"] = categoryId
        map["limit"] = limit
        map["sort"] = selection
        map["page"] = page.toString()

        showProgressDialog(this, rvProduct)

        RetrofitClientSingleton.getInstance().getProduct(map)
                .enqueue(object : retrofit2.Callback<ProductDataResponse> {

                    override fun onResponse(call: Call<ProductDataResponse>, response: Response<ProductDataResponse>) {
                        isLoadingProducts = false

                        if (page == 1) productList.clear()

                        stopProgress()

                        if (response.isSuccessful) {
                            val responseProductCategory = response.body()
                            when (responseProductCategory?.status) {
                                SUCCESS -> {
                                    val productDataResponse = response.body()
                                    productDataResponse?.let {
                                        val list = productDataResponse.data!!
                                        totalRecords = productDataResponse.total_record
                                        if (list.size > 0) {
                                            mAdapter!!.addAll(list)
                                        } else {
                                            showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                                        }


                                        if (totalRecords <= mAdapter!!.itemCount) {
                                            isLastPageProduct = true
                                        }
                                    }
                                }

                                FAILURE -> {
                                    toast(this@ProductListActivity, responseProductCategory.message)
                                }

                                ORDER_ALREADY_PLACED -> {
                                    toast(this@ProductListActivity, responseProductCategory.message)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<ProductDataResponse>, t: Throwable) {
                        isLoadingProducts = false;

                        stopProgress()
                        // Log.e(TAG, t.message)
                        toast(this@ProductListActivity, t.message!!)
                    }
                })
    }

    override fun onClick(view: View?) {
        when (view?.id) {

            R.id.ivGridList -> {

                isSwitched = mAdapter!!.toggleItemViewType()
                rvProduct.layoutManager =
                        if (isSwitched) linearLayoutManager
                        else gridlayoutManager
                if (isSwitched) {
                    ivGridList.setImageResource(R.drawable.grid)
                } else {
                    ivGridList.setImageResource(R.drawable.ic_list)
                }

                mAdapter?.notifyDataSetChanged()
            }

            R.id.ivSort -> {

                val items = if (intent.hasExtra("CategoryId")) arrayOf<CharSequence>("Position", "Name", "Price")
                else arrayOf<CharSequence>("Relevance", "Name", "Price")

                // Creating and Building the Dialog
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Select Sort By")

                builder.setSingleChoiceItems(items, sortSelection
                ) { dialog, item ->
                    sortSelection = item
                    dialog.dismiss()

                    when (sortSelection) {
                        0 -> selection = if (intent.hasExtra("CategoryId")) "position" else "relevance"
                        1 -> selection = "name"
                        2 -> selection = "price"
                    }

                    if (!isNetworkAvailable(this@ProductListActivity)) {
                        showNoInternetLayout(llParentNoData)
                    } else {
                        page = 1
                        isLastPageProduct = false
                        getListData()
                    }

                }

                dialog = builder.create()
                dialog.show()

            }

            R.id.ivShowItem -> {
                val items = arrayOf("12", "24", "36")

                // Creating and Building the Dialog
                val builder = AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.show_item_per_page))

                builder.setSingleChoiceItems(items, showPerPageSelection
                ) { dialog, item ->
                    showPerPageSelection = item
                    dialog.dismiss()
                    if (!isNetworkAvailable(this@ProductListActivity)) {
                        showNoInternetLayout(llParentNoData)
                    } else {
                        page = 1
                        limit = items[item]
                        getListData()
                    }
                }

                dialog = builder.create()
                dialog.show()

            }
        }
    }

    private fun getListData() {
        if (intent.hasExtra("CategoryId")) {
            getProductData()
        } else {
            getSearchData()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            R.id.menuSearch -> {

            }

            R.id.menuCart -> {
                if (SharedPref.getIntValue(this, cartCount) > 0)
                    startActivity(Intent(this, ShoppingCartActivity::class.java))
                else
                    toast(this, R.string.no_item_in_cart)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onAddToCartClicked(productData: ProductDataModel) {

        if (productData.isInStock!! == 0) {
            toast(this, getString(R.string.out_of_stock))
        } else {

            if (!isNetworkAvailable(this@ProductListActivity)) {
                toast(this@ProductListActivity, getString(R.string.network_error))
            } else {
                addProductToCart(productData)
            }
        }
    }

    override fun onAddToShareClicked(productData: ProductDataModel) {

        startActivity(Intent(this, ShareProductActivity::class.java).putExtra("PRODUCT_ID", productData.productId))
    }

    override fun onAddToWishListClicked(productData: ProductDataModel) {
        if (uid != "") {

            if (!isNetworkAvailable(this@ProductListActivity)) {
                toast(this@ProductListActivity, getString(R.string.network_error))

            } else {
                callApiForAddToWishList(productData)
            }

        } else {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    override fun onItemClicked(productData: ProductDataModel) {

        val intent = Intent(this@ProductListActivity, ProductDetailActivity::class.java)
        val bundle = Bundle()
        bundle.putString("PRODUCT_ID", productData.productId)
        bundle.putString("PRODUCT_NAME", productData.productName)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun callApiForAddToWishList(productData: ProductDataModel) {
        showProgressDialog(this, rvProduct)

        val map = HashMap<String, String>()

        map["product_id"] = productData.productId!!
        map["qty"] = "1"
        map["user_id"] = uid

        RetrofitClientSingleton.getInstance().addToWishlist(map).enqueue(object : retrofit2.Callback<AddToWishListModel> {
            override fun onFailure(call: Call<AddToWishListModel>?, t: Throwable?) {
                stopProgress()
                toast(this@ProductListActivity, R.string.something_went_wrong)
            }

            override fun onResponse(call: Call<AddToWishListModel>?, response: Response<AddToWishListModel>) {
                stopProgress()
                if (response.isSuccessful) {
                    val responseProductCategory = response.body()
                    when (responseProductCategory?.status) {
                        SUCCESS -> {
                            toast(this@ProductListActivity, responseProductCategory.message)
                        }

                        FAILURE -> {
                            toast(this@ProductListActivity, responseProductCategory.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@ProductListActivity, responseProductCategory.message)
                        }

                    }

                } else {
                    toast(this@ProductListActivity, R.string.something_went_wrong)
                }
            }
        })
    }

    private fun addProductToCart(productData: ProductDataModel) {

        showProgressDialog(this, rvProduct)

        val map = HashMap<String, String>()

        val userIdForCart = SharedPref.getStringValue(this, userId)
        if (userIdForCart != "")
            map["user_id"] = userIdForCart

        map["product_id"] = productData.productId!!
        map["qty"] = "1"

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = SharedPref.getIntValue(this, cartId).toString()
        else
            map["cart_id"] = ""

        map["device_id"] = getAndroidId(this)

        RetrofitClientSingleton.getInstance().addToCart(map)
                .enqueue(object : retrofit2.Callback<AddToCartModel> {
                    override fun onFailure(call: Call<AddToCartModel>?, t: Throwable?) {
                        stopProgress()
                        toast(this@ProductListActivity, R.string.something_went_wrong)
                    }

                    override fun onResponse(call: Call<AddToCartModel>?, response: Response<AddToCartModel>) {
                        stopProgress()
                        if (response.isSuccessful) {
                            val responseProductCategory = response.body()
                            when (responseProductCategory?.status) {
                                SUCCESS -> {
                                    SharedPref.setIntValue(this@ProductListActivity, cartCount, responseProductCategory.data!!.count)
                                    SharedPref.setIntValue(this@ProductListActivity, cartId, responseProductCategory.data!!.cart_id)
//                                    toast(this@ProductListActivity, "cart count >> ${responseProductCategory.data!!.count}")
                                    toast(this@ProductListActivity, responseProductCategory.message!!)
                                }

                                FAILURE -> {
                                    toast(this@ProductListActivity, responseProductCategory.message)
                                }

                                ORDER_ALREADY_PLACED -> {
                                    toast(this@ProductListActivity, responseProductCategory.message)
                                }

                            }


                        } else {
                            toast(this@ProductListActivity, R.string.something_went_wrong)
                        }

                        // update cart count
                        onResume()
                    }
                })
    }

    private fun getSearchData() {

        val map = HashMap<String, String>()
        map["keyword"] = seachQuery
        map["limit"] = limit
        map["sort"] = selection
        map["page"] = page.toString()

        showProgressDialog(this, rvProduct)

        RetrofitClientSingleton.getInstance().searchProduct(map)
                .enqueue(object : retrofit2.Callback<ProductDataResponse> {

                    override fun onResponse(call: Call<ProductDataResponse>, response: Response<ProductDataResponse>) {
                        isLoadingProducts = false

                        if (page == 1) productList.clear()

                        stopProgress()

                        if (response.isSuccessful) {
                            val responseProductCategory = response.body()
                            when (responseProductCategory?.status) {
                                SUCCESS -> {
                                    val productDataResponse = response.body()
                                    productDataResponse?.let {
                                        totalRecords = productDataResponse.total_record
                                        val list = productDataResponse.data!!

                                        if (list.size > 0) {
                                            mAdapter!!.addAll(list)
                                        } else {
                                            showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                                        }

                                        if (totalRecords <= mAdapter!!.itemCount) {
                                            isLastPageProduct = true
                                        }
                                    }
                                }

                                FAILURE -> {
                                    toast(this@ProductListActivity, responseProductCategory.message)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<ProductDataResponse>, t: Throwable) {
                        isLoadingProducts = false

                        stopProgress()
                        // Log.e(TAG, t.message)
                        toast(this@ProductListActivity, t.message!!)
                    }
                })
    }

    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val visibleItemCount = if (isSwitched) {
                linearLayoutManager!!.childCount
            } else {
                gridlayoutManager!!.childCount
            }

            val totalItemCount = mAdapter!!.itemCount

            val firstVisibleItemPosition = if (isSwitched) {
                linearLayoutManager!!.findFirstVisibleItemPosition()
            } else {
                gridlayoutManager!!.findFirstVisibleItemPosition()
            }

            Log.d(TAG, "$visibleItemCount , $totalItemCount, $firstVisibleItemPosition")

            if (!isLoadingProducts && !isLastPageProduct) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                    loadMoreProducts()
                }
            }
        }
    }

    private fun loadMoreProducts() {
        isLoadingProducts = true

        page += 1

        getListData()
    }
}