package com.lalidea.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.lalidea.R
import com.lalidea.utils.SharedPref
import com.lalidea.utils.cartCount
import com.lalidea.utils.coupenCode
import kotlinx.android.synthetic.main.activity_order_success.*

class OrderSuccessActivity : AppCompatActivity(), View.OnClickListener {

    val TAG = OrderSuccessActivity::class.java.simpleName
    var order_id: String? = null
    var print_order_id: String? = null
    var paymentStatus: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_success)

        setViews()

        SharedPref.setStringValue(this@OrderSuccessActivity, coupenCode, "")

    }

    private fun setViews() {

        if (intent.hasExtra("print_order_id")) {
            order_id = intent.extras.getString("order_id")
            print_order_id = intent.extras.getString("print_order_id")
            paymentStatus = intent.extras.getString("PaymentStatus")

            if (paymentStatus.equals("Success")) {

                tvThanksPurchaseMsg.visibility = View.VISIBLE
                txtOrderId.visibility = View.VISIBLE
                tvPaymentNote.visibility = View.VISIBLE

                tvPaymentStatus.text = this@OrderSuccessActivity.getString(R.string.order_received)
                tvThanksPurchaseMsg.text = this@OrderSuccessActivity.getString(R.string.thank_purchase_msg)

                tvPaymentNote.text = this@OrderSuccessActivity.getString(R.string.tynote)

                txtOrderId.text = "Your order # is : " + print_order_id

                //  SharedPref.setIntValue(this, cartId, 0)
                SharedPref.setIntValue(this, cartCount, 0)

            } else if (paymentStatus.equals("Failure")) {

                tvPaymentStatus.text = this@OrderSuccessActivity.getString(R.string.failure_payment)
                tvThanksPurchaseMsg.visibility = View.INVISIBLE
                txtOrderId.text = "Your order # is : " + print_order_id
                txtOrderId.visibility = View.VISIBLE
                tvPaymentNote.visibility = View.INVISIBLE
            }
        }else{
            tvPaymentStatus.text = this@OrderSuccessActivity.getString(R.string.failure_payment)
            tvThanksPurchaseMsg.visibility = View.INVISIBLE
            txtOrderId.visibility = View.INVISIBLE
            tvPaymentNote.visibility = View.INVISIBLE
        }

        btncontinueShopping.setOnClickListener(this)
    }


    override fun onClick(v: View) {
        when (v.id) {

            R.id.btncontinueShopping -> {

                redirectToHome()

            }

            else -> {
            }
        }
    }

    private fun redirectToHome() {
        val i = Intent(this, MainActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        redirectToHome()
    }
}
