package com.lalidea.activities

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.webkit.*
import android.widget.LinearLayout
import com.lalidea.R
import com.lalidea.models.PaymentOrderStatusResponse
import com.lalidea.models.PlaceOrderId
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_cancel_payment.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

/**
 * Created by reena on 26/12/17.
 */
class PaymentActivity : AppCompatActivity(), View.OnClickListener {

    var TAG = PaymentActivity::class.java.simpleName
    var orderId: String? = null
    var printOrderId: String? = null
    var paymentMethod: String? = null
    private var progDailog: ProgressDialog? = null

    var paymentUrl = ""
    var billingId = ""
    var shippingId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.payment)

        if (intent.hasExtra("print_order_id")) {
            orderId = intent.extras.getString("order_id")
            printOrderId = intent.extras.getString("print_order_id")
            paymentMethod = intent.extras.getString("paymentmethod")
            print("$TAG, paymentMethod >> $paymentMethod")
            paymentUrl = when (paymentMethod) {
                "paytm_cc" -> "https://www.lalidea.com/index.php/paytm/processing/redirect?order_id=$orderId&access_type=mobile"
                else -> "https://www.lalidea.com/attune-myws/index/payuMoneyUrl?order_id=$orderId&payment_method=pumcp"
            }
        } else if (intent.hasExtra("paymentmethod")) {
            paymentMethod = intent.extras.getString("paymentmethod")
            Log.e(TAG, "paymentMethod >> $paymentMethod")
            billingId = intent.extras.getString("billingId")
            shippingId = intent.extras.getString("shippingId")
            paymentUrl = "https://www.lalidea.com/attune-myws/payment/paypal?customerid=${SharedPref.getStringValue(this, userId)}"
        }

        setView()
    }

    private fun setView() {

        progDailog = ProgressDialog(this)
        progDailog?.setMessage(getString(R.string.please_wait))
        progDailog?.setCancelable(false)

        wvPayment.webViewClient = MyWebViewClient()
        wvPayment.webChromeClient = MyChromeClient()
        wvPayment.settings.javaScriptEnabled = true

        wvPayment.settings.loadWithOverviewMode = true
        wvPayment.settings.useWideViewPort = true

        wvPayment.settings.builtInZoomControls = false
        wvPayment.settings.setSupportZoom(false)
        wvPayment.settings.javaScriptCanOpenWindowsAutomatically = true
        wvPayment.settings.allowFileAccess = true
        wvPayment.settings.domStorageEnabled = true

        wvPayment.loadUrl(paymentUrl)
    }

    private inner class MyWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest?): Boolean {

            Log.e(TAG, "shouldOverrideUrlLoading : url >> ${view.url}")
            // view.loadUrl(url)
            return super.shouldOverrideUrlLoading(view, request)
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageStarted(view, url, null)
            Log.e(TAG, "onPageFinished : url >> $url")

            val intent = Intent(this@PaymentActivity, OrderSuccessActivity::class.java)

            if (paymentMethod == "paypal_express") {
                if (url.contains("success")) {
                    val urlSuccess = Uri.parse(url)
                    val token = urlSuccess.getQueryParameter("token")
                    val payerId = urlSuccess.getQueryParameter("PayerID")

                    Log.e(TAG, "onPageFinished : token >> $token")
                    Log.e(TAG, "onPageFinished : payerId >> $payerId")
                    callPlaceOrderApi(token, payerId)
                } else if (url.contains("failure")) {
                    Log.e("payment", "Failure")
                    intent.putExtra("PaymentStatus", "Failure")
//                    intent.putExtra("order_id", order_id)
//                    intent.putExtra("print_order_id", print_order_id)
                    this@PaymentActivity.startActivity(intent)
                }
            } else {
                if (url.contains("success")) {

                    Log.e("payment", "Success")

                    intent.putExtra("PaymentStatus", "Success")
                    intent.putExtra("order_id", orderId)
                    intent.putExtra("print_order_id", printOrderId)
                    this@PaymentActivity.startActivity(intent)

                } else if (url.contains("failure")) {

                    Log.e("payment", "Failure")
                    intent.putExtra("PaymentStatus", "Failure")
                    intent.putExtra("order_id", orderId)
                    intent.putExtra("print_order_id", printOrderId)
                    this@PaymentActivity.startActivity(intent)
                }
            }

        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            Log.e(TAG, "onReceivedError : error >> $error")
            super.onReceivedError(view, request, error)
        }
    }

    fun cancelledPaymentDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_cancel_payment)

        dialog.show()

        dialog.llCancelPayment.setOnClickListener({
            dialog.dismiss()
        })

        dialog.llOkPayment.setOnClickListener({
            dialog.dismiss()

            if (!isNetworkAvailable(this@PaymentActivity)) {
                toast(this@PaymentActivity, getString(R.string.network_error))
            } else {
                if (paymentMethod == "paypal_express") {
                    val intent = Intent(this@PaymentActivity, OrderSuccessActivity::class.java)
                    intent.putExtra("PaymentStatus", "Failure")
                    startActivity(intent)
                } else {
                    cancelledPayment(dialog.llOkPayment)
                }
            }
        })
    }

    private fun cancelledPayment(llOkPayment: LinearLayout) {
        val map = HashMap<String, String>()

        map["order_id"] = orderId!!
        map["status"] = "1"

        showProgressDialog(this@PaymentActivity, llOkPayment as View)

        val apiService = RetrofitClientSingleton.getInstance()

        apiService.paymentApplyForOrder(map).enqueue(object : retrofit2.Callback<PaymentOrderStatusResponse> {

            override fun onFailure(call: Call<PaymentOrderStatusResponse>, t: Throwable) {
                stopProgress()
                toast(this@PaymentActivity, getString(R.string.something_went_wrong))
            }

            override fun onResponse(call: Call<PaymentOrderStatusResponse>?, response: Response<PaymentOrderStatusResponse>) {
                stopProgress()
                if (response.isSuccessful) {
                    val paymentOrderStatusResponse = response.body()

                    when (paymentOrderStatusResponse?.status) {

                        SUCCESS -> {

                            val intent = Intent(this@PaymentActivity, OrderSuccessActivity::class.java)
                            intent.putExtra("print_order_id", printOrderId)
                            intent.putExtra("PaymentStatus", "Failure")
                            startActivity(intent)
                        }

                        FAILURE -> {
                            toast(this@PaymentActivity, paymentOrderStatusResponse.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@PaymentActivity, paymentOrderStatusResponse.message)
                        }

                    }
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
        }
    }

    override fun onBackPressed() {

        if (wvPayment.canGoBack()) {
            wvPayment.goBack()
        } else {
            // Let the system handle the back button

            cancelledPaymentDialog()


        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {

                cancelledPaymentDialog()

            }
        }
        return super.onOptionsItemSelected(item)
    }

    inner class MyChromeClient : WebChromeClient() {
        override fun onPermissionRequest(request: PermissionRequest) {
            this@PaymentActivity.runOnUiThread({
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    request.grant(request.resources)
                }
            })
        }

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)

            if (newProgress == 100) {
                if (progDailog != null) {
                    progDailog?.dismiss()
                }

            } else {
                if (progDailog != null) {
                    progDailog?.show()
                }
            }
        }
    }

    override fun onStop() {
        if (progDailog != null && progDailog!!.isShowing) {
            progDailog?.dismiss()
        }

        progDailog = null

        super.onStop()
    }

    private fun callPlaceOrderApi(token: String, payerId: String) {

        showProgressDialog(this, wvPayment)

        val map = HashMap<String, String>()

        map["user_id"] = SharedPref.getStringValue(this, userId)
        map["cart_id"] = SharedPref.getIntValue(this, cartId).toString()
        map["billingaddress_id"] = billingId
        map["shippingadress_id"] = shippingId
        map["payment_method"] = "paypal_express"
        map["token"] = token
        map["PayerID"] = payerId
        map["device_id"] = getAndroidId(this)

        RetrofitClientSingleton.getInstance().placePaypalOrder(map)
                .enqueue(object : retrofit2.Callback<PlaceOrderId> {
                    override fun onFailure(call: Call<PlaceOrderId>?, t: Throwable?) {
                        stopProgress()
                        toast(this@PaymentActivity, R.string.something_went_wrong)
                    }

                    override fun onResponse(call: Call<PlaceOrderId>?, response: Response<PlaceOrderId>) {
                        stopProgress()
                        if (response.isSuccessful) {
                            val responseProductCategory = response.body()
                            when (responseProductCategory?.status) {
                                SUCCESS -> {
                                    val orderId = responseProductCategory.data?.order_detail?.order_id
                                    val printOrderId = responseProductCategory.data?.order_id

                                    val i = Intent(this@PaymentActivity, OrderSuccessActivity::class.java)
                                    i.putExtra("order_id", orderId)
                                    i.putExtra("print_order_id", printOrderId)
                                    i.putExtra("PaymentStatus", "Success")
                                    startActivity(i)
                                }

                                FAILURE -> {
                                    toast(this@PaymentActivity, responseProductCategory.message)
                                }

                                ORDER_ALREADY_PLACED -> {
                                    orderAlreadyPlacedDialog(this@PaymentActivity, response.body()!!.message!!)
                                }
                            }

                        } else {
                            toast(this@PaymentActivity, R.string.something_went_wrong)
                        }
                    }
                })
    }
}
