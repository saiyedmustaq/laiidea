package com.lalidea.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.lalidea.R
import com.lalidea.adapters.CheckoutReviewOrderAdapter
import com.lalidea.models.DefaultAddress
import com.lalidea.models.DefaultAddressResponse
import com.lalidea.models.PlaceOrderId
import com.lalidea.models.ShoppingCartResponseModel
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_order_checkout.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import retrofit2.Call
import retrofit2.Response
import java.util.*


class OrderCheckOutActivity : AppCompatActivity(), View.OnClickListener {
    val TAG = OrderCheckOutActivity::class.java.simpleName

    var billingClick = "y"
    var shippingClick = "n"
    var shippingMethodClick = "n"
    var paymentClick = "n"
    var reviewClick = "n"
    var shippingId = ""
    var billingId = ""
    var paymentmethod = ""
    private var billingDefault: DefaultAddress? = null
    private var shippingDefault: DefaultAddress? = null
    private var arrayList: ArrayList<ShoppingCartResponseModel.DataBean.ProductBean> = ArrayList()

    private var mAdapter: CheckoutReviewOrderAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_checkout)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.checkout)

        setViews()

    }

    private fun setViews() {

        txtShippingMethod.text = "Free Shipping\nFree 0.00"
        btnBillingCont.setOnClickListener(this)
        btnShippingCont.setOnClickListener(this)
        btnShippingMethodCont.setOnClickListener(this)
        btnPaymentCont.setOnClickListener(this)
        btnPlaceOrder.setOnClickListener(this)


        txtShippingTitle.setOnClickListener(this)
        txtBillingTitle.setOnClickListener(this)
        txtShippingMethodTitle.setOnClickListener(this)
        txtPaymentTitle.setOnClickListener(this)
        txtOrderReviewTitle.setOnClickListener(this)
        txtChangeShippingAdd.setOnClickListener(this)
        txtChangeBillingAdd.setOnClickListener(this)


        rvItemsReview.layoutManager = LinearLayoutManager(this)

        mAdapter = CheckoutReviewOrderAdapter(this, arrayList)
        rvItemsReview.adapter = mAdapter
        rvItemsReview.isNestedScrollingEnabled = false

        if (!isNetworkAvailable(this@OrderCheckOutActivity)) {
            toast(this@OrderCheckOutActivity, getString(R.string.network_error))
        } else {
            callDefaultAddApi()
            getReviewItems()
        }
    }


    private fun getReviewItems() {


        val map = HashMap<String, String>()

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()

        val userIdForCart = SharedPref.getStringValue(this, userId)
        if (userIdForCart != "")
            map["user_id"] = userIdForCart
        map["device_id"] = getAndroidId(this)

        RetrofitClientSingleton.getInstance().getShoppingCartItems(map)
                .enqueue(object : retrofit2.Callback<ShoppingCartResponseModel> {
                    override fun onFailure(call: Call<ShoppingCartResponseModel>, t: Throwable) {

                        toast(this@OrderCheckOutActivity, getString(R.string.something_went_wrong))
                    }

                    override fun onResponse(call: Call<ShoppingCartResponseModel>, response: Response<ShoppingCartResponseModel>) {

                        if (response.isSuccessful) {
                            when (response.body()?.status) {
                                SUCCESS -> {
                                    val cartResponse = response.body()

                                    cartResponse?.let {

                                        if (cartResponse.data!!.product == null) {
                                            println("$TAG, getShoppingCartItems >> null")
                                        }

                                        val list = cartResponse.data!!.product
                                        if (!list!!.isEmpty()) {

                                            //  txtSubTotalCheckout.text = "Rs. 66,700.00"
                                            txtShippingChargesCheckout.text = "0.00"
                                            // txtGrandTotalCheckout.text = "Rs. 66,700.00"

                                            val cartTotal = cartResponse.data!!.total
                                            txtSubTotalCheckout.text = cartTotal!!.subtotal
                                            txtGrandTotalCheckout.text = cartTotal.grand_total
                                            if (!cartTotal.discount!!.isEmpty()) {
                                                lldiscount.visibility = View.VISIBLE
                                                txtShippingDiscount.text = cartTotal.discount
                                            } else {
                                                lldiscount.visibility = View.GONE
                                            }
                                            arrayList.addAll(list)
                                            mAdapter?.notifyDataSetChanged()
                                        }
                                    }
                                }

                                FAILURE -> {
                                    toast(this@OrderCheckOutActivity, response.body()!!.message)
                                }

                                ORDER_ALREADY_PLACED -> {
                                    orderAlreadyPlacedDialog(this@OrderCheckOutActivity, response.body()!!.message!!)
                                }
                            }
                        }
                    }

                })
    }

    private fun callDefaultAddApi() {
        showProgressDialog(this, txtBillingTitle)

        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getStringValue(this, userId)
        val apiService = RetrofitClientSingleton.getInstance()

        apiService.getDefaultAddress(map).enqueue(object : retrofit2.Callback<DefaultAddressResponse> {
            override fun onFailure(call: Call<DefaultAddressResponse>, t: Throwable) {
                toast(this@OrderCheckOutActivity, getString(R.string.something_went_wrong))
                stopProgress()

            }

            override fun onResponse(call: Call<DefaultAddressResponse>, response: Response<DefaultAddressResponse>) {
                stopProgress()

                if (response.isSuccessful) {
                    val defaultAddress = response.body()
                    when (defaultAddress?.status) {
                        SUCCESS -> {
                            if (defaultAddress.data != null) {

                                if (defaultAddress.data?.billingAddress!!.entityId != null) {
                                    billingDefault = defaultAddress.data?.billingAddress


                                    var fulladd = billingDefault!!.street
                                    if (billingDefault!!.city != null) {
                                        fulladd = fulladd + ", " + billingDefault!!.city
                                    }
                                    if (billingDefault!!.region != null) {
                                        fulladd = fulladd + ", " + billingDefault!!.region
                                    }
                                    if (billingDefault!!.postcode != null) {
                                        fulladd = fulladd + ", " + billingDefault!!.postcode
                                    }
                                    fulladd = fulladd + " -India"
                                    txtAddressBilling.text = fulladd
                                    billingId = "" + billingDefault!!.entityId
                                }




                                if (defaultAddress.data?.shippingAddress!!.entityId != null) {
                                    shippingDefault = defaultAddress.data?.shippingAddress

                                    var fulladd = shippingDefault!!.street
                                    if (shippingDefault!!.city != null) {
                                        fulladd = fulladd + ", " + shippingDefault!!.city
                                    }
                                    if (shippingDefault!!.region != null) {
                                        fulladd = fulladd + ", " + shippingDefault!!.region
                                    }
                                    if (shippingDefault!!.postcode != null) {
                                        fulladd = fulladd + ", " + shippingDefault!!.postcode
                                    }
                                    fulladd = fulladd + " -India"
                                    txtAddressShipping.text = fulladd
                                    shippingId = "" + shippingDefault!!.entityId

                                }
                            }
                        }
                        FAILURE -> {
                            toast(this@OrderCheckOutActivity, defaultAddress.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@OrderCheckOutActivity, defaultAddress.message)
                        }

                    }

                }


            }


        })

        setBillingView()
    }


    override fun onClick(v: View) {
        when (v.id) {

            R.id.txtBillingTitle -> {

                if (billingClick == "y") {
                    setBillingView()

                }
            }
            R.id.txtShippingTitle -> {
                if (shippingClick == "y") {
                    setShippingView()
                }

            }
            R.id.txtShippingMethodTitle -> {
                if (shippingMethodClick == "y") {
                    setShippingMethodView()
                }

            }
            R.id.txtPaymentTitle -> {
                if (paymentClick == "y") {
                    setPaymentView()
                }


            }
            R.id.txtOrderReviewTitle -> {
                if (reviewClick == "y") {
                    setOrderReview()

                }
            }
            R.id.btnBillingCont -> {
                if (billingId.isEmpty()) {

                    toast(this@OrderCheckOutActivity, getString(R.string.select_billing))
                    return
                }
                if (chUseAsShipping.isChecked) {

                    shippingId = billingId
                    txtAddressShipping.text = txtAddressBilling.text.toString()
                    setShippingMethodView()
                } else {
                    setShippingView()
                }

            }
            R.id.btnShippingCont -> {

                if (shippingId.isEmpty()) {

                    toast(this@OrderCheckOutActivity, getString(R.string.select_shipping))
                    return
                }
                setShippingMethodView()
            }
            R.id.btnShippingMethodCont -> {

                setPaymentView()
            }
            R.id.btnPaymentCont -> {
                if (rdpayumoney.isChecked || rdcod.isChecked || rdpaypal.isChecked || rdpaytm.isChecked) {
                    setOrderReview()
                } else {
                    toast(this@OrderCheckOutActivity, getString(R.string.select_payment))
                }

            }
            R.id.btnPlaceOrder -> {

                if (rdpaypal.isChecked) {
                    val i = Intent(this@OrderCheckOutActivity, PaymentActivity::class.java)
                    i.putExtra("paymentmethod", "paypal_express")
                    i.putExtra("billingId", billingId)
                    i.putExtra("shippingId", shippingId)
                    startActivity(i)
                } else {
                    callPlaceOrderApi()
                }

            }
            R.id.txtChangeShippingAdd -> {
                val i = Intent(this, AddressesActivity::class.java)
                i.putExtra(fromactivity, chnageactivity)
                startActivityForResult(i, SHIPPING)
            }

            R.id.txtChangeBillingAdd -> {
                val i = Intent(this, AddressesActivity::class.java)
                i.putExtra(fromactivity, chnageactivity)
                startActivityForResult(i, BILLING)
            }
            else -> {
            }
        }
    }

    private fun callPlaceOrderApi() {
        when {
            rdpayumoney.isChecked -> paymentmethod = "pumcp"
            rdpaypal.isChecked -> paymentmethod = "paypal_express"
            rdcod.isChecked -> paymentmethod = "cashondelivery"
            rdpaytm.isChecked -> paymentmethod = "paytm_cc"
        }

        Log.d("paymentmethod", "" + paymentmethod)
        Log.d("billid", "" + billingId)
        Log.d("shipid", "" + shippingId)

        showProgressDialog(this, rvItemsReview)

        val map = HashMap<String, String>()

        map["user_id"] = SharedPref.getStringValue(this, userId)
        map["cart_id"] = SharedPref.getIntValue(this, cartId).toString()
        map["billingaddress_id"] = billingId
        map["shippingadress_id"] = shippingId
        map["payment_method"] = paymentmethod
        map["device_id"] = getAndroidId(this)

        RetrofitClientSingleton.getInstance().placeOrder(map)
                .enqueue(object : retrofit2.Callback<PlaceOrderId> {
                    override fun onFailure(call: Call<PlaceOrderId>?, t: Throwable?) {
                        stopProgress()
                        toast(this@OrderCheckOutActivity, R.string.something_went_wrong)
                    }

                    override fun onResponse(call: Call<PlaceOrderId>?, response: Response<PlaceOrderId>) {
                        stopProgress()
                        if (response.isSuccessful) {
                            val responseProductCategory = response.body()
                            when (responseProductCategory?.status) {
                                SUCCESS -> {
                                    val orderId = responseProductCategory.data?.order_detail?.order_id
                                    val printOrderId = responseProductCategory.data?.order_id


                                    if (rdcod.isChecked) {
                                        val i = Intent(this@OrderCheckOutActivity, OrderSuccessActivity::class.java)
                                        i.putExtra("order_id", orderId)
                                        i.putExtra("print_order_id", printOrderId)
                                        i.putExtra("PaymentStatus", "Success")
                                        startActivity(i)

                                    } else {

                                        println("!!!!!!!!Order CheckOut orderId===" + orderId)
                                        val i = Intent(this@OrderCheckOutActivity, PaymentActivity::class.java)
                                        i.putExtra("order_id", orderId)
                                        i.putExtra("print_order_id", printOrderId)
                                        i.putExtra("paymentmethod", paymentmethod)
                                        startActivity(i)
                                    }
                                }

                                FAILURE -> {
                                    toast(this@OrderCheckOutActivity, responseProductCategory.message)
                                }

                                ORDER_ALREADY_PLACED -> {
                                    orderAlreadyPlacedDialog(this@OrderCheckOutActivity, response.body()!!.message!!)
                                }
                            }

                        } else {
                            toast(this@OrderCheckOutActivity, R.string.something_went_wrong)
                        }
                    }
                })
    }

    private fun setOrderReview() {
        billingClick = "y"
        shippingClick = "y"
        shippingMethodClick = "y"
        paymentClick = "y"
        reviewClick = "y"

        llBilling.visibility = View.GONE
        llShipping.visibility = View.GONE
        llShippingMethod.visibility = View.GONE
        llPayment.visibility = View.GONE
        llOrdderReview.visibility = View.VISIBLE
    }

    private fun setPaymentView() {
        billingClick = "y"
        shippingClick = "y"
        shippingMethodClick = "y"
        paymentClick = "y"
        reviewClick = "n"

        llBilling.visibility = View.GONE
        llShipping.visibility = View.GONE
        llShippingMethod.visibility = View.GONE
        llPayment.visibility = View.VISIBLE
        llOrdderReview.visibility = View.GONE
    }

    private fun setShippingMethodView() {

        billingClick = "y"
        shippingClick = "y"
        shippingMethodClick = "y"
        paymentClick = "n"
        reviewClick = "n"

        llBilling.visibility = View.GONE
        llShipping.visibility = View.GONE
        llShippingMethod.visibility = View.VISIBLE
        llPayment.visibility = View.GONE
        llOrdderReview.visibility = View.GONE

    }

    private fun setShippingView() {

        billingClick = "y"
        shippingClick = "y"
        shippingMethodClick = "n"
        paymentClick = "n"
        reviewClick = "n"
        if (shippingId.isEmpty()) {
            txtAddressShipping.text = getString(R.string.no_default_address_please_add_default_address)
        }
        llBilling.visibility = View.GONE
        llShipping.visibility = View.VISIBLE
        llShippingMethod.visibility = View.GONE
        llPayment.visibility = View.GONE
        llOrdderReview.visibility = View.GONE


    }

    private fun setBillingView() {

        billingClick = "y"
        shippingClick = "n"
        shippingMethodClick = "n"
        paymentClick = "n"
        reviewClick = "n"
        if (billingId.isEmpty()) {
            txtAddressBilling.text = getString(R.string.no_default_address_please_add_default_address)
        }
        llBilling.visibility = View.VISIBLE
        llShipping.visibility = View.GONE
        llShippingMethod.visibility = View.GONE
        llPayment.visibility = View.GONE
        llOrdderReview.visibility = View.GONE

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val address = data.extras!!.getString("address")

                if (requestCode == SHIPPING) {
                    shippingId = data.extras!!.getString("address_id")

                    txtAddressShipping.text = address
                } else if (requestCode == BILLING) {
                    billingId = data.extras!!.getString("address_id")
                    txtAddressBilling.text = address
                }
            }


        }
    }

}
