package com.lalidea.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import com.lalidea.R
import com.lalidea.adapters.MenuAdapter
import com.lalidea.fragments.*
import com.lalidea.listeners.OnGetMenuId
import com.lalidea.models.MenuModel
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_app_update.*
import kotlinx.android.synthetic.main.dialog_logout.*
import kotlinx.android.synthetic.main.layout_no_data.*
import kotlinx.android.synthetic.main.menu_layout.view.*
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import kotlinx.android.synthetic.main.nav_header_main.view.rlHome
import org.jsoup.Jsoup
import retrofit2.Call
import retrofit2.Response
import java.util.ArrayList


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, OnGetMenuId {


    val TAG = MainActivity::class.java.simpleName

    private lateinit var handler: Handler

    private lateinit var fragment: Fragment

    private var mFragmentManager: FragmentManager? = null

    private var selectedItem = 0

    private var sayBackPress: Long = 0

    var linearLayoutManager: LinearLayoutManager? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar.title = getString(R.string.home)


        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)

        drawerLayout.addDrawerListener(toggle)

        toggle.syncState()

        handler = Handler()

        navigationView.setNavigationItemSelectedListener(this)

        //set first item selected by default
        selectedItem = R.id.navHome
        navigate(R.id.navHome)
//        if (!BuildConfig.DEBUG)
//            CheckForUpdate().execute()


        getDrawerMenu()
        linearLayoutManager = LinearLayoutManager(this)


    }

    override fun onResume() {
        super.onResume()
        setDrawerHeader()
        changeLoginNavigationItem()
        closeDrawer()
    }

    private fun setDrawerHeader() {

        val uid = SharedPref.getStringValue(this, userId)
        if (uid != "") {
            navigationView.getHeaderView(0).llisLogin.visibility = View.VISIBLE
            navigationView.getHeaderView(0).llNoLogin.visibility = View.GONE
            navigationView.getHeaderView(0).tvHeaderName.text = SharedPref.getStringValue(this, firstname) +
                    " " + SharedPref.getStringValue(this, lastname)
            navigationView.getHeaderView(0).tvHeaderEmail.text = SharedPref.getStringValue(this, email)
            navigationView.getHeaderView(0).rlLogout.visibility = View.VISIBLE
            navigationView.getHeaderView(0).rlLogout.visibility = View.VISIBLE
            navigationView.getHeaderView(0).rlHome.visibility = View.VISIBLE
            navigationView.getHeaderView(0).rlLogout.setOnClickListener {
                logoutConfirmation()
            }

        } else {
            navigationView.getHeaderView(0).llisLogin.visibility = View.GONE
            navigationView.getHeaderView(0).llNoLogin.visibility = View.VISIBLE
            navigationView.getHeaderView(0).rlLogout.visibility = View.GONE

        }


        navigationView.getHeaderView(0).rlHome.setOnClickListener {
            fragment = HomeFragment.newInstance(HOME)
            toolbar.title = getString(R.string.home)
            selectedItem = R.id.navHome
            loadFragment()
            closeDrawer()
        }

        navigationView.getHeaderView(0).rlLogout.setOnClickListener {
            logoutConfirmation()
        }

        navigationView.getHeaderView(0).llNavHeader.setOnClickListener(View.OnClickListener {
            closeDrawer()
            val userId = SharedPref.getStringValue(this, userId)
            if (userId == "") {
                startActivity(Intent(this, LoginActivity::class.java))
            } else {
                selectedItem = R.id.navAccount
                navigate(R.id.navAccount)
            }

            return@OnClickListener

        })
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        //close drawer
        closeDrawer()

        //check if same item selected from navigation drawer
//        if (selectedItem != id) {
        handler.postDelayed({
            //update selected item
            //navigate to selected fragment
            navigate(id)
        }, 300)
//        }

        return true
    }

    /**
     * close drawer
     */
    private fun closeDrawer() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    override fun onClicMenu(categoryId: String?, categoryName: String?) {

    }

    override fun getMenuId(categoryId: String?, categoryName: String?, subcategory: ArrayList<MenuModel.DataBean.CategoryBean.SubCategory>?) {
        val productListIntent = Intent(this@MainActivity, ProductListActivity::class.java)
        val subCategoryIntent = Intent(this@MainActivity, SubCategoryActivity::class.java)

        if (subcategory!!.isEmpty()) {
            productListIntent.putExtra("CategoryId", categoryId)
            productListIntent.putExtra("Name", categoryName)
            startActivity(productListIntent)
        } else {
            Log.e(TAG, "Empty")
            subCategoryIntent.putExtra("CategoryId", categoryId)
            subCategoryIntent.putExtra("Name", categoryName)
            subCategoryIntent.putParcelableArrayListExtra("Categories", subcategory)
            startActivity(subCategoryIntent)
        }


        Log.e(TAG, "Cat $categoryId")
    }

    /**
     * Navigation Drawer item clicks
     * */
    private fun navigate(id: Int) {

        when (id) {
            R.id.navHome -> {
                fragment = HomeFragment.newInstance(HOME)
                toolbar.title = getString(R.string.home)
                selectedItem = R.id.navHome
                loadFragment()
            }
            R.id.navPosters -> {
                fragment = ProductSubCategoryFragment.newInstance(POSTERS)
                toolbar.title = getString(R.string.posters)
                selectedItem = R.id.navPosters
                loadFragment()
            }
            R.id.navFrames -> {
                fragment = ProductSubCategoryFragment.newInstance(FRAMES)
                toolbar.title = getString(R.string.frames)
                selectedItem = R.id.navFrames
                loadFragment()
            }

            R.id.navStationery -> {
                fragment = ProductSubCategoryFragment.newInstance(STATIONERY)
                toolbar.title = getString(R.string.stationery)
                selectedItem = R.id.navStationery
                loadFragment()
            }
            R.id.navSelfiePrint -> {
                fragment = PrintYourPhotoFragment.newInstance()
                toolbar.title = getString(R.string.selfie_print)
                selectedItem = R.id.navSelfiePrint
                loadFragment()
            }

            R.id.navIslamicArt -> {
                fragment = ProductSubCategoryFragment.newInstance(ISLAMICART)
                toolbar.title = getString(R.string.islamic_art)
                selectedItem = R.id.navIslamicArt
                loadFragment()
            }


            R.id.navWallArt -> {
                fragment = ProductSubCategoryFragment.newInstance(WALLART)
                toolbar.title = getString(R.string.wall_art)
                selectedItem = R.id.navWallArt
                loadFragment()
            }


            R.id.navCorporateTrophy -> {
                fragment = ProductSubCategoryFragment.newInstance(WALLART)
                toolbar.title = getString(R.string.corporate_trophy)
                selectedItem = R.id.navCorporateTrophy
                loadFragment()
            }

            R.id.navContactUs -> {
                fragment = ContactUsFragment.newInstance()
                toolbar.title = getString(R.string.contact_us)
                selectedItem = R.id.navContactUs
                loadFragment()
            }
            R.id.navAboutUs -> {
                fragment = AboutUsFragment.newInstance()
                toolbar.title = getString(R.string.about_us)
                selectedItem = R.id.navAboutUs
                loadFragment()
            }
            R.id.navLegal -> {
                fragment = LegalFragment.newInstance()
                toolbar.title = getString(R.string.legal)
                selectedItem = R.id.navLegal
                loadFragment()
            }

            R.id.navAccount -> {
                val uid = SharedPref.getStringValue(this, userId)
                if (uid == "") {
                    toolbar.title = getString(R.string.home)
                    selectedItem = R.id.navHome
                    navigationView.setCheckedItem(R.id.navHome)
                    startActivity(Intent(this, LoginActivity::class.java))
                } else {
                    fragment = AccountMenuFragment.newInstance(ACCOUNT)
                    toolbar.title = getString(R.string.account)
                    selectedItem = R.id.navAccount
                    loadFragment()
                }
            }

            R.id.navLogout -> {
                logoutConfirmation()
            }
            R.id.navLogin -> {
                navigationView.setCheckedItem(selectedItem)
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }

    }

    private fun logoutConfirmation() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_logout)
        dialog.show()
        dialog.llLogoutDialogTitleCancel.setOnClickListener { dialog.dismiss() }
        dialog.llLogoutDialogTitleYes.setOnClickListener {
            dialog.dismiss()
            SharedPref.clear(this)
            startActivity(Intent(this@MainActivity, MainActivity::class.java))
            toast(this@MainActivity, R.string.logout_success)
            finish()
        }
    }

    /**
     * load fragment
     */
    private fun loadFragment() {
//        fragment = fragmentClass!!.newInstance() as Fragment
        mFragmentManager = this.supportFragmentManager
        val ft = mFragmentManager?.beginTransaction()
        ft?.replace(R.id.frameLayout, fragment, selectedItem.toString())
        ft?.addToBackStack(selectedItem.toString())
        ft?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft?.commit()
    }

//


    override fun onBackPressed() {
        /*if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else if (supportFragmentManager.backStackEntryCount == 1) {
            if (sayBackPress + 2000 > System.currentTimeMillis()) {
                finishAffinity()
            } else {
                toast(this, "Press once again to exit!")
                sayBackPress = System.currentTimeMillis()
            }
        } else {

            if (supportFragmentManager.backStackEntryCount > 1) {
                val fragmentTag = supportFragmentManager.getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 2).name.toInt()

                when (fragmentTag) {
                    R.id.navHome -> {
                        selectedItem = R.id.navHome
                        toolbar.title = getString(R.string.home)
                        navigationView.setCheckedItem(R.id.navHome)
                    }
                    R.id.navAccount -> {
                        selectedItem = R.id.navAccount
                        toolbar.title = getString(R.string.account)
                        navigationView.setCheckedItem(R.id.navAccount)
                    }
                    R.id.navFrames -> {
                        selectedItem = R.id.navFrames
                        selectedItem = R.id.navFrames
                        toolbar.title = getString(R.string.frames)
                        navigationView.setCheckedItem(R.id.navFrames)
                    }
                    R.id.navPosters -> {
                        selectedItem = R.id.navPosters
                        toolbar.title = getString(R.string.posters)
                        navigationView.setCheckedItem(R.id.navPosters)
                    }
                    R.id.navIslamicArt -> {
                        selectedItem = R.id.navIslamicArt
                        toolbar.title = getString(R.string.islamic_art)
                        navigationView.setCheckedItem(R.id.navIslamicArt)
                    }

                    R.id.navWallArt -> {
                        selectedItem = R.id.navWallArt
                        toolbar.title = getString(R.string.wall_art)
                        navigationView.setCheckedItem(R.id.navWallArt)
                    }
                    R.id.navCorporateTrophy -> {
                        selectedItem = R.id.navCorporateTrophy
                        toolbar.title = getString(R.string.corporate_trophy)
                        navigationView.setCheckedItem(R.id.navCorporateTrophy)
                    }
                    R.id.navStationery -> {
                        selectedItem = R.id.navStationery
                        toolbar.title = getString(R.string.stationery)
                        navigationView.setCheckedItem(R.id.navStationery)
                    }
                    R.id.navSelfiePrint -> {
                        selectedItem = R.id.navSelfiePrint
                        toolbar.title = getString(R.string.selfie_print)
                        navigationView.setCheckedItem(R.id.navSelfiePrint)
                    }
                    R.id.navContactUs -> {
                        selectedItem = R.id.navContactUs
                        toolbar.title = getString(R.string.contact_us)
                        navigationView.setCheckedItem(R.id.navContactUs)
                    }
                    R.id.navAboutUs -> {
                        selectedItem = R.id.navAboutUs
                        toolbar.title = getString(R.string.about_us)
                        navigationView.setCheckedItem(R.id.navAboutUs)
                    }
                    R.id.navLegal -> {
                        selectedItem = R.id.navLegal
                        toolbar.title = getString(R.string.legal)
                        navigationView.setCheckedItem(R.id.navLegal)
                    }
                }
            }
            super.onBackPressed()
        }*/

    }

    private fun changeLoginNavigationItem() {
        val navMenu = navigationView.menu
        val userId = SharedPref.getStringValue(this, userId)
        if (userId == "") {
            /* navMenu.findItem(R.id.navLogout).isVisible = false
             navMenu.findItem(R.id.navLogin).isVisible = true*/
        } else {
            /*navMenu.findItem(R.id.navLogout).isVisible = true
            navMenu.findItem(R.id.navLogin).isVisible = false*/
        }

    }

    private inner class CheckForUpdate : AsyncTask<String, String, String>() {

        var pInfo = packageManager.getPackageInfo(packageName, 0)
        val oldVersion: String = pInfo.versionName
        lateinit var newVersion: String

        override fun doInBackground(vararg urls: String): String? {

            newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" +
                    packageName + "&hl=en")
                    .timeout(30000)
                    .userAgent(
                            "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com").get()
                    .select("div[itemprop=softwareVersion]").first()
                    .ownText()

            Log.d("new Version", newVersion)
            Log.d("old Version", oldVersion)

            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (newVersion == oldVersion) {
                Log.d(TAG, "Update not available version : $newVersion")
            } else {
                appUpdateDialog(newVersion)
                Log.d(TAG, "Update available version : $newVersion")
            }
        }
    }

    private fun appUpdateDialog(newVersion: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_app_update)

        dialog.show()

        dialog.tvMessageUpdate.text = "New version $newVersion is available!"

        dialog.llDialogLater.setOnClickListener { dialog.dismiss() }

        dialog.llDialogUpdate.setOnClickListener {
            dialog.dismiss()
            val appPackageName = packageName // getPackageName() from Context or Activity object
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
            } catch (e: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
            }

        }
    }

    private fun getDrawerMenu() {
        RetrofitClientSingleton.getInstance()
                .getDrawerMenu()
                .enqueue(object : retrofit2.Callback<MenuModel> {
                    override fun onFailure(call: Call<MenuModel>, t: Throwable) {
                        Log.e(TAG, t.message)
                    }

                    override fun onResponse(call: Call<MenuModel>, response: Response<MenuModel>) {
                        if (response.isSuccessful) {
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    navigationView.getHeaderView(0).rvMenu.layoutManager = linearLayoutManager
                                    val menuResponse = response.body()!!.data
                                    val menuAdapter = MenuAdapter(this@MainActivity, menuResponse, this@MainActivity)
                                    navigationView.getHeaderView(0).rvMenu.adapter = menuAdapter

                                }
                                FAILURE -> {
                                    toast(this@MainActivity, response.body()!!.message)
                                    showSomethingWentWrongDataLayout(llParentNoData)
                                }
                            }
                        }
                    }

                })
    }
}
