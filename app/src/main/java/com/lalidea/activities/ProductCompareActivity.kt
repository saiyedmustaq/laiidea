package com.lalidea.activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Window
import android.widget.LinearLayout
import com.lalidea.R
import com.lalidea.adapters.ProductCompareAdapter
import com.lalidea.listeners.OnProductRemoveFromCompare
import com.lalidea.models.ProductsCompareModel
import com.lalidea.utils.toast
import kotlinx.android.synthetic.main.activity_product_compare.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_delete_confirm.*

class ProductCompareActivity : AppCompatActivity(), OnProductRemoveFromCompare {

    val TAG = MainActivity::class.java.simpleName

    var productcompareList: ArrayList<ProductsCompareModel> = ArrayList()
    var obj_adapter: ProductCompareAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_compare)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.compare_product)
        setView()
    }

    private fun setView() {
        comparerecyclerview.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        comparerecyclerview.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL))
        val urls: ArrayList<String> = arrayListOf("http://www.lalidea.com/media/catalog/product/cache/1/small_image/125x125/9df78eab33525d08d6e5fb8d27136e95/g/r/graphic9.jpg",
                "http://www.lalidea.com/media/catalog/product/cache/1/small_image/125x125/9df78eab33525d08d6e5fb8d27136e95/g/r/graphic7.jpg",
                "http://www.lalidea.com/media/catalog/product/cache/1/small_image/125x125/9df78eab33525d08d6e5fb8d27136e95/g/r/graphic1.jpg",
                "http://www.lalidea.com/media/catalog/product/cache/1/small_image/125x125/9df78eab33525d08d6e5fb8d27136e95/a/l/always_keep_it_opern_12x18.jpg")
        val brandModel = ProductsCompareModel()
        brandModel.thumb = ""
        brandModel.producttitle = ""
        brandModel.price = ""
        brandModel.description = "Description"
        brandModel.shortdescription = "Short description "
        brandModel.sku = "SKU"
        productcompareList.add(brandModel)
        obj_adapter = ProductCompareAdapter(this, productcompareList, this)
        for (item in urls.indices) {
            val brandModel1 = ProductsCompareModel()
            brandModel1.thumb = urls[item]
            brandModel1.producttitle = "Product Title"
            brandModel1.price = "Rs 100.00"
            brandModel1.description = "This is long description of this product >> This is long description of this product >> " +
                    "This is long description of this product >> This is long description of this product >> This is long description of this product"
            brandModel.shortdescription = "This is short description of this product"
            brandModel.sku = "SKU"
            productcompareList.add(brandModel1)
        }

        obj_adapter = ProductCompareAdapter(this, productcompareList, this)

        comparerecyclerview.adapter = obj_adapter

        comparerecyclerview.isNestedScrollingEnabled = false
    }

    override fun onItemClicked(pos: Int) {

    }

    override fun onCompareRemove(pos: Int) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_delete_confirm)

        dialog.show()

        dialog.llCancelDialogDelete.setOnClickListener({ dialog.dismiss() })

        dialog.llOkDialogDelete.setOnClickListener({
            dialog.dismiss()
            productcompareList.removeAt(pos)
            obj_adapter?.notifyItemRemoved(pos)

        })
    }

    override fun onCompare_addtocart(pos: Int) {
        toast(this@ProductCompareActivity, "Add to cart clicked $pos")
    }

    override fun onCompare_addtoWishlist(pos: Int) {
        toast(this@ProductCompareActivity, "Add to wishlist $pos")
    }
}
