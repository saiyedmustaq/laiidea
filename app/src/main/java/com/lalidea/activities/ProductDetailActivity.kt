package com.lalidea.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ScrollView
import com.bumptech.glide.Glide
import com.lalidea.R
import com.lalidea.adapters.GalleryAdapter
import com.lalidea.adapters.ReviewsAdapter
import com.lalidea.appController.AppController
import com.lalidea.database.models.RecentlyViewedProductsModel
import com.lalidea.listeners.OnClickAdapter
import com.lalidea.models.*
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.content_product_detail.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_update_qty.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

/**
 * Created by reena on 6/12/17.
 */
class ProductDetailActivity : AppCompatActivity(), View.OnClickListener, OnClickAdapter {

    lateinit var cartBadge: LayerDrawable
    val TAG = ProductDetailActivity::class.java.simpleName
    var quentity: Int = 0

    private var reviewsList: ArrayList<Reviews> = ArrayList()
    private var galleryList: ArrayList<GalleryData> = ArrayList()

    private var mGalleryAdapter: GalleryAdapter? = null
    var mReviewsAdapter: ReviewsAdapter? = null

    private var uid: String = ""

    var productId: String = ""
    var productName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        setSupportActionBar(toolbar)

        val bundle = intent.extras
        productId = bundle!!.getString("PRODUCT_ID")
        productName = bundle.getString("PRODUCT_NAME")

        val recentlyViewedProductsModel = RecentlyViewedProductsModel(id = productId, name = productName)

        Single.fromCallable {
            AppController.database?.recentlyViewedProductsDao()!!
                    .insertRecentlyViewedProduct(recentlyViewedProductsModel)
        }.subscribeOn(Schedulers.newThread()).subscribe()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = productName
        setView()

        deleteRecentViewProduct()
    }

    @SuppressLint("CheckResult")
    private fun deleteRecentViewProduct() {

        Single.fromCallable {
            AppController.database?.recentlyViewedProductsDao()!!.getAllRecentlyViewedProductsCount()
        }.subscribeOn(Schedulers.io())
                ?.subscribeOn(Schedulers.newThread())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe { list ->
                    kotlin.run {
                        if (list.size > 10) {
                            Log.e(TAG, "deleteRecentViewProduct : deleted product >> ${list[0].name}")
                            Single.fromCallable {
                                AppController.database?.recentlyViewedProductsDao()!!.deleteRecentlyViewedProduct(list[0])
                            }.subscribeOn(Schedulers.io())
                                    ?.subscribeOn(Schedulers.newThread())
                                    ?.observeOn(AndroidSchedulers.mainThread())
                                    ?.subscribe()
                        }
                    }
                }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)

        val itemCart = menu.findItem(R.id.menuCart)
        cartBadge = itemCart.icon as LayerDrawable
        setBadgeCount(this, cartBadge, SharedPref.getIntValue(this, cartCount).toString())

        val searchItem: MenuItem = menu.findItem(R.id.menuSearch)

        val searchView = searchItem.actionView as SearchView

        searchView.setOnSearchClickListener { setItemsVisibility(menu, searchItem, false) }
        // Detect SearchView close
        searchView.setOnCloseListener {
            setItemsVisibility(menu, searchItem, true)
            false
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(txt: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.isIconified = true
                searchView.onActionViewCollapsed()
                setItemsVisibility(menu, searchItem, true)
                submitSearchQuery(this@ProductDetailActivity, query)
                return false
            }

        })
        return true
    }

    private fun setItemsVisibility(menu: Menu, exception: MenuItem,
                                   visible: Boolean) {
        for (i in 0 until menu.size()) {
            val item = menu.getItem(i)
            if (item !== exception)
                item.isVisible = visible
        }
    }

    override fun onResume() {
        super.onResume()
        uid = SharedPref.getStringValue(this, userId)
        if (uid != "") {
            edtNickName.setText(SharedPref.getStringValue(this, firstname))
        }
        println("$TAG, onResume: ${::cartBadge.isInitialized}: ")
        println("$TAG, onResume: ${SharedPref.getIntValue(this, cartCount)}: ")
        if (::cartBadge.isInitialized)
            setBadgeCount(this, cartBadge, SharedPref.getIntValue(this, cartCount).toString())

    }

    private fun setView() {
        btnAddToCartPD.setOnClickListener(this)
        ivUpArrow.setOnClickListener(this)
        ivDownArrow.setOnClickListener(this)
        ivAddToWishlistPD.setOnClickListener(this)
        ivSharePD.setOnClickListener(this)
        etQuentity.setOnClickListener(this)
        tvAddReview.setOnClickListener(this)
        btnSubmitReview.setOnClickListener(this)
        tvViewMoreReviews.setOnClickListener(this)

        //Product Gallery
        rvProductDetail.layoutManager = LinearLayoutManager(this@ProductDetailActivity, RecyclerView.HORIZONTAL, false)
        mGalleryAdapter = GalleryAdapter(this@ProductDetailActivity, galleryList, this@ProductDetailActivity)
        rvProductDetail.adapter = mGalleryAdapter
        rvProductDetail.isNestedScrollingEnabled = false

        //Reviews
        rvReviews.layoutManager = LinearLayoutManager(this@ProductDetailActivity, LinearLayout.VERTICAL, false)
        mReviewsAdapter = ReviewsAdapter(this@ProductDetailActivity, reviewsList)
        rvReviews.adapter = mReviewsAdapter

        if (!isNetworkAvailable(this@ProductDetailActivity)) {
            toast(this@ProductDetailActivity, getString(R.string.network_error))
        } else {
            getProductDetail()
        }


    }

    fun loadProductDetailImage(strImage: String) {
        Glide.with(this@ProductDetailActivity)
                .load(strImage)
                .placeholder(R.drawable.ic_place_holder)
                .error(R.drawable.ic_place_holder)
                .fitCenter()
                .dontAnimate()
                .into(ivProductDetail)
        ivProductDetail.setOnClickListener {
            val intent = Intent(this@ProductDetailActivity, FullScreenImageActivity::class.java)
            intent.putExtra("strImageUrl", strImage)
            startActivity(intent)
        }
    }

    private fun getProductDetail() {

        showProgressDialog(this, llParentProductDetail)

        val map = HashMap<String, String>()
        map["product_id"] = productId

        val apiService = RetrofitClientSingleton.getInstance()
        apiService.productDetail(map)
                .enqueue(object : retrofit2.Callback<ProductDetailResponse> {

                    override fun onResponse(call: Call<ProductDetailResponse>, response: Response<ProductDetailResponse>) {

                        stopProgress()

                        if (response.isSuccessful) {

                            when (response.body()?.status) {
                                SUCCESS -> {
                                    val productDetailResponse = response.body()
                                    productDetailResponse?.let {
                                        galleryList.clear()
                                        reviewsList.clear()
                                        if (productDetailResponse.status!! == SUCCESS) {
                                            val myProdudtDetailsResponse = productDetailResponse.data!!.product
                                            tvDescription.text = productDetailResponse.data?.product?.productDescription
                                            if (myProdudtDetailsResponse!!.special_price == null || myProdudtDetailsResponse.special_price == myProdudtDetailsResponse.productPrice || myProdudtDetailsResponse.special_price == "" || myProdudtDetailsResponse.special_price!! == "0") {
                                                tvAmount.text = "Rs." + myProdudtDetailsResponse.productPrice
                                                tvSepcialPrice.visibility = View.GONE
                                            } else {
                                                tvSepcialPrice.text = "Rs." + myProdudtDetailsResponse.special_price
                                                val ssBuilder =
                                                        SpannableStringBuilder("Rs" + myProdudtDetailsResponse.productPrice)
                                                val strikeboundSpan = StrikethroughSpan()
                                                ssBuilder.setSpan(
                                                        strikeboundSpan,
                                                        0,
                                                        ssBuilder.length,
                                                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                                                )

                                                tvAmount.text = ssBuilder
                                            }


                                            //tvAmount.text = productDetailResponse.data?.product?.productPrice


                                            tvDesc.text = productDetailResponse.data?.product?.productDescription
                                            tvReviews.text = productDetailResponse.data?.reviews?.count + " Reviews"

                                            loadProductDetailImage(productDetailResponse.data!!.product!!.productDetailImage!!)

                                            val productGalleryList = productDetailResponse.data?.product?.galleryData as java.util.ArrayList<GalleryData>

                                            val productReviewsList = productDetailResponse.data?.reviews?.review as java.util.ArrayList<Reviews>

                                            if (productDetailResponse.data!!.product!!.isInStock == 0) {
                                                llAddToCart.visibility = View.GONE
                                                tvAvaibilityStock.text = getString(R.string.availability_out_of_stock)
                                            } else {
                                                llAddToCart.visibility = View.VISIBLE
                                                tvAvaibilityStock.text = getString(R.string.availability_in_stock)
                                            }

                                            //Gallery
                                            galleryList.addAll(productGalleryList)
                                            mGalleryAdapter?.notifyDataSetChanged()

                                            //Reviews
                                            try {
                                                if (productReviewsList.size > 2)
                                                    reviewsList.addAll(productReviewsList.subList(0, 2))
                                                else
                                                    reviewsList.addAll(productReviewsList)

                                            } catch (e: Exception) {
                                                e.printStackTrace()
                                            }
                                            mReviewsAdapter?.notifyDataSetChanged()

                                            if (reviewsList.size > 0) {
                                                tvViewMoreReviews.visibility = View.VISIBLE
                                            } else {
                                                tvViewMoreReviews.visibility = View.GONE
                                            }

                                        } else {
                                            toast(this@ProductDetailActivity, productDetailResponse.message!!)
                                        }
                                    }
                                }


                                FAILURE -> {
                                    toast(this@ProductDetailActivity, response.body()!!.message)
                                }

                                ORDER_ALREADY_PLACED -> {
                                    toast(this@ProductDetailActivity, response.body()!!.message)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<ProductDetailResponse>, t: Throwable) {
                        stopProgress()
                        Log.e(TAG, t.message)
                        toast(this@ProductDetailActivity, t.message!!)
                    }
                })
    }


    private fun addReview() {
        val map = HashMap<String, String>()

        if (uid != "") {
            map["user_id"] = uid

        }

        map["product_id"] = productId
        map["nickname"] = edtNickName.text.toString()
        map["title"] = edtSummaryOfReview.text.toString().trim()
        map["detail"] = edtReview.text.toString().trim()

        showProgressDialog(this@ProductDetailActivity, tvAddReview as View)

        RetrofitClientSingleton.getInstance().addReview(map)
                .enqueue(object : retrofit2.Callback<StatusMessageResponse> {
                    override fun onResponse(call: Call<StatusMessageResponse>,
                                            response: Response<StatusMessageResponse>) {
                        stopProgress()
                        if (response.isSuccessful) {
                            when (response.body()?.status) {
                                SUCCESS -> {
                                    val responseReview = response.body()
                                    responseReview?.let {
                                        if (responseReview.status == SUCCESS) {

                                            responseReview.let {
                                                toast(this@ProductDetailActivity, responseReview.message!!)
                                            }

                                        } else {
                                            toast(this@ProductDetailActivity, responseReview.message!!)
                                        }
                                    }
                                }

                                FAILURE -> {
                                    toast(this@ProductDetailActivity, response.body()!!.message)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<StatusMessageResponse>, t: Throwable) {
                        stopProgress()
                        toast(this@ProductDetailActivity, getString(R.string.something_went_wrong))
                    }
                })
    }

    override fun onClickSelect(strItemName: String, pos: Int) {
        loadProductDetailImage(strItemName)
    }

    override fun onClick(pos: Int) {

    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.etQuentity -> {
                etQuentity.isCursorVisible = true
            }

            R.id.btnAddToCartPD -> {
                etQuentity.hideKeyboard()
                if (!isNetworkAvailable(this@ProductDetailActivity)) {
                    toast(this@ProductDetailActivity, getString(R.string.network_error))
                } else if (etQuentity.text.toString().trim().isEmpty() || etQuentity.text.toString().trim().toInt() == 0) {
                    toast(this@ProductDetailActivity, getString(R.string.please_enter_proper_qty))
                } else {
                    etQuentity.setText(formatString(etQuentity.text.toString().toDouble()))
                    if (etQuentity.text.toString().trim() == "0") {
                        etQuentity.setText("1")
                    }
                    addProductToCart(btnAddToCartPD)
                }

            }

            R.id.ivUpArrow -> {

                etQuentity.isCursorVisible = false
                if (!etQuentity.text.toString().isEmpty()) {
                    quentity = etQuentity.text.toString().toInt()
                    quentity += 1
                    etQuentity.setText(quentity.toString())
                } else {
                    quentity = 1
                }
            }

            R.id.ivDownArrow -> {
                etQuentity.isCursorVisible = false

                if (!etQuentity.text.toString().isEmpty()) {
                    if (quentity > 1) {
                        quentity = etQuentity.text.toString().toInt()
                        quentity -= 1
                        etQuentity.setText(quentity.toString())
                    }
                } else {
                    quentity = 1
                }
            }

            R.id.ivAddToWishlistPD -> {
                if (uid != "") {

                    if (!isNetworkAvailable(this@ProductDetailActivity)) {
                        toast(this@ProductDetailActivity, getString(R.string.network_error))
                    } else {
                        callApiForAddToWishList()
                    }

                } else {
                    startActivity(Intent(this, LoginActivity::class.java))
                }
            }

            R.id.ivSharePD -> {
                //    toast(this,"Coming soon...")
                startActivity(Intent(this, ShareProductActivity::class.java).putExtra("PRODUCT_ID", productId))
            }

            R.id.tvAddReview -> {
                scrollView.scrollTo(0, 0) // scroll to application top
                val reviewPosition: Int = llReview.bottom
                scrollView.scrollTo(0, reviewPosition)
                scrollView.fullScroll(ScrollView.FOCUS_DOWN)
            }

            R.id.btnSubmitReview -> {

                if (edtNickName.text.toString().trim().isEmpty()) {
                    toast(this, getString(R.string.enter_nick_name))
                    return
                } else if (edtSummaryOfReview.text.toString().trim().isEmpty()) {
                    toast(this, getString(R.string.enter_summary_of_review))
                    return
                } else if (edtReview.text.toString().trim().isEmpty()) {
                    toast(this, getString(R.string.enter_review))
                    return
                } else {

                    if (!isNetworkAvailable(this@ProductDetailActivity)) {
                        toast(this@ProductDetailActivity, getString(R.string.network_error))
                    } else {
                        addReview()
                    }
                }
            }

            R.id.tvViewMoreReviews -> {
                val intent = Intent(this@ProductDetailActivity, ViewMoreReviewActivity::class.java)
                intent.putExtra("PRODUCT_ID", productId)
                startActivity(intent)
            }
        }
    }

    private fun callApiForAddToWishList() {
        showProgressDialog(this, ivAddToWishlistPD)

        val map = HashMap<String, String>()

        map["product_id"] = productId
        map["qty"] = etQuentity.text.toString()
        map["user_id"] = uid

        RetrofitClientSingleton.getInstance().addToWishlist(map).enqueue(object : retrofit2.Callback<AddToWishListModel> {
            override fun onFailure(call: Call<AddToWishListModel>?, t: Throwable?) {
                stopProgress()
                toast(this@ProductDetailActivity, R.string.something_went_wrong)
            }

            override fun onResponse(call: Call<AddToWishListModel>?, response: Response<AddToWishListModel>) {
                stopProgress()
                if (response.isSuccessful) {
                    val responseProductCategory = response.body()
                    when (responseProductCategory?.status) {
                        SUCCESS -> {
                            toast(this@ProductDetailActivity, responseProductCategory.message)
                        }

                        FAILURE -> {
                            toast(this@ProductDetailActivity, responseProductCategory.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@ProductDetailActivity, responseProductCategory.message)
                        }
                    }

                } else {
                    toast(this@ProductDetailActivity, R.string.something_went_wrong)
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            R.id.menuSearch -> {

            }
            R.id.menuCart -> {
                if (SharedPref.getIntValue(this, cartCount) > 0)
                    startActivity(Intent(this, ShoppingCartActivity::class.java))
                else
                    toast(this, R.string.no_item_in_cart)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addProductToCart(view: View) {

        showProgressDialog(this, view)

        val map = HashMap<String, String>()

        map["product_id"] = productId
        map["qty"] = etQuentity.text.toString()

        val userIdForCart = SharedPref.getStringValue(this, userId)
        if (userIdForCart != "")
            map["user_id"] = userIdForCart

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()

        map["device_id"] = getAndroidId(this)

        RetrofitClientSingleton.getInstance().addToCart(map).enqueue(object : retrofit2.Callback<AddToCartModel> {
            override fun onFailure(call: Call<AddToCartModel>?, t: Throwable?) {
                stopProgress()
                toast(this@ProductDetailActivity, R.string.something_went_wrong)
            }

            override fun onResponse(call: Call<AddToCartModel>?, response: Response<AddToCartModel>) {
                stopProgress()
                if (response.isSuccessful) {
                    val responseProductCategory = response.body()
                    when (responseProductCategory?.status) {
                        SUCCESS -> {
                            etQuentity.setText("1")
                            SharedPref.setIntValue(this@ProductDetailActivity, cartCount, responseProductCategory.data!!.count)
                            SharedPref.setIntValue(this@ProductDetailActivity, cartId, responseProductCategory.data!!.cart_id)
                            toast(this@ProductDetailActivity, responseProductCategory.message)

                        }

                        FAILURE -> {
                            toast(this@ProductDetailActivity, responseProductCategory.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@ProductDetailActivity, responseProductCategory.message)
                        }
                    }

                } else {
                    toast(this@ProductDetailActivity, R.string.something_went_wrong)
                }

                // update cart count
                onResume()

            }
        })
    }
}