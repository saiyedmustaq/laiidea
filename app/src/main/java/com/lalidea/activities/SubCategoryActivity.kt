package com.lalidea.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.lalidea.R
import com.lalidea.adapters.SubMenuAdapter
import com.lalidea.listeners.OnGetMenuId
import com.lalidea.models.MenuModel
import kotlinx.android.synthetic.main.activity_sub_category.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import android.support.v7.widget.DividerItemDecoration
import android.graphics.Rect
import android.view.View


class SubCategoryActivity : AppCompatActivity(), OnGetMenuId {


    lateinit var list: ArrayList<MenuModel.DataBean.CategoryBean.SubCategory>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_category)
        var productTitle = intent.getStringExtra("Name")
        var categoryId = intent.getStringExtra("CategoryId")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.title = productTitle
        setSupportActionBar(toolbar)




        list = intent.getParcelableArrayListExtra<MenuModel.DataBean.CategoryBean.SubCategory>("Categories")
        Log.e("TEST", "" + list.size)
        val linearLayoutManager = LinearLayoutManager(this@SubCategoryActivity)
        rvSubcategory.layoutManager = linearLayoutManager
        val dividerItemDecoration = DividerItemDecoration(rvSubcategory.context,
                linearLayoutManager.orientation)
        rvSubcategory.addItemDecoration(dividerItemDecoration)
        bindRecyclerView()

    }

    override fun getMenuId(categoryId: String?, categoryName: String?, subcategory: ArrayList<MenuModel.DataBean.CategoryBean.SubCategory>?) {

    }

    override fun onClicMenu(categoryId: String?, categoryName: String?) {
        val productListIntent = Intent(this@SubCategoryActivity, ProductListActivity::class.java)
        productListIntent.putExtra("CategoryId", categoryId)
        productListIntent.putExtra("Name", categoryName)
        startActivity(productListIntent)
    }

    private fun bindRecyclerView() {

        val mAdapter = SubMenuAdapter(this@SubCategoryActivity, list, this@SubCategoryActivity)
        rvSubcategory.adapter = mAdapter
        mAdapter.notifyDataSetChanged()
    }

}
