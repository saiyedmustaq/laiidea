package com.lalidea.activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import com.lalidea.R
import com.lalidea.adapters.ShareProductAdapter
import com.lalidea.listeners.OnItemDeleteInShare
import com.lalidea.models.ShareRequestModel
import com.lalidea.models.StatusMessageResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.content_share.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_delete_confirm.*
import kotlinx.android.synthetic.main.dialog_share_recipient.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

/**
 * Created by reena on 16/12/17.
 */
class ShareProductActivity : AppCompatActivity(), View.OnClickListener, OnItemDeleteInShare {

    val TAG = ShareProductActivity::class.java.simpleName

    private var arrayList: ArrayList<ShareRequestModel.RecipientDataBean> = ArrayList()
    private var mAdapter: ShareProductAdapter? = null

    var productId: String? = null
    private var uid: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_product)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.share)

        productId = intent.getStringExtra("PRODUCT_ID")

        setView()
    }

    private fun setView() {
        btnAddRecipient.setOnClickListener(this)
        btnSendEmail.setOnClickListener(this)

        uid = SharedPref.getStringValue(this, userId)
        if (uid != "") {
            edtName.setText(SharedPref.getStringValue(this, firstname) + " " + SharedPref.getStringValue(this, lastname))
            edtEmail.setText(SharedPref.getStringValue(this, email))
        }

        rvShare.layoutManager = LinearLayoutManager(this)
        mAdapter = ShareProductAdapter(this, arrayList, this)
        rvShare.adapter = mAdapter
        rvShare.isNestedScrollingEnabled = false

    }

    override fun onItemDelete(pos: Int) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_delete_confirm)

        dialog.show()

        dialog.tvMessageDetailsDele.text = getString(R.string.delete_recipient)

        dialog.llCancelDialogDelete.setOnClickListener({ dialog.dismiss() })

        dialog.llOkDialogDelete.setOnClickListener({
            dialog.dismiss()
            arrayList.removeAt(pos)
            mAdapter?.notifyItemRemoved(pos)
        })
    }

    /**
     * dialog to add recipient
     * */
    private fun dialogAddRecipient() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_share_recipient)

        dialog.show()

        dialog.tvShareDialogCancel.setOnClickListener { dialog.dismiss() }

        dialog.tvShareDialogOk.setOnClickListener {
            if (dialog.etDialogNameShare.text.toString().trim().isEmpty()) {
                toast(this@ShareProductActivity, getString(R.string.enter_recipient_name))

            } else if (dialog.etDialogEmailShare.text.toString().trim().isEmpty()) {
                toast(this@ShareProductActivity, getString(R.string.enter_email))

            } else if (!isValidMail(dialog.etDialogEmailShare.text.toString().trim())) {
                toast(this@ShareProductActivity, getString(R.string.enter_valid_email))

            } else {
                val share = ShareRequestModel.RecipientDataBean()
                share.name = dialog.etDialogNameShare.text.toString()
                share.email = dialog.etDialogEmailShare.text.toString()
                arrayList.add(share)
                mAdapter?.notifyDataSetChanged()
                dialog.dismiss()

            }
        }
    }

    private fun sendShareProduct() {

        showProgressDialog(this, btnSendEmail)

        val shareModel = ShareRequestModel()
        shareModel.name = edtName.text.toString()
        shareModel.email = edtEmail.text.toString()
        shareModel.message = edtMessage.text.toString()

        shareModel.product_id = productId?.toInt()

        shareModel.recipient = arrayList

        RetrofitClientSingleton.getInstance().shareProduct(shareModel).enqueue(object :
                retrofit2.Callback<StatusMessageResponse> {

            override fun onResponse(call: Call<StatusMessageResponse>, response: Response<StatusMessageResponse>) {
                stopProgress()
                if (response.isSuccessful) {

                    val shareProductResponse = response.body()

                    when (shareProductResponse?.status) {
                        SUCCESS -> {
                            toast(this@ShareProductActivity, shareProductResponse.message)
                            finish()
                        }

                        FAILURE -> {
                            toast(this@ShareProductActivity, shareProductResponse.message)
                        }

                        ORDER_ALREADY_PLACED -> {
                            toast(this@ShareProductActivity, shareProductResponse.message)
                        }
                    }

                } else {
                    stopProgress()
                    toast(this@ShareProductActivity, response.body()?.message)

                }
            }

            override fun onFailure(call: Call<StatusMessageResponse>, error: Throwable) {
                stopProgress()
                Log.e(TAG, "onFailure : ${error.message}")
                toast(this@ShareProductActivity, getString(R.string.something_went_wrong))
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnAddRecipient -> {

                dialogAddRecipient()
            }

            R.id.btnSendEmail -> {

                if (edtName.text.toString().isEmpty()) {
                    toast(this@ShareProductActivity, getString(R.string.enter_name))
                } else if (edtEmail.text.toString().isEmpty()) {
                    toast(this@ShareProductActivity, getString(R.string.enter_email))
                } else if (!isValidMail(edtEmail.text.toString())) {
                    toast(this@ShareProductActivity, getString(R.string.enter_valid_email))
                } else if (edtMessage.text.toString().isEmpty()) {
                    toast(this@ShareProductActivity, getString(R.string.enter_message))
                } else if (arrayList.size == 0) {
                    toast(this@ShareProductActivity, getString(R.string.add_recipient))
                } else {
                    if (!isNetworkAvailable(this@ShareProductActivity)) {
                        toast(this@ShareProductActivity, getString(R.string.network_error))
                    } else {
                        sendShareProduct()
                    }
                }
            }
        }
    }
}