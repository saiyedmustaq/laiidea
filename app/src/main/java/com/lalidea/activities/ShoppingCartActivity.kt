package com.lalidea.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import com.lalidea.R
import com.lalidea.adapters.ShoppingCartAdapter
import com.lalidea.listeners.OnItemUpdateDeleteInCart
import com.lalidea.models.ShoppingCartResponseModel
import com.lalidea.models.StatusMessageResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_shopping_cart.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_delete_confirm.*
import kotlinx.android.synthetic.main.dialog_update_qty.*
import kotlinx.android.synthetic.main.layout_no_data.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class ShoppingCartActivity : AppCompatActivity(), OnItemUpdateDeleteInCart, View.OnClickListener {

    val TAG = ShoppingCartActivity::class.java.simpleName

    private var arrayList: ArrayList<ShoppingCartResponseModel.DataBean.ProductBean> = ArrayList()

    private var mAdapter: ShoppingCartAdapter? = null

    var inStockBol = false

    val getShoppingCartItem = 1
    val updateQty = 2
    val deleteItemFromCart = 3
    val applyCoupenCode = 4
    val removeCoupenCode = 5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_cart)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.title_shopping_cart)

//        Glide.with(this)
//                .load("http://www.lalidea.com/media/wysiwyg/infortis/ultimo/custom/banners/cart-discount.png")
//                .dontAnimate()
//                .into(ivDiscountCode)

        btnApplyDiscountCode.setOnClickListener(this)

        btnRemoveDiscountCode.setOnClickListener(this)

        btnCheckout.setOnClickListener(this)

        rvShoppingCart.layoutManager = LinearLayoutManager(this)
        rvShoppingCart.addItemDecoration(DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL))
        mAdapter = ShoppingCartAdapter(this, arrayList, this)
        rvShoppingCart.adapter = mAdapter
        rvShoppingCart.isNestedScrollingEnabled = false

        if (isNetworkAvailable(this))
            getShoppingCartItems()
        else {
            showNoInternetLayout(llParentNoData)
            rlParentShoppingCart.visibility = View.GONE
        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        val inflater = menuInflater
//        inflater.inflate(R.menu.menu_shopping_cart, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            R.id.menuEstimateShippingAndTax -> {
                toast(this, "Coming soon..")
                /*startActivity(Intent(this@ShoppingCartActivity,
                        EstimateShippingAndTaxActivity::class.java))*/
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onItemDelete(pos: Int) {
        deleteItemDialog(pos)
    }

    override fun onItemUpdate(pos: Int) {
        dialogEditQty(pos)
    }

    /**
     * dialog to edit quantity
     * */
    private fun dialogEditQty(pos: Int) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_update_qty)
        dialog.etDialogQty.setText(formatString(arrayList[pos].qty.toDouble()))
        dialog.etDialogQty.setSelection(dialog.etDialogQty.text.length)
        dialog.show()

        dialog.tvQtyDialogCancel.setOnClickListener { dialog.dismiss() }

        dialog.tvQtyDialogOk.setOnClickListener {
            if (dialog.etDialogQty.text.toString().trim().isEmpty() || dialog.etDialogQty.text.toString().trim().toInt() == 0) {
                toast(this@ShoppingCartActivity, getString(R.string.please_enter_proper_qty))
            } else {
                if (!isNetworkAvailable(this@ShoppingCartActivity)) {
                    toast(this@ShoppingCartActivity, getString(R.string.network_error))
                } else {
                    //TODO call update qty service
                    arrayList[pos].qty = dialog.etDialogQty.text.toString()
                    updateQty(arrayList[pos], dialog)

                }
            }
        }
    }

    private fun deleteItemDialog(pos: Int) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_delete_confirm)

        dialog.show()

        dialog.llCancelDialogDelete.setOnClickListener({ dialog.dismiss() })

        dialog.llOkDialogDelete.setOnClickListener({
            deleteItemFormCart(arrayList[pos], dialog)
        })
    }

    override fun onClick(v: View) {
        when (v.id) {

            R.id.btnCheckout -> {

                val uid = SharedPref.getStringValue(this, userId)

                if (uid != "") {
                    if (inStockBol) verifyCartId() else toast(this, getString(R.string.error_outofstock))

                } else {
                    startActivity(Intent(this, LoginActivity::class.java))

                }
            }

            R.id.btnApplyDiscountCode -> {
                if (etShoppingCartCoupenCode.text.isEmpty()) {
                    toast(this, getString(R.string.enter_coupen_code_first))
                } else {
                    etShoppingCartCoupenCode.hideKeyboard()
                    applyDiscount(etShoppingCartCoupenCode.text.toString())
                }
            }

            R.id.btnRemoveDiscountCode -> {
                removeDiscount()
            }
            else -> {
            }
        }
    }

    private fun getShoppingCartItems() {
        showProgressDialog(this@ShoppingCartActivity, rvShoppingCart)

        val map = HashMap<String, String>()

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()
        else
            map["cart_id"] = ""

        val userIdForCart = SharedPref.getStringValue(this, userId)

        if (userIdForCart != "")
            map["user_id"] = userIdForCart

        map["device_id"] = getAndroidId(this)

        RetrofitClientSingleton.getInstance().getShoppingCartItems(map)
                .enqueue(object : retrofit2.Callback<ShoppingCartResponseModel> {
                    override fun onFailure(call: Call<ShoppingCartResponseModel>, t: Throwable) {
                        stopProgress()
                        toast(this@ShoppingCartActivity, getString(R.string.something_went_wrong))
                        showSomethingWentWrongDataLayout(llParentNoData)
                        rlParentShoppingCart.visibility = View.GONE

                    }

                    override fun onResponse(call: Call<ShoppingCartResponseModel>, response: Response<ShoppingCartResponseModel>) {
                        stopProgress()
                        parseResponse(getShoppingCartItem, response)

//                        if (response.isSuccessful) {
//                            when (response.body()?.status) {
//                                SUCCESS -> {
//                                    val cartResponse = response.body()
//
//                                    cartResponse?.let {
//
//                                        if (cartResponse.data!!.product == null) {
//                                            println("$TAG, getShoppingCartItems >> null")
//                                        }
//
//                                        val list = cartResponse.data!!.product
//                                        if (list!!.isEmpty()) {
//                                            showNoDataLayout(llParentNoData, "No items found in cart")
//                                        } else {
//                                            val cartTotal = cartResponse.data!!.total
//                                            tvShoppingCartSubTotal.text = cartTotal!!.subtotal
//                                            tvShoppingGrandTotal.text = cartTotal.grand_total
//                                            if (!cartTotal.discount!!.isEmpty()) {
//                                                tvShoppingCartDiscount.visibility = View.VISIBLE
//                                                tvShoppingCartDiscount.text = cartTotal.discount
//                                            } else {
//                                                tvShoppingCartDiscount.visibility = View.GONE
//                                            }
////                                            arrayList.addAll(list)
////                                            mAdapter?.notifyDataSetChanged()
//                                            mAdapter?.setList(list)
//                                        }
//                                    }
//                                }
//
//                                FAILURE -> {
//                                    toast(this@ShoppingCartActivity, response.body()!!.message)
//                                    showSomethingWentWrongDataLayout(llParentNoData)
//                                    rlParentShoppingCart.visibility = View.GONE
//
//                                }
//
//                                ORDER_ALREADY_PLACED -> {
//                                    toast(this@ShoppingCartActivity, response.body()!!.message)
//                                    showSomethingWentWrongDataLayout(llParentNoData)
//                                    rlParentShoppingCart.visibility = View.GONE
//                                }
//                                else -> {
//                                    showSomethingWentWrongDataLayout(llParentNoData)
//                                    rlParentShoppingCart.visibility = View.GONE
//                                }
//                            }
//                        }
                    }

                })
    }


    private fun updateQty(productBean: ShoppingCartResponseModel.DataBean.ProductBean, dialog: Dialog) {
        showProgressDialog(this@ShoppingCartActivity, rvShoppingCart)

        val map = HashMap<String, String>()

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()

        val userIdForCart = SharedPref.getStringValue(this, userId)
        if (userIdForCart != "")
            map["user_id"] = userIdForCart

        map["qty"] = productBean.qty
        map["product_id"] = productBean.product_id!!
        map["cart_item_id"] = productBean.cart_item_id!!

        RetrofitClientSingleton.getInstance().editCart(map)
                .enqueue(object : retrofit2.Callback<ShoppingCartResponseModel> {
                    override fun onFailure(call: Call<ShoppingCartResponseModel>, t: Throwable) {
                        stopProgress()
                        toast(this@ShoppingCartActivity, getString(R.string.something_went_wrong))
                        showSomethingWentWrongDataLayout(llParentNoData)
                        rlParentShoppingCart.visibility = View.GONE
                    }

                    override fun onResponse(call: Call<ShoppingCartResponseModel>, response: Response<ShoppingCartResponseModel>) {
                        stopProgress()
                        dialog.dismiss()
                        parseResponse(updateQty, response)

//                        if (response.isSuccessful) {
//                            when (response.body()?.status) {
//                                SUCCESS -> {
//                                    val cartResponse = response.body()
//
//                                    cartResponse?.let {
//                                        val list = cartResponse.data!!.product
//                                        if (list!!.isEmpty()) {
//                                            rlParentShoppingCart.visibility = View.GONE
//                                            showNoDataLayout(llParentNoData, "No items found in cart")
//                                            SharedPref.setIntValue(this@ShoppingCartActivity, cartCount, 0)
//                                        } else {
//                                            arrayList.clear()
//                                            mAdapter?.clearAdapter()
//                                            val cartTotal = cartResponse.data!!.total
//                                            tvShoppingCartSubTotal.text = cartTotal!!.subtotal
//                                            tvShoppingGrandTotal.text = cartTotal.grand_total
//                                            if (!cartTotal.discount!!.isEmpty()) {
//                                                tvShoppingCartDiscount.visibility = View.VISIBLE
//                                                tvShoppingCartDiscount.text = cartTotal.discount
//                                            } else {
//                                                tvShoppingCartDiscount.visibility = View.GONE
//                                            }
//                                            SharedPref.setIntValue(this@ShoppingCartActivity, cartCount, list.size)
////                                            arrayList.addAll(list)
////                                            mAdapter?.notifyDataSetChanged()
//                                            mAdapter?.setList(list)
//                                        }
//                                    }
//
//                                    dialog.dismiss()
//                                }
//
//                                FAILURE -> {
//                                    toast(this@ShoppingCartActivity, response.body()!!.message)
//                                    showSomethingWentWrongDataLayout(llParentNoData)
//                                    rlParentShoppingCart.visibility = View.GONE
//                                }
//
//                                ORDER_ALREADY_PLACED -> {
//                                    toast(this@ShoppingCartActivity, response.body()!!.message)
//                                    showSomethingWentWrongDataLayout(llParentNoData)
//                                    rlParentShoppingCart.visibility = View.GONE
//                                }
//                            }
//                        }
                    }

                })
    }

    private fun deleteItemFormCart(productBean: ShoppingCartResponseModel.DataBean.ProductBean,
                                   dialog: Dialog) {
        showProgressDialog(this@ShoppingCartActivity, rvShoppingCart)

        val map = HashMap<String, String>()

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()

        map["product_id"] = productBean.product_id!!
        map["cart_item_id"] = productBean.cart_item_id!!

        RetrofitClientSingleton.getInstance()
                .deleteItemFromCart(map).enqueue(object : retrofit2.Callback<ShoppingCartResponseModel> {
            override fun onFailure(call: Call<ShoppingCartResponseModel>, t: Throwable) {
                stopProgress()
                toast(this@ShoppingCartActivity, getString(R.string.something_went_wrong))
                showSomethingWentWrongDataLayout(llParentNoData)
                rlParentShoppingCart.visibility = View.GONE
            }

            override fun onResponse(call: Call<ShoppingCartResponseModel>, response: Response<ShoppingCartResponseModel>) {
                stopProgress()
                dialog.dismiss()

                parseResponse(deleteItemFromCart, response)

//                if (response.isSuccessful) {
//                    when (response.body()?.status) {
//                        SUCCESS -> {
//                            val cartResponse = response.body()
//
//                            cartResponse?.let {
//                                val list = cartResponse.data!!.product
//
//
//                                if (cartResponse.data!!.product == null) {
//                                    println("$TAG, deleteItemFormCart >> null")
//                                }
//                                if (list!!.isEmpty()) {
//                                    SharedPref.setIntValue(this@ShoppingCartActivity, cartCount, 0)
//                                    rlParentShoppingCart.visibility = View.GONE
//                                    showNoDataLayout(llParentNoData, "No items found in cart")
//                                } else {
//                                    arrayList.clear()
//                                    mAdapter?.clearAdapter()
//                                    val cartTotal = cartResponse.data!!.total
//                                    tvShoppingCartSubTotal.text = cartTotal!!.subtotal
//                                    tvShoppingGrandTotal.text = cartTotal.grand_total
//                                    if (!cartTotal.discount!!.isEmpty()) {
//                                        tvShoppingCartDiscount.visibility = View.VISIBLE
//                                        tvShoppingCartDiscount.text = cartTotal.discount
//                                    } else {
//                                        tvShoppingCartDiscount.visibility = View.GONE
//                                    }
//                                    SharedPref.setIntValue(this@ShoppingCartActivity, cartCount, list.size)
//
////                                    arrayList.addAll(list)
////                                    mAdapter?.notifyDataSetChanged()
//                                    mAdapter?.setList(list)
//                                }
//                            }
//                            dialog.dismiss()
//                        }
//
//                        FAILURE -> {
//                            toast(this@ShoppingCartActivity, response.body()!!.message)
//                            showSomethingWentWrongDataLayout(llParentNoData)
//                            rlParentShoppingCart.visibility = View.GONE
//                        }
//
//                        ORDER_ALREADY_PLACED -> {
//                            toast(this@ShoppingCartActivity, response.body()!!.message)
//                            showSomethingWentWrongDataLayout(llParentNoData)
//                            rlParentShoppingCart.visibility = View.GONE
//                        }
//                    }
//                }
            }

        })
    }

    private fun applyDiscount(strCode: String) {
        showProgressDialog(this@ShoppingCartActivity, rvShoppingCart)

        val map = HashMap<String, String>()
        map["code"] = strCode

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()

        RetrofitClientSingleton.getInstance().applyDiscount(map)
                .enqueue(object : retrofit2.Callback<ShoppingCartResponseModel> {
                    override fun onFailure(call: Call<ShoppingCartResponseModel>, t: Throwable) {
                        stopProgress()
                        toast(this@ShoppingCartActivity, getString(R.string.something_went_wrong))
                        rlParentShoppingCart.visibility = View.GONE
                        showSomethingWentWrongDataLayout(llParentNoData)
                    }

                    override fun onResponse(call: Call<ShoppingCartResponseModel>, response: Response<ShoppingCartResponseModel>) {
                        stopProgress()

//                        parseResponse(applyCoupenCode,response)
                        if (response.isSuccessful) {
                            when (response.body()?.status) {
                                SUCCESS -> {
                                    val cartResponse = response.body()

                                    cartResponse?.let {

                                        toast(this@ShoppingCartActivity, cartResponse.message)

                                        if (cartResponse.data!!.product == null) {
                                            println("$TAG, getShoppingCartItems >> null")
                                        }

                                        val list = cartResponse.data!!.product
                                        if (list!!.isEmpty()) {
                                            rlParentShoppingCart.visibility = View.GONE
                                            showNoDataLayout(llParentNoData, "No items found in cart")
                                        } else {
                                            val cartTotal = cartResponse.data!!.total
                                            tvShoppingCartSubTotal.text = "Subtotal " + cartTotal!!.subtotal
                                            tvShoppingGrandTotal.text = "Grand Total " + cartTotal.grand_total
                                            if (!cartTotal.discount!!.isEmpty()) {
                                                tvShoppingCartDiscount.visibility = View.VISIBLE
                                                tvShoppingCartDiscount.text = "Discount " +cartTotal . discount
                                            } else {
                                                tvShoppingCartDiscount.visibility = View.GONE
                                            }

                                            mAdapter?.setList(list)

                                            checkOutofStock(list)

                                            rlApplyCoupenCode.visibility = View.GONE
                                            rlRemoveCoupenCode.visibility = View.VISIBLE
                                            tvShoppingCartCoupenCode.text = strCode

                                            SharedPref.setStringValue(this@ShoppingCartActivity, coupenCode, strCode)
                                        }
                                    }


                                }

                                FAILURE -> {
                                    toast(this@ShoppingCartActivity, response.body()!!.message)
                                }

                                ORDER_ALREADY_PLACED -> {
                                    toast(this@ShoppingCartActivity, response.body()!!.message)
                                    rlParentShoppingCart.visibility = View.GONE
                                    showSomethingWentWrongDataLayout(llParentNoData)
                                }
                            }
                        }
                    }

                })
    }


    private fun removeDiscount() {
        showProgressDialog(this@ShoppingCartActivity, rvShoppingCart)

        val map = HashMap<String, String>()

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()

        RetrofitClientSingleton.getInstance().removeDiscount(map)
                .enqueue(object : retrofit2.Callback<ShoppingCartResponseModel> {
                    override fun onFailure(call: Call<ShoppingCartResponseModel>, t: Throwable) {
                        stopProgress()
                        toast(this@ShoppingCartActivity, getString(R.string.something_went_wrong))
                        rlParentShoppingCart.visibility = View.GONE
                        showSomethingWentWrongDataLayout(llParentNoData)
                    }

                    override fun onResponse(call: Call<ShoppingCartResponseModel>, response: Response<ShoppingCartResponseModel>) {
                        stopProgress()
                        parseResponse(removeCoupenCode, response)

//                        if (response.isSuccessful) {
//                            when (response.body()?.status) {
//                                SUCCESS -> {
//                                    val cartResponse = response.body()
//
//                                    cartResponse?.let {
//
//                                        if (cartResponse.data!!.product == null) {
//                                            println("$TAG, getShoppingCartItems >> null")
//                                        }
//
//                                        val list = cartResponse.data!!.product
//                                        if (list!!.isEmpty()) {
//                                            rlParentShoppingCart.visibility = View.GONE
//                                            showNoDataLayout(llParentNoData, "No items found in cart")
//                                        } else {
//                                            val cartTotal = cartResponse.data!!.total
//                                            tvShoppingCartSubTotal.text = cartTotal!!.subtotal
//                                            tvShoppingGrandTotal.text = cartTotal.grand_total
//                                            if (!cartTotal.discount!!.isEmpty()) {
//                                                tvShoppingCartDiscount.visibility = View.VISIBLE
//                                                tvShoppingCartDiscount.text = cartTotal.discount
//                                            } else {
//                                                tvShoppingCartDiscount.visibility = View.GONE
//                                            }
////                                            arrayList.clear()
////                                            mAdapter?.clearAdapter()
////                                            arrayList.addAll(list)
////                                            mAdapter?.notifyDataSetChanged()
//                                            mAdapter?.setList(list)
//
//                                            rlApplyCoupenCode.visibility = View.VISIBLE
//                                            rlRemoveCoupenCode.visibility = View.GONE
//                                            tvShoppingCartCoupenCode.text = ""
//                                            etShoppingCartCoupenCode.setText("")
//                                        }
//                                    }
//                                }
//
//                                FAILURE -> {
//                                    toast(this@ShoppingCartActivity, response.body()!!.message)
//                                    rlParentShoppingCart.visibility = View.GONE
//                                    showSomethingWentWrongDataLayout(llParentNoData)
//                                }
//
//                                ORDER_ALREADY_PLACED -> {
//                                    toast(this@ShoppingCartActivity, response.body()!!.message)
//                                    rlParentShoppingCart.visibility = View.GONE
//                                    showSomethingWentWrongDataLayout(llParentNoData)
//                                }
//                            }
//                        }
                    }

                })
    }

    fun parseResponse(from: Int, response: Response<ShoppingCartResponseModel>) {
        if (response.isSuccessful) {
            when (response.body()?.status) {
                SUCCESS -> {
                    val cartResponse = response.body()

                    cartResponse?.let {

                        if (cartResponse.data!!.product == null) {
                            println("$TAG, parseResponse >> null")
                        }

                        val list = cartResponse.data!!.product
                        if (list!!.isEmpty()) {
                            rlParentShoppingCart.visibility = View.GONE
                            SharedPref.setIntValue(this@ShoppingCartActivity, cartCount, 0)
                            showNoDataLayout(llParentNoData, "No items found in cart")
                        } else {
                            val cartTotal = cartResponse.data!!.total
                            tvShoppingCartSubTotal.text = "Subtotal " + cartTotal!!.subtotal
                            tvShoppingGrandTotal.text = "Grand Total " + cartTotal.grand_total
                            if (!cartTotal.discount!!.isEmpty()) {
                                tvShoppingCartDiscount.visibility = View.VISIBLE
                                tvShoppingCartDiscount.text = "Discount " + cartTotal.discount
                            } else {
                                tvShoppingCartDiscount.visibility = View.GONE
                            }
//
                            mAdapter?.setList(list)

                            checkOutofStock(list)

                            rlApplyCoupenCode.visibility = View.VISIBLE
                            rlRemoveCoupenCode.visibility = View.GONE
                            tvShoppingCartCoupenCode.text = ""
                            etShoppingCartCoupenCode.setText("")

                            if (from == removeCoupenCode) {
                                SharedPref.setStringValue(this@ShoppingCartActivity, coupenCode, "")
                            }

                            if (cartResponse.data!!.total!!.discount != "") {
                                rlApplyCoupenCode.visibility = View.GONE
                                rlRemoveCoupenCode.visibility = View.VISIBLE

                                val coupenCode = SharedPref.getStringValue(this@ShoppingCartActivity, coupenCode, "attune")

                                tvShoppingCartCoupenCode.text = coupenCode
                                etShoppingCartCoupenCode.setText(coupenCode)
                            } else {
                                rlApplyCoupenCode.visibility = View.VISIBLE
                                rlRemoveCoupenCode.visibility = View.GONE
                                tvShoppingCartCoupenCode.text = ""
                                etShoppingCartCoupenCode.setText("")
                            }

                            SharedPref.setIntValue(this@ShoppingCartActivity, cartCount, cartResponse.data!!.total!!.count)
                        }
                    }
                }

                FAILURE -> {
                    toast(this@ShoppingCartActivity, response.body()!!.message)
                    rlParentShoppingCart.visibility = View.GONE
                    showSomethingWentWrongDataLayout(llParentNoData)
                }

                ORDER_ALREADY_PLACED -> {
                    toast(this@ShoppingCartActivity, response.body()!!.message)
                    rlParentShoppingCart.visibility = View.GONE
                    showSomethingWentWrongDataLayout(llParentNoData)
                    orderAlreadyPlacedDialog(this@ShoppingCartActivity, response.body()!!.message!!)
                }
                else -> {
                    toast(this@ShoppingCartActivity, response.body()!!.message)
                    rlParentShoppingCart.visibility = View.GONE
                    showSomethingWentWrongDataLayout(llParentNoData)
                }
            }
        } else {
            toast(this@ShoppingCartActivity, R.string.something_went_wrong)
            rlParentShoppingCart.visibility = View.GONE
            showSomethingWentWrongDataLayout(llParentNoData)
        }
    }

    private fun checkOutofStock(list: List<ShoppingCartResponseModel.DataBean.ProductBean>) {
        for (cartListModel in list) {
            val inStock = cartListModel.inStock

            if (inStock == 0) {

                inStockBol = false
                break
            } else {
                inStockBol = true

            }
        }

    }

    private fun verifyCartId() {
        showProgressDialog(this@ShoppingCartActivity, rvShoppingCart)

        val map = HashMap<String, String>()

        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()

        RetrofitClientSingleton.getInstance().verifycartId(map)
                .enqueue(object : retrofit2.Callback<StatusMessageResponse> {
                    override fun onFailure(call: Call<StatusMessageResponse>, t: Throwable) {
                        stopProgress()
                        toast(this@ShoppingCartActivity, getString(R.string.something_went_wrong))
                    }

                    override fun onResponse(call: Call<StatusMessageResponse>, response: Response<StatusMessageResponse>) {
                        stopProgress()

                        if (response.isSuccessful) {
                            when (response.body()?.status) {
                                SUCCESS -> {
                                    startActivity(Intent(this@ShoppingCartActivity, OrderCheckOutActivity::class.java))
                                }

                                FAILURE -> {
                                    toast(this@ShoppingCartActivity, response.body()!!.message)
                                }

                                ORDER_ALREADY_PLACED -> {
                                    orderAlreadyPlacedDialog(this@ShoppingCartActivity, response.body()!!.message!!)
                                }
                            }
                        }
                    }

                })
    }
}
