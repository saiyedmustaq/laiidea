package com.lalidea.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.lalidea.R
import com.lalidea.models.UserDataResponse
import com.lalidea.retrofitClient.RetrofitClientSingleton
import com.lalidea.utils.*
import kotlinx.android.synthetic.main.activity_registration.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class RegistrationActivity : AppCompatActivity(), View.OnClickListener {

    val TAG = RegistrationActivity::class.java.simpleName
    private lateinit var strFirstName: String
    private lateinit var strlastname: String
    private lateinit var strMiddleInitial: String
    private lateinit var strEmail: String
    private lateinit var strPass: String
    private var isSubscribed = "1"
    private lateinit var confirmPassword: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.sign_up)

        setView()
    }

    private fun setView() {
        signUpRegister.setOnClickListener(this)
        tvSignInReg.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.signUpRegister -> onClickSignUp()

            R.id.tvSignInReg -> {
//                startActivity(Intent(this@RegistrationActivity, LoginActivity::class.java))
                finish()
            }

            else -> {
            }
        }
    }

    private fun onClickSignUp() {
        if (etFirstNameReg.text.toString().trim().isEmpty()) {
            toast(this@RegistrationActivity, getString(R.string.enterfirstname))
        } else if (!isValidText(etFirstNameReg.text.toString().trim())) {
            toast(this@RegistrationActivity, getString(R.string.enterfirstname))
        } else if (etLastNameReg.text.toString().trim().isEmpty()) {
            toast(this@RegistrationActivity, getString(R.string.enterlastname))
        } else if (!isValidText(etLastNameReg.text.toString().trim())) {
            toast(this@RegistrationActivity, getString(R.string.enterlastname))
        } else if (etRegEmail.text.toString().trim().isEmpty()) {
            toast(this@RegistrationActivity, getString(R.string.enteremail))
        } else if (!isValidMail(etRegEmail.text.toString().trim())) {
            toast(this@RegistrationActivity, getString(R.string.validemail))

        } else if (etRegPassword.text.toString().trim().isEmpty()) {
            toast(this@RegistrationActivity, getString(R.string.enterpass))
        } else if (etRegPassword.text.toString().trim().length < 6) {
            toast(this@RegistrationActivity, getString(R.string.enterpass_lentgh))
        } else if (etRegConfirmPass.text.toString().trim().isEmpty()) {
            toast(this@RegistrationActivity, getString(R.string.enterconfirmpass))
        } else if (!etRegPassword.text.toString().trim().equals(etRegConfirmPass.text.toString().trim())) {
            toast(this@RegistrationActivity, getString(R.string.passnotmatch))
        } else {
            strFirstName = etFirstNameReg.text.toString()
            strMiddleInitial = etMiddleNameReg.text.toString()
            strlastname = etLastNameReg.text.toString()
            strEmail = etRegEmail.text.toString()
            strPass = etRegPassword.text.toString()
            confirmPassword = etRegConfirmPass.text.toString()
            if (!isNetworkAvailable(this@RegistrationActivity)) {
                toast(this@RegistrationActivity, getString(R.string.network_error))
            } else {
                isSubscribed = if (chRegNewsLetter.isChecked) "1" else "0"
                signUpRegister.hideKeyboard()
                doRegister()
            }
        }
    }

    private fun doRegister() {
        showProgressDialog(this@RegistrationActivity, signUpRegister as View)

        val map = HashMap<String, String>()

        map["firstname"] = strFirstName
        map["middlename"] = strMiddleInitial
        map["lastname"] = strlastname
        map["email"] = strEmail
        map["is_subscribed"] = isSubscribed
        map["password"] = strPass
        map["confirmation"] = confirmPassword
        val userCartId = SharedPref.getIntValue(this, cartId)
        if (userCartId != 0)
            map["cart_id"] = userCartId.toString()
        else
            map["cart_id"] = ""
        map["device_id"] = getAndroidId(this)
        RetrofitClientSingleton.getInstance()
                .getSignUp(map)
                .enqueue(object : retrofit2.Callback<UserDataResponse> {
                    override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                        stopProgress()
                        Log.e(TAG, t.message)
                        toast(this@RegistrationActivity, R.string.something_went_wrong)
                    }

                    override fun onResponse(call: Call<UserDataResponse>, response: Response<UserDataResponse>) {
                        stopProgress()
                        if (response.isSuccessful) {

                            val userDataResponse = response.body()
                            when (userDataResponse!!.status) {
                                SUCCESS -> {
                                    toast(this@RegistrationActivity, userDataResponse.message!!)
                                    finish()
                                }
                                FAILURE -> {
                                    toast(this@RegistrationActivity, userDataResponse.message!!)
                                }
                                else -> {
                                    toast(this@RegistrationActivity, userDataResponse.message!!)
                                }
                            }
                        }
                    }
                })
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
