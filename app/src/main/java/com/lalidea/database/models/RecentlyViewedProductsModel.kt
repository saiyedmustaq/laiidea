package com.lalidea.database.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "Recently_viewed_products")
data class RecentlyViewedProductsModel(@PrimaryKey @ColumnInfo(name = "id") var id: String = "0",
//                                       @ColumnInfo(name = "product_id") var productId: String? = null,
                                       @ColumnInfo(name = "product_name") var name: String? = null)