package com.lalidea.database.dao

import android.arch.persistence.room.*
import com.lalidea.database.models.RecentlyViewedProductsModel
import io.reactivex.Flowable

@Dao
interface RecentlyViewedProductsModelDao {

    @Query("select * from Recently_viewed_products")
    fun getAllRecentlyViewedProducts(): Flowable<List<RecentlyViewedProductsModel>>

    @Query("select * from Recently_viewed_products")
    fun getAllRecentlyViewedProductsCount(): MutableList<RecentlyViewedProductsModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecentlyViewedProduct(category: RecentlyViewedProductsModel)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateRecentlyViewedProduct(category: RecentlyViewedProductsModel)

    @Delete
    fun deleteRecentlyViewedProduct(category: RecentlyViewedProductsModel)

}