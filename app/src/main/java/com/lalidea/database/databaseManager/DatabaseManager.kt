package com.lalidea.database.databaseManager

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.lalidea.database.dao.RecentlyViewedProductsModelDao
import com.lalidea.database.models.RecentlyViewedProductsModel

@Database(entities = [(RecentlyViewedProductsModel::class)], version = 1)
abstract class DatabaseManager : RoomDatabase() {
    abstract fun recentlyViewedProductsDao(): RecentlyViewedProductsModelDao
}